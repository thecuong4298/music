// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
    clientId: 'awesome-music-id',
    clientSecret: 'awesome-music',
    production: false,
    AUTHEN_URL: 'http://localhost:8082',
    API_URL: 'http://localhost:8082',
    // AUTHEN_URL: 'http://14.225.5.246:8090',
    // API_URL: 'http://14.225.5.246:8090',
    WEB_URL: 'http://localhost:4200',
    APP_CODE: 'VDS_REPORT',
    AUTHORIZATION_CLIENT: 'Basic YXdlc29tZS1tdXNpYy1pZDphd2Vzb21lLW11c2lj',
    ADMIN_ROLE_CODE: 'ROLE_ADMIN',
    CUSTOMER_ROLE_CODE: 'ROLE_CUSTOMER'
  };
  