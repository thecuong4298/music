import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { config } from './config/application.config';
import { TranslateService } from '@ngx-translate/core';
@Component({
  // tslint:disable-next-line
  selector: 'body',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  ok = false;
  show = true;
  constructor(private router: Router, private translate: TranslateService) {
    let lang = localStorage.getItem(config.user_default_language);
    if (lang == undefined) {
      lang = config.defaultLanguage;
    }
    translate.setDefaultLang(lang);
    translate.use(lang);
  }

  ngOnInit() {
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0);
    });
    setTimeout(() => {
      this.show = false;
    }, 500);
  }
}
