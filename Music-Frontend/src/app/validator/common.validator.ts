import { Validator, FormGroup, ValidatorFn } from '@angular/forms';

export class CommonValidator implements Validator {

    static checkPasswords(pass: string, confirm: string): ValidatorFn {
        return (fg: FormGroup) => {
            const newPass = fg.get(pass).value.trim();
            const confirmPass = fg.get(confirm).value.trim();
            return newPass === confirmPass ? null : { notSame: true };
        };
    }

    validate(control: import('@angular/forms').AbstractControl): import('@angular/forms').ValidationErrors {
        return undefined;
    }
    registerOnValidatorChange?(fn: () => void): void {
    }
}
