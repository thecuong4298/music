import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss']
})
export class InfoComponent implements OnInit {

  @Input() info: any;
  @Output() selectAll = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit(): void {
  }

  listenAll() {
    this.selectAll.emit(true);
  }
}
