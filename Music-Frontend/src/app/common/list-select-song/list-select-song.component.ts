import { Component, OnInit, Inject } from '@angular/core';
import { SongService } from '../../services/song.service';
import { ToastrService } from 'ngx-toastr';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-list-select-song',
  templateUrl: './list-select-song.component.html',
  styleUrls: ['./list-select-song.component.scss']
})
export class ListSelectSongComponent implements OnInit {
  name = '';
  listSong = [];
  songs = [];
  form = new FormGroup({
    name: new FormControl('')
  });
  constructor(
    private songService: SongService,
    private toastr: ToastrService,
    public dialogRef: MatDialogRef<ListSelectSongComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) { }

  ngOnInit(): void {
    if (this.data.songs) {
      this.data.songs.forEach(song => {
        this.songs.push(song);
      });
    }
    this.search();
  }

  check(songId) {
    if (this.songs.filter((song) => song.id === songId).length > 0) {
      return true;
    }
    return false;
  }

  search() {
    this.songService.findByCondition(this.form.controls.name.value).subscribe((song: any) => {
      if (song.code === 200) {
        this.listSong = song.data;
      } else {
        this.listSong = [];
      }
    }, err => {
      this.showError('Có lỗi không xác định xảy ra');
    });
  }

  showError(message: string) {
    this.toastr.error(message);
  }

  close() {
    this.dialogRef.close({ event: 'cancel' });
  }

  save() {
    this.dialogRef.close({ data: { songs: this.songs } });
  }

  choose(song, isCheck) {
    if (isCheck.checked) {
      this.songs.push(song);
    } else {
      this.songs = this.songs.filter((vartist) => vartist.id !== song.id);
    }
  }
}
