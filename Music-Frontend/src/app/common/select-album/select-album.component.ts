import { Component, OnInit, Inject } from '@angular/core';
import { SongService } from '../../services/song.service';
import { ToastrService } from 'ngx-toastr';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormControl, FormGroup } from '@angular/forms';
import { AlbumService } from '../../services/album.service';
import { TopicService } from '../../services/topic.service';

@Component({
  selector: 'app-select-album',
  templateUrl: './select-album.component.html',
  styleUrls: ['./select-album.component.scss']
})
export class SelectAlbumComponent implements OnInit {
  name = '';
  list = [];
  form = new FormGroup({
    name: new FormControl('')
  });
  constructor(
    private albumService: AlbumService,
    private toastr: ToastrService,
    private topicService: TopicService,
    private songService: SongService,
    public dialogRef: MatDialogRef<SelectAlbumComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) { }

  ngOnInit(): void {
    // if (this.data.songs) {
    //   this.data.songs.forEach(song => {
    //     this.songs.push(song);
    //   });
    // }
    this.search('');
  }


  search(name) {
    if (this.data.type == 'album') {
      this.albumService.findByCondition(name).subscribe((album: any) => {
        if (album.code === 200) {
          this.list = album.data;
        } else {
          this.list = [];
        }
      }, err => {
        this.showError('Có lỗi không xác định xảy ra');
      });
    } else if (this.data.type == 'topic') {
      this.topicService.findByCondition(name).subscribe((topic: any) => {
        if (topic.code === 200) {
          this.list = topic.data;
        } else {
          this.list = [];
        }
      }, err => {
        this.showError('Có lỗi không xác định xảy ra');
      });
    } else {
      this.songService.findByCondition(name).subscribe((song: any) => {
        if (song.code === 200) {
          this.list = song.data;
        } else {
          this.list = [];
        }
      }, err => {
        this.showError('Có lỗi không xác định xảy ra');
      });
    }
  }

  showError(message: string) {
    this.toastr.error(message);
  }

  close() {
    this.dialogRef.close({ event: 'cancel' });
  }

  choose(item) {
    this.dialogRef.close({ data: { item: item } });
  }
}
