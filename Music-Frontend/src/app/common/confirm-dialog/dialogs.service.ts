import { ConfirmDialogComponent } from './confirm-dialog.component';
import { TranslateService } from '@ngx-translate/core';
import { Injectable, Renderer2, RendererFactory2 } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Observable } from 'rxjs';

@Injectable()
export class DialogsService {
  bsModalRef: BsModalRef;
  renderer: Renderer2;

  constructor(
    rendererFactory: RendererFactory2,
    private translateService: TranslateService,
    public modalService: BsModalService) {
    this.renderer = rendererFactory.createRenderer(null, null);
  }

  public confirm(maintitle: string, message: string, labelYes?: string, labelNo?: string) {

    const yes = labelYes ? labelYes : this.translateService.instant('dashboard.button.yes');
    const no = labelNo ? labelNo : this.translateService.instant('dashboard.button.no');
    let title = '';
    if (!maintitle) {
      title = this.translateService.instant('Thông báo');
    } else {
      title = maintitle;
    }
    const initialState: DialogOptions = {
      title,
      message,
      buttons: {
        confirm: {
          label: yes,
          className: ''
        },
        cancel: {
          label: no,
          className: ''
        }
      },
      iconClass: 'text-warning fa fa-question-circle'
    };
    this.playSound('messagebox');
    this.bsModalRef = this.modalService.show(ConfirmDialogComponent,
      {
        initialState,
        backdrop: 'static',
        keyboard: false,

      });
    return this.bsModalRef.content.onClose as Observable<boolean>;
  }
  
  public playSound(sound: string, path = 'assets/media/sound') {
  //   const audioElement = document.createElement('audio');
  //   if (navigator.userAgent.match('Firefox/')) {
  //     audioElement.setAttribute('src', path + '/' + sound + '.ogg');
  //   } else {
  //     audioElement.setAttribute('src', path + '/' + sound + '.mp3');
  //   }

  //   audioElement.addEventListener('load', () => {
  //     audioElement.play();
  //   }, true);
  //   audioElement.pause();
  //   audioElement.play();
  }
}

export interface DialogOptions {
  title: string;
  message: string;
  buttons: DialogButtons;
  imageUrl?: string;
  iconClass?: string;
}



export interface DialogButton {
  label: string;
  className: string;
}

export interface DialogButtons {

  confirm?: DialogButton;
  cancel?: DialogButton;
}
