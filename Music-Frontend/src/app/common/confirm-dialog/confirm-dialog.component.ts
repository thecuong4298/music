import { BsModalRef } from 'ngx-bootstrap/modal';
import { DialogButtons } from './dialogs.service';
import {Component, ElementRef, HostListener, OnInit, QueryList, ViewChild, ViewChildren} from '@angular/core';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: 'confirm-dialog.component.html',
  styleUrls: ['confirm-dialog.component.css']
})
export class ConfirmDialogComponent implements OnInit {

  title: string;
  message: string;
  buttons: DialogButtons;
  onClose = new Subject();
  iconClass: any;
  buttonCollection;
  cursor = 1;


  @ViewChildren('confirmDialogButton') buttonsQueryList: QueryList<ElementRef<HTMLElement>>;
  @HostListener('document:keydown', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    event.preventDefault();
    if (event.shiftKey && event.key === 'Tab') {
      if (this.cursor === 0) {
        this.cursor = this.buttonCollection.length - 1;
      } else {
        this.cursor--;
      }
      this.buttonCollection[this.cursor].nativeElement.focus();
    } else {
      switch (event.key) {
        case 'Escape':
          this.cancel(event);
          break;
        case 'Tab':
          if (this.cursor === this.buttonCollection.length - 1) {
            this.cursor = 0;
          } else {
            this.cursor++;
          }
          this.buttonCollection[this.cursor].nativeElement.focus();
          break;
        case 'Enter':
          if (this.cursor === 0) {
            this.cancel(event);
          } else {
            this.buttonCollection[this.cursor].nativeElement.click();
          }
          break;
      }
    }
  }

  constructor(public bsModalRef: BsModalRef) { }

  confirm($event: MouseEvent | KeyboardEvent) {
    this.onClose.next(true);
    this.onClose.complete();
    this.bsModalRef.hide();
  }
  cancel($event: MouseEvent | KeyboardEvent) {
    this.onClose.next(false);
    this.onClose.complete();
    this.bsModalRef.hide();
  }

  ngOnInit(): void {
    const focusToConfirmButton = setTimeout(() => {
      this.cursor = 1;
      this.buttonCollection = this.buttonsQueryList.toArray();
      this.buttonCollection[this.cursor].nativeElement.focus();
      clearTimeout(focusToConfirmButton);
    }, 0);
  }

}
