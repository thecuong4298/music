import { Component, OnInit, ViewEncapsulation, Input, Output, EventEmitter, OnChanges } from '@angular/core';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CarouselComponent implements OnInit, OnChanges {
  @Input() title: string;
  @Input() slides: any;
  @Input() lists: any;
  @Output() clickShowAll = new EventEmitter<any>();
  @Output() clickDetail = new EventEmitter<any>();
  @Output() selectAll = new EventEmitter<number>();
  itemsPerSlide = 5;
  singleSlideOffset = true;
  noWrap = true;
  show = true;
  list = [];

  constructor() { }
  ngOnChanges(changes: import("@angular/core").SimpleChanges): void {
    if ('slides' in changes) {

    }
  }

  ngOnInit() {
    this.showAll();
  }

  select(value: any) {
    this.clickDetail.emit(value);
  }

  selectall(value) {
    this.selectAll.emit(value);
  }

  showAll() {
    if (this.show) {
      this.list = [];
      for (let i = 0; i < 5; i++) {
        if (this.lists[i])
          this.list.push(this.lists[i]);
      }
    } else {
      this.list = this.lists;
    }
    this.show = !this.show;
  }
}