import { Component, EventEmitter, Input, OnInit, Output, OnChanges, ViewEncapsulation } from '@angular/core';
import { Track } from '../../Model/track.model';
import { Song } from '../../share/model/song.model';
import { UserService } from '../../services/user.service';
import { saveMp3 } from '../../share/ultil/blob-util';
import { ToastrService } from 'ngx-toastr';
import { AccountService } from '../../services/common/account.service';

@Component({
  selector: 'app-track',
  templateUrl: './track.component.html',
  styleUrls: ['./track.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class TrackComponent implements OnInit, OnChanges {
  @Input() trackList: Song[];
  @Output() select = new EventEmitter<any>();
  @Output() share = new EventEmitter<any>();
  hover;
  p = 0;
  currenTrack: Track = new Track();
  pagination: boolean;


  constructor(
    private userService: UserService,
    private toastr: ToastrService,
    private account: AccountService
  ) {
  }
  ngOnChanges(changes: import("@angular/core").SimpleChanges): void {
    if ('trackList' in changes) {
      if(this.trackList && this.trackList.length > 21){
        this.pagination = true;
      }
    }
  }

  ngOnInit() {
    this.userService.currenTrack$.subscribe(track => {
      this.currenTrack = track;
    });
  }

  selectTrack(song: Song) {
    this.userService.setTrack([song]);
  }

  hoverOut() {
    this.hover = 0;
  }

  hovers(value: number) {
    this.hover = value;
  }

  download(track: Song) {
    this.userService.getBase64Mp3(track.path).subscribe((res: any) => {
      if (res.code == 500)
        this.toastr.error('Không tìm thấy tệp tin');
      else
        saveMp3(res.data, track.name);
    }, error => {
      this.toastr.error('Có lỗi xảy ra');
    });
  }

  addSongToPlaylist(songId: number) {
    this.account.openAddPlaylist(songId);
  }

  addSongToQuence(song: Song) {
    this.userService.addTrack(song);
  }
}
