import { Component, OnInit, Input, Output, EventEmitter, OnChanges, ViewEncapsulation } from '@angular/core';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ContentComponent implements OnInit, OnChanges {

  @Input() lists: any;
  @Output() selectItem = new EventEmitter<number>();
  @Output() selectAll = new EventEmitter<number>();
  p;
  pagination;

  constructor() { }
  ngOnChanges(changes: import("@angular/core").SimpleChanges): void {
    if ('trackList' in changes) {
      if (this.lists && this.lists.length > 21) {
        this.pagination = true;
      }
    }
  }

  ngOnInit(): void {
  }

  select(value) {
    this.selectItem.emit(value);
  }

  listen(value: number) {
    this.selectAll.emit(value);
  }

}
