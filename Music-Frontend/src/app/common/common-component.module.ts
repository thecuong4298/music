import { NgModule } from '@angular/core';
import { CarouselModule, SlideComponent } from 'ngx-bootstrap/carousel';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { CarouselComponent } from './carousel/carousel.component';
import { TrackComponent } from './track/track.component';
import { TranslateModule } from '@ngx-translate/core';
import { InfoComponent } from './info/info.component';
import { AppModule } from '../app.module';
import { Base64 } from '../model/pipeImage';
import { ContentComponent } from './content/content.component';
import { ImageComponent } from './image/image.component';
import { ImageVComponent } from './image-v/image-v.component';
import { DialogPlaylistComponent } from '../views/Customer/dialog-playlist/dialog-playlist.component';
import { MatFormFieldModule, MatInputModule, MatIconModule, MatButtonModule, MatCheckboxModule } from '@angular/material';
import { NgxPaginationModule } from 'ngx-pagination';
import { ListSelectSongComponent } from './list-select-song/list-select-song.component';
import { SelectAlbumComponent } from './select-album/select-album.component';

@NgModule({
  declarations: [
    CarouselComponent,
    TrackComponent,
    InfoComponent,
    Base64,
    ContentComponent,
    ImageComponent,
    ImageVComponent,
    DialogPlaylistComponent,
    ListSelectSongComponent,
    SelectAlbumComponent
  ],
  imports: [
    FormsModule,
    CommonModule,
    CarouselModule,
    TranslateModule,
    ReactiveFormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatIconModule,
    MatButtonModule,
    NgxPaginationModule,
    MatCheckboxModule
  ],
  exports: [
    CarouselComponent,
    TrackComponent,
    InfoComponent,
    Base64,
    ContentComponent,
    ImageComponent,
    ImageVComponent,
    DialogPlaylistComponent,
    ListSelectSongComponent,
    SelectAlbumComponent
  ],
  providers: [
  ],
  entryComponents: [DialogPlaylistComponent, ListSelectSongComponent, SelectAlbumComponent]
})
export class CommonComponentModule { }
