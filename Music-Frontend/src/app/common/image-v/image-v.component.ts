import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-image-v',
  templateUrl: './image-v.component.html',
  styleUrls: ['./image-v.component.scss']
})
export class ImageVComponent implements OnInit {

  @Input() base64: string;
  
  constructor() { }

  ngOnInit(): void {
  }

}
