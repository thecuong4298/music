import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { UploadModule } from './views/Customer/upload/upload.module';
import { ProfileModule } from './views/Customer/profile/profile.module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { DialogsService } from './common/confirm-dialog/dialogs.service';
import { DialogsModule } from './common/confirm-dialog/dialogs.module';
import { AdminModule } from './views/admin/admin.module';
import { ApiService } from './services/common/api';
import { ErrorInterceptor } from './interceptor/error.interceptor';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { LocationStrategy, HashLocationStrategy, CommonModule, DatePipe } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxAudioPlayerModule } from 'ngx-audio-player';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ToastrModule } from 'ngx-toastr';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

import { AppComponent } from './app.component';

// Import containers
import { DefaultLayoutComponent } from './containers';

import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';

const APP_CONTAINERS = [
  DefaultLayoutComponent
];

import {
  AppAsideModule,
  AppBreadcrumbModule,
  AppHeaderModule,
  AppFooterModule,
  AppSidebarModule,
} from '@coreui/angular';

// Import routing module
import { AppRoutingModule } from './app.routing';

// Import 3rd party components
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ChartsModule } from 'ng2-charts';
import {
  MatSliderModule, MatTableModule, MatFormFieldModule,
  MatInputModule, MatDialogModule, MatButtonToggleModule, MatSelectModule, MatOptionModule, MatAutocompleteModule, MatCheckboxModule
} from '@angular/material';
import { TimeTransform } from './model/pipeCustom';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HighlightDirective } from './helper/highlight.directive';
import { NgCircleProgressModule } from 'ng-circle-progress';
import { MatTooltipModule } from '@angular/material/tooltip';
import { LanguageSelectorComponent } from './language-selector';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { ArtistModule } from './views/Customer/artist/artist.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { LoginMusicComponent } from './views/Customer/login-music/login-music.component';
import { LoginComponent } from './views/Customer/login-music/login/login.component';
import { RegisterMusicComponent } from './views/Customer/login-music/register-music/register-music.component';
import { MatRadioModule } from '@angular/material/radio';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { AdminLoginComponent } from './views/admin/admin-login/admin-login.component';
import { ArtistService } from './services/artist.service';
import { MediaComponent } from './views/Customer/media/media.component';
import { AlbumModule } from './views/Customer/album/album.module';
import { AlbumService } from './services/album.service';
import { TopicService } from './services/topic.service';
import { Base64 } from './model/pipeImage';
import { CommonComponentModule } from './common/common-component.module';
import { CountryService } from './services/country.service';
import { MusicChartModule } from './views/Customer/music-chart/music-chart.module';
import { SearchComponent } from './views/Customer/search/search.component';
import { SearchModule } from './views/Customer/search/search.module';
import { CountdownModule } from 'ngx-countdown';
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    AppAsideModule,
    AppBreadcrumbModule.forRoot(),
    AppFooterModule,
    AppHeaderModule,
    MatRadioModule,
    MatDatepickerModule,
    AppSidebarModule, MatTooltipModule,
    PerfectScrollbarModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    ChartsModule, BrowserAnimationsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    }),
    HttpClientModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatDividerModule,
    MatIconModule,
    MatToolbarModule,
    FlexLayoutModule,
    TranslateModule,
    FormsModule,
    MatButtonModule,
    MatButtonToggleModule,
    CommonModule,
    MatTableModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatInputModule,
    NgxAudioPlayerModule,
    MatDialogModule,
    MatSliderModule,
    MatSelectModule,
    MatOptionModule,
    ArtistModule,
    MatCheckboxModule,
    NgCircleProgressModule.forRoot({
      // set defaults here
      radius: 100,
      outerStrokeWidth: 16,
      innerStrokeWidth: 8,
      outerStrokeColor: '#78C000',
      innerStrokeColor: '#C7E596',
      animationDuration: 300,
    }),
    CarouselModule.forRoot(),
    ToastrModule.forRoot(),
    ModalModule.forRoot(),
    NgxPaginationModule,
    AdminModule,
    DialogsModule,
    AlbumModule,
    ProfileModule,
    CommonComponentModule,
    MusicChartModule,
    MatAutocompleteModule,
    MatOptionModule,
    SearchModule,
    CountdownModule,
    UploadModule,
    MatTooltipModule,
    TooltipModule.forRoot()
  ],
  declarations: [
    AppComponent,
    ...APP_CONTAINERS,
    P404Component,
    P500Component,
    MediaComponent,
    TimeTransform,
    HighlightDirective,
    LanguageSelectorComponent,
    LoginMusicComponent,
    LoginComponent,
    RegisterMusicComponent,
    AdminLoginComponent,
    SearchComponent,
  ],
  providers: [
    ApiService, ArtistService, DialogsService, DatePipe, AlbumService, TopicService, Base64,CountryService,
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
  ],
  bootstrap: [AppComponent],
  entryComponents: [LoginMusicComponent]
})
export class AppModule { }
