import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Import Containers
import { DefaultLayoutComponent } from './containers';

import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';
import { LoginComponent } from './views/Customer/login-music/login/login.component';
import { AdminLoginComponent } from './views/admin/admin-login/admin-login.component';
import { SearchResponComponent } from './views/Customer/search/search-respon/search-respon.component';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },
  {
    path: '404',
    component: P404Component,
    data: {
      title: 'Page 404'
    }
  },
  {
    path: '500',
    component: P500Component,
    data: {
      title: 'Page 500'
    }
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'home',
        loadChildren: () => import('./views/Customer/home/home.module').then(m => m.HomeModule)
      },
      {
        path: 'artist',
        loadChildren: () => import('./views/Customer/artist/artist.module').then(m => m.ArtistModule)
      },
      {
        path: 'genres',
        loadChildren: () => import('./views/Customer/genre/genre.module').then(m => m.GenreModule)
      },
      {
        path: 'admin',
        loadChildren: () => import('./views/admin/admin.module').then(m => m.AdminModule)
      },
      {
        path: 'album',
        loadChildren: () => import('./views/Customer/album/album.module').then(m => m.AlbumModule)
      },
      {
        path: 'topic',
        loadChildren: () => import('./views/Customer/topic/topic.module').then(m => m.TopicModule)
      },
      {
        path: 'profile',
        loadChildren: () => import('./views/Customer/profile/profile.module').then(m => m.ProfileModule)
      },
      {
        path: 'chart',
        loadChildren: () => import('./views/Customer/music-chart/music-chart.module').then(m => m.MusicChartModule)
      },
      {
        path: 'customer/search',
        loadChildren: () => import('./views/Customer/search/search.module').then(m => m.SearchModule)
      },
      {
        path: 'upload',
        loadChildren: () => import('./views/Customer/upload/upload.module').then(m => m.UploadModule)
      },
    ]
  },
  {
    path: 'admin/login', component: AdminLoginComponent
  },
  { path: '**', component: P404Component }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
