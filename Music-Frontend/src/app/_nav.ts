import { INavData } from '@coreui/angular';

export const navItems = [
  {
    name: 'home',
    url: '/home',
    icon: 'icon-drop',
    i18N: 'home'
  },
  {
    name: 'top',
    url: '/chart',
    icon: 'icon-drop',
    i18N: 'top'
  },

  {
    name: 'genres',
    url: '/genres',
    icon: 'icon-drop',
    i18N: 'genres'
  },

  {
    name: 'artists',
    url: '/artist',
    icon: 'icon-drop',
    i18N: 'artists'
  },

  {
    name: 'album',
    url: '/album',
    icon: 'icon-drop',
    i18N: 'album'
  },

  {
    name: 'topic',
    url: '/topic',
    icon: 'icon-drop',
    i18N: 'theme'
  },

  {
    name: 'personal',
    url: '/profile',
    icon: 'icon-drop',
    i18N: 'personal'
  }
];
export const navAdmin = [
  {
    name: 'Quản lý người dùng',
    url: '/admin/management-user',
    icon: 'icon-user',
    i18N: 'top'
  },
  {
    name: 'Quản lý thể loại',
    url: '/admin/management-genre',
    icon: 'fa fa-tasks',
    i18N: 'top'
  },
  {
    name: 'Quản lý chủ đề',
    url: '/admin/management-topic',
    icon: 'fa fa-file-text-o',
    i18N: 'top'
  },
  {
    name: 'Quản lý album',
    url: '/admin/management-album',
    icon: 'fa fa-clone',
    i18N: 'top'
  },
  {
    name: 'Quản lý tin tức',
    url: '/admin/management-slideshow',
    icon: 'fa fa-newspaper-o',
    i18N: 'top'
  },
  {
    name: 'Quản lý ca sỹ',
    url: '/admin/management-artist',
    icon: 'icon-people',
    i18N: 'top'
  },
  {
    name: 'Quản lý bài hát',
    url: '/admin/management-song',
    icon: 'icon-music-tone',
    i18N: 'top'
  },
  {
    name: 'Quản lý upload',
    url: '/admin/management-upload',
    icon: 'fa fa-upload',
    i18N: 'top'
  }
];
