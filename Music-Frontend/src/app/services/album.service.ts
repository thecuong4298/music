import { Injectable } from '@angular/core';
import { ApiService } from './common/api';
import { BehaviorSubject, Observable } from 'rxjs';
import { AlbumModel } from '../model/album.model';

@Injectable({
  providedIn: 'root'
})
export class AlbumService {

  constructor(
    private api: ApiService
  ) { }

  resetForm: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  resetForm$: Observable<boolean> = this.resetForm.asObservable();

  setResetForm(resetForm: any) {
    this.resetForm.next(resetForm);
  }

  getAlbumActive() {
    return this.api.get(`user/getAlbumActive`);
  }

  getSongByAlbum(albumId: number) {
    return this.api.get(`user/getSongByAlbum/${albumId}`);
  }

  getAlbumById(albumId: number) {
    return this.api.get(`user/getAlbumById/${albumId}`);
  }

  get4AlbumRandom() {
    return this.api.get('music/getTop4Album');
  }

  getAllAlbum() {
    return this.api.get('music/getAllAlbum');
  }

  createOrUpdate(album: AlbumModel) {
    const newS = album;
    newS.base64 = null;
    return this.api.post('music/saveAlbum', newS);
  }

  delete(id: number) {
    return this.api.delete('music/deleteAlbum/' + id);
  }

  findByCondition(name: string) {
    return this.api.get('music/getAlbumByCondition?albumName=' + name);
  }

  getAlbumByArtist(artistId: number) {
    return this.api.get(`user/getAlbumByArtist/${artistId}`);
  }
}
