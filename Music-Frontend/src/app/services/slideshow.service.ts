import { BehaviorSubject, Observable } from 'rxjs';
import { SlideShowModel } from './../Model/slideshow.model';
import { HttpClient } from '@angular/common/http';
import { ApiService } from './common/api';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SlideShowService {

  constructor(private api: ApiService) { }

  resetForm: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  resetForm$: Observable<boolean> = this.resetForm.asObservable();

  setResetForm(resetForm: any) {
    this.resetForm.next(resetForm);
  }

  getSlide() {
    return this.api.get('slide/getAll');
  }

  createOrUpdate(slideshow: SlideShowModel) {
    const newS = slideshow;
    newS.base64 = null;
    newS.url = null;
    return this.api.post('slide/save', newS);
  }

  getById(id: number) {
    return this.api.get('slide/getSlideById/' + id);
  }

  delete(id: number) {
    return this.api.delete('slide/deleteSlide/' + id);
  }

  findByCondition(title: string) {
    return this.api.get('slide/getByCondition?slideName=' + title);
  }

//   getAllActive() {
//     return this.api.get('user/getAllGener');
//   }

//   getSongByGenreId(generId: number) {
//     return this.api.get(`user/getSongByGenerId/${generId}`);
//   }
//     get7GenreRandom() {
//     return this.api.get('music/get7GenreRandom');
//   }
}
