import { Injectable } from '@angular/core';
import { ApiService } from './common/api';

@Injectable({
  providedIn: 'root'
})
export class SearchMusicService {

  constructor(private api: ApiService) { }

  search(name: string, maxResult: boolean) {
    return this.api.get('user/search', {name: name, maxResult: maxResult});
  }
}
