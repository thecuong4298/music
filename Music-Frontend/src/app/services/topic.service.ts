import { Injectable } from '@angular/core';
import { ApiService } from './common/api';
import { BehaviorSubject, Observable } from 'rxjs';
import { TopicModel } from '../model/topic.model';

@Injectable({
  providedIn: 'root'
})
export class TopicService {

  constructor(
    private api: ApiService
  ) { }

  resetForm: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  resetForm$: Observable<boolean> = this.resetForm.asObservable();

  setResetForm(resetForm: any) {
    this.resetForm.next(resetForm);
  }

  getById(id: number) {
    return this.api.get('music/getTopicById/' + id);
  }

  getTopicActive() {
    return this.api.get(`music/getTopicActive`);
  }

  getAllTopic() {
    return this.api.get(`music/getAllTopic`)
  }

  getSongByTopic(TopicId: number) {
    return this.api.get(`user/getSongByTopic/${TopicId}`);
  }

  getTopicById(TopicId: number) {
    return this.api.get(`music/getTopicById/${TopicId}`);
  }

  get3TopicRandom() {
    return this.api.get('music/getTop3Topic');
  }

  createOrUpdate(topic: TopicModel, listSong) {
    const id = [];
    if (listSong) {
      listSong.forEach((song) => {
        id.push(song.id);
      });
    }
    const newS = topic;
    newS.base64 = null;
    return this.api.post('music/saveTopic', { 'topic': newS, 'listSongId': id });
  }

  delete(id: number) {
    return this.api.delete('music/deleteTopic/' + id);
  }

  findByCondition(name: string) {
    return this.api.get('music/getTopicByCondition?topicName=' + name);
  }
}
