import { ApiService } from './common/api';
import { Injectable } from '@angular/core';
import { PlaylistModel } from '../model/playlist.model';
import { BehaviorSubject, Observable } from 'rxjs';
import { SongPlaylist } from '../model/songplaylist.model';

@Injectable({
  providedIn: 'root'
})
export class PlaylistService {

  resetForm: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  resetForm$: Observable<boolean> = this.resetForm.asObservable();

  setResetForm(resetForm: any) {
    this.resetForm.next(resetForm);
  }

  constructor(private api: ApiService) { }

  createOrUpdate(playlist: PlaylistModel) {
    const newS = playlist;
    newS.base64 = null;
    return this.api.post('music/createPlaylist', newS);
  }

  getPlaylists(id: number) {
    return this.api.get('music/getPlaylistById/' + id);
  }

  getPlaylistById(id: number) {
    return this.api.get('music/getPlaylist/' + id);
  }

  delete(id: number) {
    return this.api.delete('music/deletePlaylist/' + id);
  }

  deleteSongInPlaylist(idPlaylist: number, idSong: number) {
    return this.api.delete('music/deleteSongInPlaylist?idPlaylist=' + idPlaylist + '&idSong=' + idSong);
  }

  findSongInPlaylistById(id: number) {
    return this.api.get('music/getSongInPlaylist/' + id);
  }

  addSongToPlaylist(songPlaylist: SongPlaylist){
    return this.api.post('user/addSongToPlaylist', songPlaylist);
  }
}
