import { SongUpload } from './../model/song-upload.model';
import { Injectable } from '@angular/core';
import { ApiService } from './common/api';
import { environment } from '../../environments/environment';
import { UserModel } from '../Model/user.model';
import { BehaviorSubject, Observable } from 'rxjs';
import { Track, Quenue } from '../Model/track.model';
import { Song } from '../share/model/song.model';
import { STORAGE_KEY } from '../constants';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  track: BehaviorSubject<Quenue> = new BehaviorSubject<Quenue>(null);
  track$: Observable<Quenue> = this.track.asObservable();
  currenTrack: BehaviorSubject<Track> = new BehaviorSubject<Track>(null);
  currenTrack$: Observable<Track> = this.currenTrack.asObservable();
  resetForm: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  resetForm$: Observable<boolean> = this.resetForm.asObservable();

  setCurrenTrack(track: Track) {
    this.currenTrack.next(track);
  }
  setResetForm(resetForm: any) {
    this.resetForm.next(resetForm);
  }

  setTrackByTrack(track: Track[]) {
    const quenue: Quenue = new Quenue();
    quenue.tracks = track;
    this.track.next(quenue);
  }

  deleteTrack(trackId: number) {
    const tracks = this.track.value.tracks.filter((track: Track) => track.id !== trackId);
    const quenue: Quenue = new Quenue();
    quenue.tracks = tracks;
    localStorage.setItem(STORAGE_KEY.playlist, JSON.stringify(tracks));
    this.track.next(quenue);
  }

  setTrack(songs: Song[]) {
    const quenue: Quenue = new Quenue();
    quenue.reload = true;
    this.addSongToQuenue(songs).subscribe((res: any) => {
      quenue.tracks = res.data;
      localStorage.setItem(STORAGE_KEY.playlist, JSON.stringify(res.data));
      localStorage.setItem('indexSong', '0');
      this.track.next(quenue);
    });
  }

  addTrack(song: Song) {
    const tracks = this.track.value.tracks;
    if (tracks.filter(track => track.id === song.id).length > 0) {
      this.toastr.error('Bài hát đã tồn tại trong danh sách phát');
      return;
    }
    const quenue: Quenue = new Quenue();
    quenue.tracks = tracks;
    this.addSongToQuenue([song]).subscribe((res: any) => {
      tracks.push(res.data[0]);
      localStorage.setItem(STORAGE_KEY.playlist, JSON.stringify(tracks));
      this.track.next(quenue);
    });
  }

  constructor(
    private api: ApiService,
    private toastr: ToastrService
  ) { }

  getAllUser() {
    return this.api.get(`user/getAll`);
  }

  saveUser(user: UserModel) {
    // console.log("=>>>>>>>>>",user);
    const newS = user;
    newS.base64 = null;
    return this.api.post(`user/save`, newS);
  }

  getRoleById(id: number) {
    return this.api.get(`user/getRoleById/${id}`);
  }

  getBase64Mp3(path: string) {
    return this.api.post(`file/getMedia`, path);
  }

  view(id: number) {
    return this.api.get(`user/viewSong/${id}`);
  }

  getUserById(id: number) {
    return this.api.get(`user/getById/${id}`);
  }

  getById(id: number) {
    return this.api.get('user/getUserById/' + id);
  }

  findByCondition(name: string) {
    return this.api.get('user/getByCondition?userName=' + name);
  }
  delete(id: number) {
    return this.api.delete('user/deleteUser/' + id);
  }

  getTopSongByCountry(topNumber: number, countryId: number, isMonth: boolean) {
    return this.api.get('user/getTopSongByCountry', { topNumber: topNumber, countryId: countryId, isMonth: isMonth });
  }

  getAllGenreActive() {
    return this.api.get('user/getAllGenreActive');
  }

  findAllSlideshowActive() {
    return this.api.get('user/findAllSlideshowActive');
  }

  addSongToQuenue(song: Song[]) {
    return this.api.post('user/addSongToQuenue', song);
  }

  reissuePassword(email: string, username: string) {
    return this.api.post(`user/reissuePassword?email=${email}&username=${username}`, null);
  }

  authenPassword(otp: string, username: string) {
    return this.api.post(`user/authenOTP?otp=${otp}&username=${username}`, null);
  }

  saveNewPass(newPass: string, username: string) {
    return this.api.post(`user/saveNewPass?newPass=${newPass}&username=${username}`, null);
  }

  uploadSong(songUpload: SongUpload) {
    return this.api.post(`user/uploadSong`, songUpload);
  }
}
