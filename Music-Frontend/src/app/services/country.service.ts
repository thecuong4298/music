import { Injectable } from '@angular/core';
import { ApiService } from './common/api';

@Injectable({
  providedIn: 'root'
})
export class CountryService {

  constructor(
    private api: ApiService
  ) { }

  getCountryActive() {
    return this.api.get(`user/getCountryActive`);
  }
}
