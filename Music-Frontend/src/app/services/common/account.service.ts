import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { UserToken } from '../../Model/user-token';
import { ApiService } from './api';
import { STORAGE_KEY, END_POINT } from '../../constants';
import { environment } from '../../../environments/environment';
import { MatDialog } from '@angular/material';
import { DialogPlaylistComponent } from '../../views/Customer/dialog-playlist/dialog-playlist.component';
import { LoginMusicComponent } from '../../views/Customer/login-music/login-music.component';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  private currentUserTokenSubject: BehaviorSubject<UserToken> = new BehaviorSubject<UserToken>(null);
  public currentUserToken: Observable<UserToken> = this.currentUserTokenSubject.asObservable();

  constructor(
    private http: HttpClient,
    private router: Router,
    private _api: ApiService,
    private dialog: MatDialog
  ) {
    const strUserToken = localStorage.getItem(STORAGE_KEY.userToken);
    if (strUserToken) {
      this.currentUserTokenSubject.next(JSON.parse(strUserToken));
    }
    // const userFeatures = localStorage.getItem(STORAGE_KEY.features);
    // if (userFeatures) {
    //     this.currentAccessibleFeatures.next(JSON.parse(userFeatures));
    // }
    // const menuItems = localStorage.getItem(STORAGE_KEY.menuItems);
    // if (menuItems) {
    //     this.menuItemsSubject.next(JSON.parse(menuItems));
    // }
  }

  getAvatar() {
    return this._api.post('user/getAvatar', this.currentUserTokenSubject.value.user.pathAvatar);
  }

  public get currentUserValue(): UserToken {
    return this.currentUserTokenSubject.value;
  }

  login(username: string, password: string): Observable<any> {
    // const userToken = JSON.parse(this.datatemp);
    // this.accessToken = userToken.access_token;
    // this.refreshToken = userToken.refresh_token;
    // localStorage.setItem(STORAGE_KEY.userToken, JSON.stringify(userToken));
    // this.currentUserTokenSubject.next(userToken);
    // return this.currentUserToken;
    return this._api.authenticate(END_POINT.authen, username, password)
      .pipe(map((userToken: any) => {
        if (userToken) {
          this.accessToken = userToken.access_token;
          this.refreshToken = userToken.refresh_token;
          localStorage.setItem(STORAGE_KEY.userToken, JSON.stringify(userToken));
          this.currentUserTokenSubject.next(userToken);
        }
        return userToken;
      }));
  }

  logout() {
    localStorage.removeItem(STORAGE_KEY.userToken);
    localStorage.removeItem(STORAGE_KEY.access_token);
    this.currentUserTokenSubject.next(null);
    if (this.router.url.indexOf('admin') != -1) {
      this.router.navigateByUrl('admin/login');
    } else {
      this.router.navigateByUrl('/home');
    }

  }

  getPasswordResetToken(formGroup): Observable<HttpEvent<any>> {
    return this.http.post<any>(`${environment.API_URL}/api/reset-password`, formGroup);
  }

  resetPassword(formGroup, id: number, token: string) {
    return this.http.put<any>(`${environment.API_URL}/api/reset-password?id=${id}&token=${token}`, formGroup);
  }

  changePassword(formGroup): Observable<HttpEvent<any>> {
    return this.http.patch<any>(`${environment.API_URL}/api/change-password`, formGroup);
  }

  get accessToken(): any {
    return localStorage.getItem(STORAGE_KEY.access_token);
  }
  set accessToken(value) {
    localStorage.setItem(STORAGE_KEY.access_token, value);
  }
  get refreshToken(): any {
    return localStorage.getItem(STORAGE_KEY.refresh_token);
  }

  set refreshToken(value) {
    localStorage.setItem(STORAGE_KEY.refresh_token, value);
  }

  public doRefreshToken() {
    return this._api.doRefreshToken(END_POINT.authen, this.refreshToken);
  }

  openAddPlaylist(songId: number) {
    if (this.currentUserValue) {
      this.dialog.open(DialogPlaylistComponent, {
        width: '25vw',
        panelClass: 'myapp-background-dialog',
        data: { songId: songId }
      });
    } else {
      const dialogLogin = this.dialog.open(LoginMusicComponent);
      dialogLogin.afterClosed().subscribe(result => {
        if (this.currentUserValue) {
          this.dialog.open(DialogPlaylistComponent, {
            width: '35vw',
            panelClass: 'myapp-background-dialog',
            data: { songId: songId }
          });
        }
      });
    }
  }
}
