import { BehaviorSubject, Observable } from 'rxjs';
import { GenreModel } from '../model/genre.model';
import { HttpClient } from '@angular/common/http';
import { ApiService } from './common/api';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GenreService {

  constructor(private api: ApiService) { }

  resetForm: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  resetForm$: Observable<boolean> = this.resetForm.asObservable();

  setResetForm(resetForm: any) {
    this.resetForm.next(resetForm);
  }

  getGenres() {
    return this.api.get('music/getAll');
  }

  createOrUpdate(genre: GenreModel) {
    const newS = genre;
    newS.base64 = null;
    return this.api.post('music/save', newS);
  }

  getById(id: number) {
    return this.api.get('music/getGenreById/' + id);
  }

  delete(id: number) {
    return this.api.delete('music/deleteGenre/' + id);
  }

  findByCondition(name: string) {
    return this.api.get('music/getByCondition?genreName=' + name);
  }

  getAllActive() {
    return this.api.get('user/getAllGenreActive');
  }

  getSongByGenreId(generId: number) {
    return this.api.get(`user/getSongByGenerId/${generId}`);
  }

  get7GenreRandom() {
    return this.api.get('music/get7GenreRandom');
  }
}
