import { ArtistModel } from './../model/artist.model';
import { Injectable } from '@angular/core';
import { ApiService } from './common/api';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ArtistService {

  constructor(
    private api: ApiService
  ) { }

  resetForm: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  resetForm$: Observable<boolean> = this.resetForm.asObservable();

  chooseArtist: BehaviorSubject<ArtistModel> = new BehaviorSubject<ArtistModel>(null);
  chooseArtist$: Observable<ArtistModel> = this.chooseArtist.asObservable();

  setResetForm(resetForm: any) {
    this.resetForm.next(resetForm);
  }

  setChooseArtist(chooseArtist: any) {
    this.chooseArtist.next(chooseArtist);
  }

  getAllArtistByCountry() {
    return this.api.get(`user/getAllArtistByCountry`);
  }

  getSongByArtist(artistId: number) {
    return this.api.get(`user/getSongByArtist/${artistId}`);
  }

  getTop4Artist() {
    return this.api.get(`music/getTop4`);
  }

  getArtistById(artistId: number) {
    return this.api.get(`user/getArtistById/${artistId}`);
  }

  getAllArtist(){
    return this.api.get(`music/getAllArtist`)
  }

  createOrUpdate(artist: ArtistModel) {
    const newS = artist;
    newS.base64 = null;
    return this.api.post('music/saveArtist', newS);
  }

  delete(id: number) {
    return this.api.delete('music/deleteArtist/' + id);
  }

  findByCondition(name: string) {
    return this.api.get('music/getArtistByCondition?artistName=' + name);
  }
}
