import { Injectable } from '@angular/core';
import { ApiService } from './common/api';

@Injectable({
  providedIn: 'root'
})
export class MusicService {

  constructor(
    private api: ApiService
  ) { }

  abc() {
    this.api.post('');
  }

}
