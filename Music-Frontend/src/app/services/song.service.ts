import { ApiService } from './common/api';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Song } from '../share/model/song.model';

@Injectable({
  providedIn: 'root'
})
export class SongService {

  constructor(private api: ApiService) { }

  resetForm: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  resetForm$: Observable<boolean> = this.resetForm.asObservable();

  setResetForm(resetForm: any) {
    this.resetForm.next(resetForm);
  }

  getSongsByUserId(id: number) {
    return this.api.get('user/getSongsByUserId/' + id);
  }

  revokeUpload(id: number) {
    return this.api.delete('user/revokeUpload/' + id);
  }

  getAllSong() {
    return this.api.get('music/getAllSong');
  }

  createOrUpdate(song: Song, singerId: number[]) {
    const newS = song;
    newS.base64 = null;
    newS.songBase64 = null;
    return this.api.post(`music/saveSong`, {'song': newS, 'listArtistId' : singerId});
  }

  findByCondition(name: string) {
    return this.api.get('music/getSongByCondition?songName=' + name);
  }

  delete(id: number) {
    return this.api.delete('music/deleteSong/' + id);
  }

  getSongById(id) {
    return this.api.get(`music/getSongById/${id}`);
  }

  getAllSongUploadByCustomer() {
    return this.api.get(`music/getAllSongUploadByCustomer`);
  }

  getDetailSongUpload(idSong: number) {
    return this.api.get(`music/getDetailSongUpload/${idSong}`);
  }

  acceptOrRejectSongUpload(songId: number, type: number) {
    return this.api.post(`music/acceptOrRejectSongUpload?songId=${songId}&type=${type}`, null);
  }

  getArtistBySongId(id) {
    return this.api.get(`music/getArtistBySongId/${id}`);
  }
}
