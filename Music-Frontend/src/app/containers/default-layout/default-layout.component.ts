import { DomSanitizer } from '@angular/platform-browser';
import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, ViewEncapsulation } from '@angular/core';
import { AuthGuard } from './../../share/auth/auth.guard';
import { navItems, navAdmin } from '../../_nav';
import { Track } from 'ngx-audio-player';
import { MusicService } from '../../services/music.service';
import { TranslateService } from '@ngx-translate/core';
import { config } from '../../config/application.config';
import { UserService } from '../../services/user.service';
import { UserModel } from '../../model/user.model';
import { MatDialog } from '@angular/material';
import { LoginMusicComponent } from '../../views/Customer/login-music/login-music.component';
import { AccountService } from '../../services/common/account.service';
import { UserToken } from '../../model/user-token';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html',
  styleUrls: ['default-layout.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DefaultLayoutComponent implements OnInit, AfterViewInit {
  @ViewChild('header', { read: ElementRef }) header: ElementRef;
  @ViewChild('appSidebar', { read: ElementRef }) sidebar: ElementRef;
  @ViewChild('body', { read: ElementRef }) body: ElementRef;

  hideText = true;
  public sidebarMinimized = true;
  public nav = navItems;
  logged;
  user: UserToken;
  isAdmin = false;
  defaultLink = '/home';
  urlImage;

  constructor(
    private translate: TranslateService,
    private domSanitizer: DomSanitizer,
    private dialog: MatDialog,
    private accountService: AccountService,
    private router: Router
  ) { }
  icon = [{ icon: 'home' }, { icon: 'sort' }, { icon: 'category' }, { icon: 'stars' }, { icon: 'album' }, { icon: 'movie_filter' }, { icon: 'library_music' }];

  ngAfterViewInit(): void {
    const button = this.header.nativeElement.getElementsByClassName('navbar-toggler d-none d-md-block');
    button[0].style.width = '0px';
    button[0].style = 'min-width: 0px; width: 0px';
    this.checkAdmin();
  }

  changeIcon() {
    const navitem = this.sidebar.nativeElement.getElementsByClassName('nav-icon icon-drop ng-star-inserted');
    this.sidebar.nativeElement.getElementsByClassName('sidebar-minimizer')[0].style = 'background: #1f264b';
    let i = 0;
    setTimeout(() => {
      while (navitem[0]) {
        navitem[0].parentNode.parentNode.style = 'padding-top: 20px; padding-bottom: 20px;';
        navitem[0].innerHTML = this.icon[i].icon;
        i++;
        navitem[0].style = ` color: white;`;
        navitem[0].className = 'nav-icon material-icons ng-star-inserted';
      }
    }, 200);
  }

  ngOnInit(): void {
    this.nav.forEach(item => {
      item.name = this.translate.instant(config.nav + item.i18N);
    });

    this.accountService.currentUserToken.subscribe(user => {
      this.user = user;
      if (this.user) {
        this.accountService.getAvatar().subscribe((res:any) => {
          this.urlImage = res.data;
        });
      } else {
        this.urlImage = null;
      }
    });
  }

  toggleMinimize(e) {
    this.sidebarMinimized = e;
    if (e) {
      return this.hideText = e;
    }
    setTimeout(() => {
      this.hideText = e;
    }, 200);
  }

  login() {
    this.dialog.open(LoginMusicComponent, {
      data: null
    });
  }
  logout() {
    setTimeout(() => {
      this.accountService.logout();
    }, 200);
  }

  checkAdmin() {
    this.user = this.accountService.currentUserValue;
    if (this.user) {
      if (this.user.authorities[0].authority === 'ROLE_ADMIN') {
        if (this.router.url.indexOf('admin') !== -1) {
          this.configAdmin();
        } else {
          this.logout();
          this.changeIcon();
        }
      } else {
        this.changeIcon();
      }
    } else {
      if (this.router.url.indexOf('admin') !== -1) {
        this.router.navigateByUrl('admin/login');
      }
      this.changeIcon();
    }
  }

  configAdmin() {
    this.isAdmin = true;
    this.nav = navAdmin;
    this.sidebarMinimized = false;
    this.defaultLink = '/admin/management-user';
  }
}
