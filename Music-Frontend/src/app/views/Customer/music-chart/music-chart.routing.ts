
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MusicChartComponent } from './music-chart.component';

const routes: Routes = [
  {
    path: '',
    component: MusicChartComponent,
    data: {
      title: 'Trang chủ'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MusicChartRoutingModule { }
