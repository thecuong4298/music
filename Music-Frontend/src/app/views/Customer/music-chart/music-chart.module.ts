import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommonComponentModule } from '../../../common/common-component.module';
import { MusicChartComponent } from './music-chart.component';
import { MusicChartRoutingModule } from './music-chart.routing';
import { HomeModule } from '../home/home.module';
import { TranslateModule } from '@ngx-translate/core';
import { NgxPaginationModule } from 'ngx-pagination';



@NgModule({
  declarations: [
    MusicChartComponent
  ],
  imports: [
    CommonModule,
    CommonComponentModule,
    MusicChartRoutingModule,
    HomeModule,
    TranslateModule,
    NgxPaginationModule,
  ]
})
export class MusicChartModule { }
