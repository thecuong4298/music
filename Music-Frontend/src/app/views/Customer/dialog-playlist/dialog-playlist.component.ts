import { AccountService } from './../../../services/common/account.service';
import { Component, OnInit, ViewEncapsulation, Inject } from '@angular/core';
import { PlaylistService } from '../../../services/playlist.service';
import { UserToken } from '../../../model/user-token';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { PlaylistModel } from '../../../model/playlist.model';
import { SongPlaylist } from '../../../model/songplaylist.model';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-dialog-playlist',
  templateUrl: './dialog-playlist.component.html',
  styleUrls: ['./dialog-playlist.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DialogPlaylistComponent implements OnInit {

  user: UserToken;
  form: FormGroup;
  text;
  allPlaylist: PlaylistModel[] = [];
  playlists: PlaylistModel[] = [];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private playlistService: PlaylistService,
    private accountService: AccountService,
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<DialogPlaylistComponent>,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.getUser();
    this.form = this.fb.group(
      { name: ['', Validators.required] }
    );
  }

  getUser() {
    this.user = this.accountService.currentUserValue;
    this.getPlayList();
  }

  getPlayList() {
    this.playlistService.getPlaylists(this.user.user.id).subscribe((res: any) => {
      this.playlists = res.data;
      this.allPlaylist = res.data;
    });
  }

  hasError(controlName: string, errorName: string) {
    return this.form.controls[controlName].hasError(errorName);
  }

  applyFilter(filterValue) {
    if (filterValue && filterValue.trim().length > 0) {
      this.playlists = this.allPlaylist.filter(item => item.name.indexOf(filterValue.trim()) != -1);
    } else {
      this.playlists = this.allPlaylist;
    }
  }

  addPlaylist() {
    if (!this.form.invalid && this.text) {
      const playlist = new PlaylistModel();
      playlist.name = this.form.controls.name.value.trim();
      playlist.status = 1;
      playlist.userId = this.user.user.id.toString();
      this.playlistService.createOrUpdate(playlist).subscribe((res: any) => {
        this.toastr.success('Tạo mới và thêm vào playlist thành công');
      });
    }
  }
  addSong(playlistId: number) {
    const songPlaylist = new SongPlaylist();
    songPlaylist.playlistId = playlistId;
    songPlaylist.songId = this.data.songId;
    songPlaylist.status = 1;
    console.log(songPlaylist);

    this.playlistService.addSongToPlaylist(songPlaylist).subscribe((res: any) => {
      if (res.code == 409) {
        this.toastr.error(res.data);
      } else {
        this.toastr.success('Thêm vào playlist thành công');
      }
      this.dialogRef.close();
    });
  }
}
