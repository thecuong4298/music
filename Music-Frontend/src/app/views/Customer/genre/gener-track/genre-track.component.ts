import { Component, OnInit } from '@angular/core';
import { GenreService } from '../../../../services/genre.service';
import { ActivatedRoute } from '@angular/router';
import { Song } from '../../../../share/model/song.model';
import { GenreModel } from '../../../../model/genre.model';
import { UserService } from '../../../../services/user.service';

@Component({
  selector: 'app-genre-track',
  templateUrl: './genre-track.component.html',
  styleUrls: ['./genre-track.component.scss']
})
export class GenreTrackComponent implements OnInit {

  songs: Song[] = [];
  genre: GenreModel = new GenreModel();

  constructor(
    private genreService: GenreService,
    private route: ActivatedRoute,
    private userService: UserService
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe(param => {
      this.getSong(param.id);
      this.getGenre(param.id);
    });
  }

  getSong(generId) {
    this.genreService.getSongByGenreId(generId).subscribe((res: any) => {
      this.songs = res.data;
      
    });
  }

  getGenre(genreId) {
    this.genreService.getById(genreId).subscribe((res: any) => {
      this.genre = res.data;
    });
  }

  listenAll() {
    this.userService.setTrack(this.songs);
  }
}
