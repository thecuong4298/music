import { NgModule } from '@angular/core';
import { GenerRoutingModule } from './genre.routing';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { CommonComponentModule } from '../../../common/common-component.module';
import { GenreComponent } from './genre.component';
import { GenreTrackComponent } from './gener-track/genre-track.component';

@NgModule({
  declarations: [
    GenreComponent,
    GenreTrackComponent
  ],
  imports: [
    FormsModule,
    CommonModule,
    GenerRoutingModule,
    CommonComponentModule
  ],
  providers: [
  ],
  entryComponents: []
})
export class GenreModule { }
