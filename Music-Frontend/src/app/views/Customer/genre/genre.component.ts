import { Component, OnInit } from '@angular/core';
import { GenreService } from '../../../services/genre.service';
import { GenreModel } from '../../../model/genre.model';
import { Router } from '@angular/router';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-genre',
  templateUrl: './genre.component.html',
  styleUrls: ['./genre.component.scss']
})
export class GenreComponent implements OnInit {

  genres: GenreModel[] = [];

  constructor(
    private route: Router,
    private genreService: GenreService,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.genreService.getAllActive().subscribe((res: any) => {
      this.genres = res.data;
    });
  }

  select(id) {
    this.route.navigateByUrl(`genres/${id}`);
  }

  selectAll(generId) {
    this.genreService.getSongByGenreId(generId).subscribe((res: any) => {
      this.userService.setTrack(res.data);
    });
  }
}
