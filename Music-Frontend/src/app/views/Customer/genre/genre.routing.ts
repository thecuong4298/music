
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GenreComponent } from './genre.component';
import { GenreTrackComponent } from './gener-track/genre-track.component';

const routes: Routes = [
  {
    path: '',
    component: GenreComponent,
    data: {
      title: 'Trang chủ'
    }
  },
  {
    path: ':id',
    component: GenreTrackComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GenerRoutingModule { }
