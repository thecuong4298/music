import { Song } from './../../../../share/model/song.model';
import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PlaylistModel } from '../../../../model/playlist.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { PlaylistService } from '../../../../services/playlist.service';
import { ToastrService } from 'ngx-toastr';
import { DialogsService } from '../../../../common/confirm-dialog/dialogs.service';
import { AccountService } from '../../../../services/common/account.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { GenreCreateComponent } from '../../../admin/management-genre/genre-create/genre-create.component';
import { environment } from '../../../../../environments/environment';
import { UserService } from '../../../../services/user.service';

@Component({
  selector: 'app-playlist-edit',
  templateUrl: './playlist-edit.component.html',
  styleUrls: ['./playlist-edit.component.scss']
})
export class PlaylistEditComponent implements OnInit {

  createForm: FormGroup;
  playlist: PlaylistModel;
  uploadEmpty: boolean;
  songs: Song[];
  imgPath: any;
  fileList: FileList;
  checkUploadSucces: boolean;
  messageErrorFile = '';
  userLogged;

  constructor(
    private fb: FormBuilder,
    private http: HttpClient,
    private playlistService: PlaylistService,
    private toastr: ToastrService,
    private dialogSv: DialogsService,
    private accountService: AccountService,
    private userService: UserService,
    public dialogRef: MatDialogRef<GenreCreateComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    this.playlist = data.playlistEdit;
    this.songs = data.listSong;
  }

  ngOnInit(): void {
    this.userLogged = this.accountService.currentUserValue;
    this.imgPath = null;
    this.checkUploadSucces = false;
    this.uploadEmpty = true;
    this.createForm = this.fb.group(
      {
        name: [this.playlist.name, Validators.required],
      }
    );
  }

  selectFile(fileInputEvent: any) {
    this.messageErrorFile = '';
    const nameFile = fileInputEvent.target.files[0].name;
    let checkFile;
    const formatFile = nameFile.substring(nameFile.length - 3, nameFile.length);
    if (formatFile) {
      if (formatFile !== 'jpg' && formatFile !== 'png') {
        checkFile = true;
        this.uploadEmpty = false;
        this.messageErrorFile = 'Ảnh không đúng định dạng';
      } else {
        this.messageErrorFile = '';
        this.uploadEmpty = true;
      }
    }

    if (checkFile) {
      return;
    }

    if (nameFile.indexOf('.jpg') != -1 || nameFile.indexOf('.png') != -1) {
      const reader = new FileReader();
      reader.readAsDataURL(fileInputEvent.target.files[0]);
      reader.onload = (_event) => {
        this.imgPath = reader.result;
        this.uploadFile(fileInputEvent.target.files[0]);
      };
    } else {
      this.imgPath = null;
    }
  }

  hasError(controlName: string, errorName: string) {
    return this.createForm.controls[controlName].hasError(errorName);
  }

  saveGenre() {
    this.getValueFromForm();
    this.dialogSv.confirm('', 'Bạn có chắc muốn lưu không?', 'Có', 'Không').subscribe(next => {
      if (next) {
        this.playlistService.createOrUpdate(this.playlist).subscribe((data: any) => {
          if (data.code === 200) {
            this.showSuccess('Thêm mới thành công');
            this.dialogRef.close();
            this.playlistService.setResetForm(true);
          }
        }, (err) => {
          this.showError('Có lỗi không xác định xảy ra');
        });
      }
    });
  }

  deleteSong(songId: number) {
    console.log(songId);
    console.log(this.playlist.id);
    this.dialogSv.confirm('', 'Bạn có chắc muốn xóa không?', 'Có', 'Không').subscribe(next => {
      if (next) {
        this.playlistService.deleteSongInPlaylist(this.playlist.id, songId).subscribe((data: any) => {
          if (data.code === 200) {
            this.showSuccess('Xóa thành công');
            this.playlistService.setResetForm(true);
            this.songs = this.songs.filter(item => item.id !== songId);
          } else {
            this.showError(data.errors);
          }
        }, (err) => {
          this.showError('Có lỗi không xác định xảy ra');
        });
      }
    });

  }

  getValueFromForm() {
    this.playlist.name = this.createForm.controls.name.value.trim();
    this.playlist.userId = this.userLogged.user.id;
    this.playlist.status = 1;
  }

  uploadFile(fileUpload: any) {
    const file: File = fileUpload;
    const formData: FormData = new FormData();
    formData.append('file', file);
    const headers = new HttpHeaders();
    headers.append('Content-type', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    const options = { headers: headers };
    this.http.post(environment.API_URL + '/file/uploadReport/genre', formData, options)
      .subscribe(
        (data: any) => {
          if (data.code === 200) {
            this.playlist.base64 = data.data;
            this.checkUploadSucces = true;
          } else {
            this.checkUploadSucces = false;
          }
        },
        err => (this.showError('Có lỗi không xác định xảy ra')),
      );
  }

  showSuccess(message: string) {
    this.toastr.success(message);
  }

  showError(message: string) {
    this.toastr.error(message);
  }

  close() {
    this.dialogRef.close();
  }

  selectTrack(song: Song) {
    this.userService.setTrack([song]);
  }

}
