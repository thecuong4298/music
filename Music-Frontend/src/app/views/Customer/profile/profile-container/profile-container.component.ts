import { SongService } from './../../../../services/song.service';
import { Song } from './../../../../share/model/song.model';
import { DialogsService } from './../../../../common/confirm-dialog/dialogs.service';
import { PlaylistService } from './../../../../services/playlist.service';
import { STORAGE_KEY } from './../../../../constants/index';
import { UserService } from './../../../../services/user.service';
import { ToastrService } from 'ngx-toastr';
import { environment } from './../../../../../environments/environment.prod';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { UserModel } from '../../../../model/user.model';
import { DomSanitizer } from '@angular/platform-browser';
import { LoginMusicComponent } from './../../login-music/login-music.component';
import { MatDialog } from '@angular/material';
import { AccountService } from './../../../../services/common/account.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PlaylistModel } from '../../../../model/playlist.model';

@Component({
  selector: 'app-profile-container',
  templateUrl: './profile-container.component.html',
  styleUrls: ['./profile-container.component.scss']
})
export class ProfileContainerComponent implements OnInit {

  userLogged;
  listSong: Song[] = [];
  userProfile: UserModel;
  uploadEmpty: boolean;
  messageErrorFile: string;
  displayName: string;
  imgPath: any;
  listPlaylist: PlaylistModel[];


  constructor(
    private accountService: AccountService,
    private dialog: MatDialog,
    private domSanitizer: DomSanitizer,
    private http: HttpClient,
    private toastr: ToastrService,
    private userService: UserService,
    private playlistService: PlaylistService,
    private dialogSv: DialogsService,
    private songService: SongService
  ) { }

  ngOnInit(): void {
    this.uploadEmpty = false;
    this.userLogged = this.accountService.currentUserValue;

    this.playlistService.resetForm$.subscribe(data => {
      if (data && this.userLogged) {
        this.getPlaylists();
      }
    });

    if (!this.userLogged) {
      const dialogLogin = this.dialog.open(LoginMusicComponent, {
        data: null
      });
      dialogLogin.afterClosed().subscribe(result => {
        if (this.accountService.currentUserValue) {
          this.userLogged = this.accountService.currentUserValue;
          this.getUser();
          this.showSuccess('Đăng nhập thành công');
        }
      });
    } else {
      this.getUser();
    }
  }

  getUser() {
    this.userService.getUserById(this.accountService.currentUserValue.user.id).subscribe(
      (data: any) => {
        if (data.code === 200) {
          this.userProfile = data.data;
          this.displayName = data.data.fullName;
          this.base64(data.data);
          this.getPlaylists();
          this.getSongsByUserId(this.userLogged.user.id);
        } else {
          this.showError(data.message);
        }
      }, err => {
        this.showError('Có lỗi không xác định xảy ra');
      }
    );
  }

  getPlaylists() {
    this.playlistService.getPlaylists(this.userLogged.user.id).subscribe((data: any) => {
      if (data.code === 200) {
        this.listPlaylist = data.data;
        this.listPlaylist.forEach(element => {
          this.base64(element);
        });
      } else {
        this.listPlaylist = [];
      }
    });
  }

  base64(user: any) {
    if (!user.base64) {
      user.url = 'assets/img/common/default-image.jpg';
    } else {
      user.url = this.domSanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' + user.base64);
    }
  }

  selectFile(fileInputEvent: any) {
    this.messageErrorFile = '';
    this.userProfile.pathAvatar = fileInputEvent.target.files[0].name;
    const formatFile = this.userProfile.pathAvatar.substring(this.userProfile.pathAvatar.length - 3, this.userProfile.pathAvatar.length);
    let checkFile;
    if (formatFile !== 'jpg' && formatFile !== 'png') {
      checkFile = true;
      this.uploadEmpty = false;
      this.messageErrorFile = 'Ảnh không đúng định dạng';
    } else {
      this.messageErrorFile = '';
      this.uploadEmpty = true;
    }

    if (checkFile) {
      return;
    }

    if (this.userProfile.pathAvatar.indexOf('.jpg') != -1 || this.userProfile.pathAvatar.indexOf('.png') != -1) {
      const reader = new FileReader();
      reader.readAsDataURL(fileInputEvent.target.files[0]);
      reader.onload = (_event) => {
        this.imgPath = reader.result;
        this.uploadFile(fileInputEvent.target.files[0]);
      };
    } else {
      this.imgPath = null;
    }
  }

  uploadFile(fileUpload: any) {
    const file: File = fileUpload;
    const formData: FormData = new FormData();
    formData.append('file', file);
    const headers = new HttpHeaders();
    headers.append('Content-type', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    const options = { headers: headers };
    this.http.post(environment.API_URL + '/file/uploadReport/user', formData, options)
      .subscribe(
        (data: any) => {
          if (data.code === 200) {
            this.userProfile.pathAvatar = data.data;
          } else {
          }
        },
        () => (this.showError('Có lỗi không xác định xảy ra'))
      );
  }

  showSuccess(message: string) {
    this.toastr.success(message);
  }

  showError(message: string) {
    this.toastr.error(message);
  }

  saveInfo(event) {
    console.log(event);
    if (event) {
      event.password = null;
      this.userService.saveUser(event).subscribe(
        (data: any) => {
          console.log(data);
          
          if (data.code === 200) {
            console.log(data.data);
            
            this.userProfile = data.data;
            this.displayName = data.data.fullName;
            // this.base64(data.data);
            this.showSuccess('Cập nhật thành công');
          } else {
            this.showError(data.errors);
          }
        }, err => {
          this.showError('Có lỗi không xác định xảy ra');
        }
      );
    }
  }

  delete(genreId) {
    this.dialogSv.confirm('', 'Bạn có chắc muốn xóa không?', 'Có', 'Không').subscribe(next => {
      if (next) {
        this.playlistService.delete(genreId).subscribe((data: any) => {
          if (data.code === 200) {
            this.showSuccess('Xóa thành công');
            this.playlistService.setResetForm(true);
          }
        }, (err) => {
          this.showError('Có lỗi không xác định xảy ra');
        });
      }
    });
  }

  getSongsByUserId(idUser) {
    this.songService.getSongsByUserId(idUser).subscribe(
      (data: any) => {
        if (data.code === 200) {
          this.listSong = data.data;
          this.listSong.forEach(element => {
            this.base64(element);
          });
        } else {
          this.listSong = [];
        }
      }, () => {
        this.showError('Có lỗi không xác định xảy ra');
      }
    );
  }

  revoke(event) {
    this.dialogSv.confirm('', 'Bạn có chắc muốn thu hồi không?', 'Có', 'Không').subscribe(next => {
      if (next) {
        this.songService.revokeUpload(event).subscribe((data: any) => {
          if (data.code === 200) {
            this.showSuccess('Thu hồi thành công');
            this.playlistService.setResetForm(true);
            this.getSongsByUserId(this.userLogged.user.id);
          } else {
            this.showError(data.errors);
          }
        }, (err) => {
          this.showError('Có lỗi không xác định xảy ra');
        });
      }
    });
  }

}
