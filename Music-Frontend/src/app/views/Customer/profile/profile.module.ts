import { CountdownModule } from 'ngx-countdown';
import { MatRadioModule } from '@angular/material/radio';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { ProfileContainerComponent } from './profile-container/profile-container.component';
import { ProfileRoutingModule } from './profile.routing';
import { ProfilePlaylistComponent } from './profile-playlist/profile-playlist.component';
import { ProfileUploadComponent } from './profile-upload/profile-upload.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileAccountComponent } from './profile-account/profile-account.component';
import { MatInputModule, MatButtonModule, MatFormFieldModule, MatTableModule, MatSelectModule, MatOptionModule, MatTabsModule, MatNativeDateModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { PlaylistCreateComponent } from './playlist-create/playlist-create.component';
import { PlaylistEditComponent } from './playlist-edit/playlist-edit.component';
import { AuthenPasswordComponent } from './authen-password/authen-password.component';

@NgModule({
  declarations: [
    ProfileContainerComponent,
    ProfileUploadComponent,
    ProfilePlaylistComponent,
    ProfileUploadComponent,
    ProfileAccountComponent,
    PlaylistCreateComponent,
    PlaylistEditComponent,
    AuthenPasswordComponent,
  ],
  imports: [
    CommonModule,
    MatInputModule,
    MatButtonModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
    NgxPaginationModule,
    MatSelectModule,
    MatOptionModule,
    MatTabsModule,
    ProfileRoutingModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatMomentDateModule,
    MatInputModule,
    MatRadioModule,
    CountdownModule
  ],
  entryComponents: [PlaylistCreateComponent, PlaylistEditComponent, AuthenPasswordComponent]
})
export class ProfileModule { }
