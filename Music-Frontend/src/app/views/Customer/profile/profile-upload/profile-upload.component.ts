import { Song } from './../../../../share/model/song.model';
import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-profile-upload',
  templateUrl: './profile-upload.component.html',
  styleUrls: ['./profile-upload.component.scss']
})
export class ProfileUploadComponent implements OnInit {

  @Input() listSong: Song[];
  @Output() revokeUpload = new EventEmitter<any>();
  p;
  constructor() { }

  ngOnInit(): void {
  }

  revoke(idSong) {
    this.revokeUpload.emit(idSong);
  }

}
