import { LoginMusicComponent } from './../../login-music/login-music.component';
import { Router } from '@angular/router';
import { AccountService } from './../../../../services/common/account.service';
import { CommonValidator } from './../../../../validator/common.validator';
import { ToastrService } from 'ngx-toastr';
import { UserService } from './../../../../services/user.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MatDialog } from '@angular/material';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-authen-password',
  templateUrl: './authen-password.component.html',
  styleUrls: ['./authen-password.component.scss']
})
export class AuthenPasswordComponent implements OnInit {

  createForm: FormGroup;
  authenForm: FormGroup;
  newPassForm: FormGroup;
  username: string;

  constructor(
    public dialogRef: MatDialogRef<AuthenPasswordComponent>,
    private fb: FormBuilder,
    private userService: UserService,
    private toastr: ToastrService,
    private accountService: AccountService,
    private router: Router,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.createForm = this.fb.group(
      {
        name: ['', Validators.required],
        email: ['', [Validators.required, Validators.pattern('^[a-z0-9._,]+@\\w+([\\.-]?\\w+)*(\\.\\w{2,})+$')]],
      }
    );
  }

  createAuthenForm() {
    this.authenForm = this.fb.group(
      {
        otp: ['', Validators.required],
      }
    );
  }

  createNewPassForm() {
    this.newPassForm = this.fb.group(
      {
        newPass: ['', Validators.required],
        newPassConfirm: ['', Validators.required],
      }, { validator: CommonValidator.checkPasswords('newPass', 'newPassConfirm') }
    );
  }

  close() {
    this.dialogRef.close();
  }

  sendOtp() {
    if (!this.createForm.invalid) {
      const username = this.createForm.controls.name.value.trim();
      const email = this.createForm.controls.email.value.trim();

      this.userService.reissuePassword(email, username).subscribe(
        (data: any) => {
          if (data.code === 200) {
            this.username = username;
            this.createForm = null;
            this.createAuthenForm();
          } else {
            this.showError(data.errors);
          }
        }, () => {
          this.showError('Có lỗi không xác định xảy ra');
        }
      );
    }
  }

  authenOTP() {
    if (!this.authenForm.invalid && this.username) {
      const otp = this.authenForm.controls.otp.value.trim();

      this.userService.authenPassword(otp, this.username).subscribe(
        (data: any) => {
          if (data.code === 200) {
            this.authenForm = null;
            this.createNewPassForm();
          } else {
            this.showError(data.errors);
          }
        }, () => {
          this.showError('Có lỗi không xác định xảy ra');
        }
      );
    }
  }

  saveNewPass() {
    if (!this.newPassForm.invalid && this.username) {
      const pass = this.newPassForm.controls.newPass.value.trim();

      this.userService.saveNewPass(pass, this.username).subscribe(
        (data: any) => {
          if (data.code === 200) {
            this.dialogRef.close();
            this.showSuccess('Lưu mật khẩu mới thành công');
            this.accountService.logout();
            this.router.navigateByUrl('/home');
            this.dialog.open(LoginMusicComponent, {
              data: null
            });
          } else {
            this.showError(data.errors);
          }
        }, () => {
          this.showError('Có lỗi không xác định xảy ra');
        });
    }
  }

  checkValidForm(errType: string) {
    if (this.newPassForm.controls.newPass.valid && this.newPassForm.controls.newPassConfirm.valid) {
      return this.newPassForm.hasError(errType);
    }
  }

  hasErrorCreate(controlName: string, errorName: string) {
    return this.createForm.controls[controlName].hasError(errorName);
  }

  hasErrorAuthen(controlName: string, errorName: string) {
    return this.authenForm.controls[controlName].hasError(errorName);
  }

  hasErrorNewPass(controlName: string, errorName: string) {
    return this.newPassForm.controls[controlName].hasError(errorName);
  }

  showSuccess(message: string) {
    this.toastr.success(message);
  }

  showError(message: string) {
    this.toastr.error(message);
  }
}
