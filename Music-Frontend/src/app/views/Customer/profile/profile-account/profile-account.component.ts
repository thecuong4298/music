import { AuthenPasswordComponent } from './../authen-password/authen-password.component';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DatePipe } from '@angular/common';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { UserModel } from '../../../../model/user.model';
import { Component, OnInit, Input, SimpleChanges, OnChanges, Output, EventEmitter } from '@angular/core';
import { FormGroup, Form, FormBuilder, Validators } from '@angular/forms';
import { MAT_DATE_LOCALE, DateAdapter, MAT_DATE_FORMATS, MatDialog } from '@angular/material';

export const MY_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};
@Component({
  selector: 'app-profile-account',
  templateUrl: './profile-account.component.html',
  styleUrls: ['./profile-account.component.css'],
  providers: [
    MatDatepickerModule,
    DatePipe,

    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS }
  ],
})
export class ProfileAccountComponent implements OnInit, OnChanges {

  createForm: FormGroup;
  @Input() userProfile: UserModel;
  @Output() saveInfo = new EventEmitter<any>();

  mdtMaxDate = new Date(new Date().setDate(new Date().getDate()));
  mdtMinDate = new Date(1900, 12, 1);

  constructor(private fb: FormBuilder, private dialog: MatDialog) { }

  ngOnChanges(changes: SimpleChanges): void {
    if ('userProfile' in changes) {
      this.createReactiveForm();
    }
  }

  ngOnInit(): void {

  }

  createReactiveForm() {
    this.createForm = this.fb.group(
      {
        fullName: [this.userProfile.fullName, Validators.required],
        phone: [this.userProfile.phone, [Validators.required, Validators.pattern('^[0-9]{9,11}$')]],
        birthday: [this.userProfile.birthday, Validators.required],
        gender: [this.userProfile.gender, Validators.required]
      }
    );
  }

  hasError(controlName: string, errorName: string) {
    return this.createForm.controls[controlName].hasError(errorName);
  }

  close() {

  }

  save() {
    this.getValueFromForm();
    if (!this.createForm.invalid) {
      this.saveInfo.emit(this.userProfile);
    }
  }

  getValueFromForm() {
    this.userProfile.fullName = this.createForm.controls.fullName.value.trim();
    if (this.createForm.controls.birthday.value._d) {
      this.userProfile.birthday = this.createForm.controls.birthday.value._d;
    }
    this.userProfile.phone = this.createForm.controls.phone.value;
    this.userProfile.gender = this.createForm.controls.gender.value;
  }

  reissuePass() {
      const dialogRef = this.dialog.open(AuthenPasswordComponent, {
        disableClose: true,
        width: '70vw',
        panelClass: 'myapp-background-dialog'
      });
      dialogRef.afterClosed().subscribe(result => {
      });
  }

}
