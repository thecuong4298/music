import { Song } from './../../../../share/model/song.model';
import { PlaylistService } from './../../../../services/playlist.service';
import { PlaylistEditComponent } from './../playlist-edit/playlist-edit.component';
import { PlaylistModel } from '../../../../model/playlist.model';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PlaylistCreateComponent } from '../playlist-create/playlist-create.component';
import { MatDialog } from '@angular/material';
import { GenreService } from '../../../../services/genre.service';
import { DomSanitizer } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-profile-playlist',
  templateUrl: './profile-playlist.component.html',
  styleUrls: ['./profile-playlist.component.scss']
})
export class ProfilePlaylistComponent implements OnInit {

  @Input() listPlaylist: PlaylistModel[];
  @Output() delete = new EventEmitter<any>();
  playlistEdit: PlaylistModel;
  listSong: Song[];

  constructor(
    private dialog: MatDialog,
    private playlistService: PlaylistService,
    private domSanitizer: DomSanitizer,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
  }

  openCreate() {
    const dialogRef = this.dialog.open(PlaylistCreateComponent, {
      disableClose: true,
      width: '70vw',
      panelClass: 'myapp-background-dialog'
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  openEdit(playlist) {
    this.playlistService.setResetForm(false);
    this.getById(playlist.id);
  }

  getById(playlistId) {
    this.playlistService.getPlaylistById(playlistId).subscribe((data: any) => {
      if (data.code === 200) {
        this.playlistEdit = data.data;
        if (this.playlistEdit.base64) {
          this.playlistEdit.url = this.domSanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' + data.data.base64);
        } else {
          this.playlistEdit.url = 'assets/img/common/default-image.jpg';
        }
        this.getSongInPlaylist(playlistId);

      } else {
        this.showError(data.mstrErrors);
      }
    }, () => {
      this.showError('Có lỗi không xác định xảy ra');
    });
  }

  getSongInPlaylist(playlistId) {
    this.playlistService.findSongInPlaylistById(playlistId).subscribe(
      (data: any) => {
        if (data.code === 200 || data.code === 204) {
          if (data.code === 200) {
            this.listSong = data.data;
            this.listSong.forEach(element => {
              if (element.base64) {
                element.url = this.domSanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' + element.base64);
              } else {
                element.url = 'assets/img/common/default-image.jpg';
              }
            });
          } else {
            this.listSong = [];
          }

          const dialogRef = this.dialog.open(PlaylistEditComponent, {
            disableClose: true,
            width: '70vw',
            panelClass: 'myapp-background-dialog',
            data: { playlistEdit: this.playlistEdit, listSong: this.listSong }
          });

          dialogRef.afterClosed().subscribe(result => {
          });
        }
      }, () => {
        this.showError('Có lỗi không xác định xảy ra');
      });
  }

  deletePlaylist(id: number) {
    this.delete.emit(id);
  }

  showSuccess(message: string) {
    this.toastr.success(message);
  }

  showError(message: string) {
    this.toastr.error(message);
  }
}
