import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { startWith, map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { FormControl, FormBuilder, FormGroup } from '@angular/forms';
import { UserService } from '../../../services/user.service';
import { SearchMusicService } from '../../../services/search-music.service';
import { ArtistModel } from '../../../Model/artist.model';
import { Song } from '../../../share/model/song.model';
import { Router } from '@angular/router';

// export const _filter = (opt: string[], value: string): string[] => {
//   const filterValue = value.toLowerCase();

//   return opt.filter(item => item.toLowerCase().indexOf(filterValue) === 0);
// };
@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SearchComponent implements OnInit {
  stateForm: FormGroup = this._formBuilder.group({
    stateGroup: '',
  });

  artists: ArtistModel[] = [];
  songs: Song[] = [];
  value;

  constructor(
    private _formBuilder: FormBuilder,
    private router: Router
  ) { }

  ngOnInit() {
    // this.stateForm.controls.stateGroup.valueChanges.subscribe(value => {
    //   this.value = value;
    //   this.search(value);
    // });
  }

  search() {
    const search = this.stateForm.controls.stateGroup.value;
    if (!search || search.length == 0) {
      return;
    }
    this.router.navigate(['/customer/search', search]);
  }

  // private search(value: string) {
  //   if (value) {
  //     this.value = value;
  //     this.searchService.search(value).subscribe((res: any) => {
  //       this.artists = res.data.artist;
  //       this.songs = res.data.listSong;
  //     });
  //   }
  //   // return this.stateGroups;
  // }
}
