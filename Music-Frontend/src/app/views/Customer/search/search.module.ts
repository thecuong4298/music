import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchResponComponent } from './search-respon/search-respon.component';
import { CommonComponentModule } from '../../../common/common-component.module';
import { SearchRoutingModule } from './search.routing';



@NgModule({
  declarations: [
    SearchResponComponent
  ],
  imports: [
    CommonModule,
    CommonComponentModule,
    SearchRoutingModule
  ]
})
export class SearchModule { }
