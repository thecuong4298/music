
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SearchResponComponent } from './search-respon/search-respon.component';

const routes: Routes = [
  {
    path: ':name',
    component: SearchResponComponent,
    data: {
      title: 'Trang chủ'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SearchRoutingModule { }
