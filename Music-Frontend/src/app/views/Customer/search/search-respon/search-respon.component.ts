import { Component, OnInit } from '@angular/core';
import { SearchMusicService } from '../../../../services/search-music.service';
import { Song } from '../../../../share/model/song.model';
import { ArtistModel } from '../../../../Model/artist.model';
import { AlbumModel } from '../../../../Model/album.model';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-search-respon',
  templateUrl: './search-respon.component.html',
  styleUrls: ['./search-respon.component.scss']
})
export class SearchResponComponent implements OnInit {
  songs: Song[] = [];
  artists: ArtistModel[] = [];
  albums: AlbumModel[] = [];
  constructor(
    private searchService: SearchMusicService,
    private router: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.router.params.subscribe(res => {
      this.searchService.search(res.name, true).subscribe((respon: any) => {
        this.artists = respon.data.artist;
        this.songs = respon.data.listSong;
        this.albums = respon.data.listAlbum;
      });
    });
  }
}

