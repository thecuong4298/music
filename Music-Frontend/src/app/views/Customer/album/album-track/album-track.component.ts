import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AlbumModel } from '../../../../model/album.model';
import { Song } from '../../../../share/model/song.model';
import { AlbumService } from '../../../../services/album.service';
import { UserService } from '../../../../services/user.service';

@Component({
  selector: 'app-album-track',
  templateUrl: './album-track.component.html',
  styleUrls: ['./album-track.component.scss']
})
export class AlbumTrackComponent implements OnInit {
  album: AlbumModel = new AlbumModel();
  songs: Song[] = [];
  constructor(
    private route: ActivatedRoute,
    private albumService: AlbumService,
    private userService: UserService
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe(param => {
      this.getSong(param.id);
      this.getAlbum(param.id);
    });
  }


  getSong(albumId: number) {
    this.albumService.getSongByAlbum(albumId).subscribe((data: any) => {
      this.songs = data.data;
    });
  }

  getAlbum(albumId: number){
    this.albumService.getAlbumById(albumId).subscribe((data: any) => {
      this.album = data.data;
    });
  }

  listenAll() {
    this.userService.setTrack(this.songs);
  }
}
