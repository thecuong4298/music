import { Component, OnInit } from '@angular/core';
import { AlbumService } from '../../../services/album.service';
import { AlbumModel } from '../../../model/album.model';
import { Router } from '@angular/router';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-album',
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.scss']
})
export class AlbumComponent implements OnInit {

  albums: AlbumModel[] = [];

  constructor(
    private albumservice: AlbumService,
    private route: Router,
    private userService: UserService
  ) { }

  ngOnInit(): void {
    this.albumservice.getAlbumActive().subscribe((res: any) => {
      this.albums = res.data;
    });
  }

  select(id: number) {
    this.route.navigateByUrl(`album/${id}`);
  }

  selectAll(albumId) {
    this.albumservice.getSongByAlbum(albumId).subscribe((data: any) => {
      this.userService.setTrack(data.data);
    });
  }
}
