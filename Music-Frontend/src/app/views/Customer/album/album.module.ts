import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlbumComponent } from './album.component';
import { AlbumTrackComponent } from './album-track/album-track.component';
import { CommonComponentModule } from '../../../common/common-component.module';
import { AlbumRoutingModule } from './album.routing';



@NgModule({
  declarations: [
    AlbumComponent,
    AlbumTrackComponent,
  ],
  imports: [
    CommonModule,
    CommonComponentModule,
    AlbumRoutingModule
  ]
})
export class AlbumModule { }
