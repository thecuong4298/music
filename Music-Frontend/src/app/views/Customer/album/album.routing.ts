
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AlbumComponent } from './album.component';
import { AlbumTrackComponent } from './album-track/album-track.component';

const routes: Routes = [
  {
    path: '',
    component: AlbumComponent,
    data: {
      title: 'Trang chủ'
    }
  },
  {
    path: ':id',
    component: AlbumTrackComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AlbumRoutingModule { }
