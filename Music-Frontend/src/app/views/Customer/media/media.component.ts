import { Component, OnInit, ElementRef, AfterViewInit, ViewChild, ViewEncapsulation, HostListener, NgZone } from '@angular/core';
import { Track } from '../../../model/track.model';
import { DomSanitizer } from '@angular/platform-browser';
import { UserService } from '../../../services/user.service';
import { saveMp3 } from '../../../share/ultil/blob-util';
import { Base64 } from '../../../model/pipeImage';
import { STORAGE_KEY } from '../../../constants';
import { interval } from 'rxjs';
import { Router } from '@angular/router';
import { AccountService } from '../../../services/common/account.service';

@Component({
  selector: 'app-media',
  templateUrl: './media.component.html',
  styleUrls: ['./media.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MediaComponent implements OnInit, AfterViewInit {

  constructor(
    private base64: Base64,
    private userService: UserService,
    private route: Router,
    public ngZone: NgZone,
    private accountService: AccountService
  ) { }

  @ViewChild('audioPlayer', { read: ElementRef }) player: ElementRef;
  @ViewChild('renderer', { read: ElementRef }) canvas: ElementRef;
  @ViewChild('form', { read: ElementRef }) form: ElementRef;
  @ViewChild('imgrotate', { read: ElementRef }) imgrotate: ElementRef;

  clickWidth = false;
  displayTitle = '';
  displayVolumeControls = true;
  indexSong = 0;
  loaderDisplay = false;
  isPlaying = false;
  currentTime = 0;
  volume = 1;
  duration = 0.01;
  loop = 1;
  mix = false;
  dfVolumne = 1;
  playlistTrack: Track[] = [];
  currentTrack: Track;
  opened = false;
  img = 'assets/img/media/noreplay.png';
  imgMix = 'assets/img/media/nomix.png';
  srcVL = 'assets/img/media/volume100.png';
  openPlaylist = true;
  lyric;
  // visual

  ctx;
  center_x;
  center_y;
  radius;
  x_end;
  y_end;
  bar_height;
  frequency_array;
  bars = 200;
  bar_width = 2;
  audio;
  context;
  analyser;
  source;
  rads;
  x;
  y;
  hover;
  displayedColumns: string[] = ['title', 'link'];
  msaapPlaylist: Track[] = [
    {
      id: 0,
      title: 'Cành lá',
      link: 'audio.mp3',
      img: 'assets/img/cd.jpg',
      singer: 'Cuong',
      lyric: 'Em gì ơi /n anh ở đây /n cd /nbadblskd/nlskd/nalo/nblo/ncon meo meo /n meo con cac'
    },
  ];

  url;
  time: number = 0;
  interval;
  isplay = true;

  ngAfterViewInit(): void {
    if (this.route.url.indexOf('admin') !== -1) {
      return;
    }
    this.userService.track$.subscribe(quenue => {
      if (quenue && quenue.tracks) {
        if (quenue.reload) {
          this.msaapPlaylist = quenue.tracks;
          this.isplay = false;
          this.playlistTrack = this.msaapPlaylist;
          let index = 0;
          this.playlistTrack.forEach(track => {
            track.index = index;
            index++;
          });
          this.indexSong = 0;
          this.currentTrack = this.playlistTrack[this.indexSong];
          this.updateCurrentSong(this.isplay);
        } else {
          this.updateTrack(quenue.tracks);
        }
      }
    });
    const _this = this;
    this.player.nativeElement.addEventListener('playing', (function () {
      _this.isPlaying = true;
      _this.duration = Math.floor(_this.player.nativeElement.duration);
    }));
    this.player.nativeElement.addEventListener('pause', (/**
      * @return {?}
      */
      function () {
        _this.isPlaying = false;
      }));
    this.player.nativeElement.addEventListener('timeupdate', (/**
      * @return {?}
      */
      function () {
        _this.currentTime = Math.floor(_this.player.nativeElement.currentTime);
      }));
    this.player.nativeElement.addEventListener('volume', (/**
      * @return {?}
      */
      function () {
        _this.volume = Math.floor(_this.player.nativeElement.volume);
      }));
    this.player.nativeElement.addEventListener('loadstart', (/**
      * @return {?}
      */
      function () {
        _this.loaderDisplay = true;
      }));
    this.player.nativeElement.addEventListener('loadeddata', (/**
      * @return {?}
      */
      function () {
        _this.loaderDisplay = false;
        _this.duration = Math.floor(_this.player.nativeElement.duration);
      }));

    this.player.nativeElement.addEventListener('ended', (/**
        * @return {?}
        */
      function () {
        if (_this.checkIfSongHasStartedSinceAtleastTwoSeconds()) {
          if (this.mix) {
            _this.nextSong();
          } else {
            _this.nextSong();
          }
        }
      }));
    this.initPage();
  }

  getPlaylist() {
    const tracks = localStorage.getItem(STORAGE_KEY.playlist);
    if (tracks && tracks.length > 0) {
      this.msaapPlaylist = JSON.parse(tracks);
      this.userService.setTrackByTrack(this.msaapPlaylist);
      this.playlistTrack = this.msaapPlaylist;
      const index = localStorage.getItem('indexSong');
      if (index) {
        this.indexSong = Number(index);
      }
    } else {
      this.userService.getTopSongByCountry(15, 1, false).subscribe((res: any) => {
        localStorage.setItem(STORAGE_KEY.playlist, JSON.stringify(res.data));
        this.userService.setTrack(res.data);
      });
    }
  }

  ngOnInit() {
    if (this.route.url.indexOf('admin') !== -1) {
      return;
    }
    this.getPlaylist();
    this.playlistTrack = this.msaapPlaylist;
    let index = 0;
    this.playlistTrack.forEach(track => {
      track.index = index;
      index++;
    });
    this.currentTrack = this.playlistTrack[this.indexSong];
    this.updateCurrentSong(this.isplay);
  }

  updateTrack(tracks: Track[]) {
    this.msaapPlaylist = tracks;
    this.playlistTrack = this.msaapPlaylist;
    let index = 0;
    this.playlistTrack.forEach(track => {
      track.index = index;
      index++;
    });
  }

  startTimer() {
    this.interval = setInterval(() => {
      this.time++;
      if (this.time == 60) {
        this.userService.view(this.currentTrack.id).subscribe(res => {
          console.log(res);
        });
      }
    }, 1000);
  }

  pauseTimer() {
    clearInterval(this.interval);
  }
  hoverOut() {
    this.hover = 0;
  }

  hovers(value: number) {
    this.hover = value;
  }

  applyFilter(filterValue: string) {
    if (filterValue && filterValue.trim().length > 0) {
      this.playlistTrack = this.msaapPlaylist.filter(item => item.title.indexOf(filterValue.trim()) != -1);
    } else {
      this.playlistTrack = this.msaapPlaylist;
    }
  }

  openPlayList() {
    this.openPlaylist = !this.openPlaylist;
  }

  closePlayList() {
    this.openPlaylist = true;
  }

  openMedia() {
    this.clickWidth = !this.clickWidth;
  }

  mouseLoop() {
    if (this.loop == 1) {
      this.img = 'assets/img/media/noreplayh.png';
    } else if (this.loop == 2) {
      this.img = 'assets/img/media/replayallh.png';
    } else if (this.loop == 0) {
      this.img = 'assets/img/media/replay1h.png';
    }
  }

  mouseLoopOut() {
    if (this.loop == 1) {
      this.img = 'assets/img/media/noreplay.png';
    }
  }

  mouseMix() {
    this.imgMix = 'assets/img/media/mix.png';
  }

  mouseMixOut() {
    if (!this.mix) {
      this.imgMix = 'assets/img/media/nomix.png';
    }
  }

  bindPlayerEvent() {

  }
  previousSong() {
    this.currentTime = 0;
    this.duration = 0.01;
    if ((this.indexSong - 1) < 0) {
      this.indexSong = (this.playlistTrack.length - 1);
    } else {
      this.indexSong--;
    }
    this.updateCurrentSong(false);
  }

  play() {
    this.player.nativeElement.currentTime = 0;
    this.displayTitle = this.currentTrack.title;
  }


  checkIfSongHasStartedSinceAtleastTwoSeconds() {
    return this.player.nativeElement.currentTime > 2;
  }

  updateCurrentSong(play?) {
    const _this = this;
    this.currentTrack = this.playlistTrack[this.indexSong];
    localStorage.setItem('indexSong', this.indexSong.toString());
    this.userService.getBase64Mp3(this.currentTrack.link).subscribe((res: any) => {
      this.displayTitle = this.currentTrack.title;
      if (this.currentTrack.lyric && this.currentTrack.lyric.length > 0) {
        this.lyric = this.currentTrack.lyric;
      }
      this.url = this.base64.transform(res.data, 'data:audio/mp3;base64,');
      setTimeout((function () {
        this.currentTime = 0;
        this.duration = 0.01;
        if (!play) {
          _this.player.nativeElement.play();
          _this.time = 1;
          setTimeout(() => {
            // if (_this.isplay) {
              _this.pauseTimer();
              _this.startTimer();
            // }
          }, 1000);
        }
      }), 200);
    });
  }

  playBtnHandler() {
    if (this.loaderDisplay) {
      return;
    }
    if (this.player.nativeElement.paused) {
      this.player.nativeElement.play(this.currentTime);
      this.startTimer();
    } else {
      this.currentTime = this.player.nativeElement.currentTime;
      this.player.nativeElement.pause();
      this.pauseTimer();
    }
    this.displayTitle = this.currentTrack.title;
  }

  selectATrack(index: number) {
    if (index == -1) {
      const marr = this.playlistTrack.filter(item => item.index != this.indexSong);
      const vindex = Math.floor(Math.random() * (marr.length - 1));
      this.indexSong = marr[vindex].index;
    } else {
      this.indexSong = index - 1;
    }
    this.updateCurrentSong(false);
  }

  pnextSong() {
    if ((this.indexSong + 1) >= this.playlistTrack.length) {
      this.indexSong = 0;
    } else {
      this.indexSong++;
    }
    this.updateCurrentSong(false);
  }

  nextSong() {
    this.currentTime = 0;
    this.duration = 0.01;
    if (this.loop == 2) {
      if (this.mix) {
        this.selectATrack(-1);
      } else {
        this.pnextSong();
      }
      this.player.nativeElement.play(this.currentTime);
    } else if (this.loop == 1) {
      if (this.indexSong != this.playlistTrack.length - 1) {
        if (this.mix) {
          this.selectATrack(-1);
        } else {
          this.pnextSong();
        }
        this.player.nativeElement.play(this.currentTime);
      } else {
        this.player.nativeElement.pause();
      }
    } else {
      this.player.nativeElement.play(this.currentTime);
    }
  }

  clickMix() {
    if (this.mix == false) {
      this.mix = true;
      this.imgMix = 'assets/img/media/mix.png';
    } else {
      this.mix = false;
      this.imgMix = 'assets/img/media/nomix.png';
    }
  }

  clickLoop() {
    if (this.loop == 0) {
      this.loop = 1;
      this.img = 'assets/img/media/noreplay.png';
    } else if (this.loop == 1) {
      this.loop = 2;
      this.img = 'assets/img/media/replayall.png';
    } else if (this.loop == 2) {
      this.loop = 0;
      this.img = 'assets/img/media/replay1.png';
    }
    this.mouseLoop();
  }

  toggleVolume() {
    if (this.volume === 0) {
      this.setVolume(this.dfVolumne);
      this.srcVL = 'assets/img/media/volume100.png';
    } else {
      this.volume = 0;
      this.player.nativeElement.volume = 0;
      this.srcVL = 'assets/img/media/volume0.png';
    }
  }

  setVolume(volume: any) {
    this.volume = volume;
    this.dfVolumne = volume;
    this.player.nativeElement.volume = volume;
    if (this.volume === 0) {
      this.srcVL = 'assets/img/media/volume0.png';
    } else {
      this.srcVL = 'assets/img/media/volume100.png';
    }
  }

  currTimePosChanged(event: any) {
    this.player.nativeElement.currentTime = event.value;
  }

  initPage() {
    this.audio = this.player.nativeElement;
    this.context = new (window.AudioContext)();
    this.analyser = this.context.createAnalyser();
    this.source = this.context.createMediaElementSource(this.audio);
    this.source.connect(this.analyser);
    this.analyser.connect(this.context.destination);
    this.frequency_array = new Uint8Array(this.analyser.frequencyBinCount);
    this.animationLooper();
  }
  animationLooper() {
    if (this.isPlaying) {
      this.canvas.nativeElement.height = this.form.nativeElement.offsetHeight / 3;
    } else {
      this.canvas.nativeElement.height = 0;
    }
    this.canvas.nativeElement.width = this.form.nativeElement.offsetWidth / 12 * 4.5;
    this.ctx = (<HTMLCanvasElement>this.canvas.nativeElement).getContext('2d');
    // find the center of the window
    this.center_x = this.canvas.nativeElement.width / 2;
    this.center_y = this.canvas.nativeElement.height / 2;
    this.radius = 40;
    // style the background
    let gradient = this.ctx.createLinearGradient(0, 0, 0, this.canvas.nativeElement.height);
    gradient.addColorStop(0, '#161c35');
    gradient.addColorStop(1, '#161c35');
    this.ctx.fillStyle = gradient;
    this.ctx.fillRect(0, 0, this.canvas.nativeElement.width, this.canvas.nativeElement.height);
    // draw a circle
    this.ctx.beginPath();
    this.ctx.arc(this.center_x, this.center_y, this.radius, 0, 2 * Math.PI);
    // this.ctx.stroke();
    this.analyser.getByteFrequencyData(this.frequency_array);
    for (let i = 0; i < this.bars; i++) {
      // divide a circle into equal parts
      this.rads = Math.PI * 2 / this.bars;
      this.bar_height = this.frequency_array[i] * 0.3;
      // set coordinates
      this.x = this.center_x + Math.cos(this.rads * i) * (this.radius);
      this.y = this.center_y + Math.sin(this.rads * i) * (this.radius);
      this.x_end = this.center_x + Math.cos(this.rads * i) * (this.radius + this.bar_height);
      this.y_end = this.center_y + Math.sin(this.rads * i) * (this.radius + this.bar_height);
      // draw a bar
      this.drawBar(this.x, this.y, this.x_end, this.y_end, this.bar_width, this.frequency_array[i]);
    }
    this.ngZone.runOutsideAngular(() => {
      requestAnimationFrame(() => {
        this.animationLooper();
      });
    });
  }
  // for drawing a bar
  drawBar(x1, y1, x2, y2, width, frequency) {
    const lineColor = 'rgb(' + frequency + ', ' + frequency + ', ' + 205 + ')';
    this.ctx.strokeStyle = lineColor;
    this.ctx.lineWidth = width;
    this.ctx.beginPath();
    this.ctx.moveTo(x1, y1);
    this.ctx.lineTo(x2, y2);
    this.ctx.stroke();
  }

  download(track: Track) {
    this.userService.getBase64Mp3(track.link).subscribe((res: any) => {
      saveMp3(res.data, track.title);
    });
  }
  delete(track: Track) {
    this.userService.deleteTrack(track.id);
  }

  addPlaylist(track: Track) {
    this.accountService.openAddPlaylist(track.id);
  }
}
