import { SlideShowModel } from './../../../../model/slideshow.model';
import { UserService } from './../../../../services/user.service';
import { TopicService } from './../../../../services/topic.service';
import { TopicModel } from '../../../../model/topic.model';
import { AlbumService } from './../../../../services/album.service';
import { AlbumModel } from '../../../../model/album.model';
import { GenreModel } from '../../../../model/genre.model';
import { GenreService } from './../../../../services/genre.service';
import { ArtistModel } from '../../../../model/artist.model';
import { DomSanitizer } from '@angular/platform-browser';
import { ArtistService } from './../../../../services/artist.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SongService } from '../../../../services/song.service';
declare var $: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  listArtist: ArtistModel[];
  listGenre: GenreModel[];
  listAlbums: AlbumModel[];
  listTopic: TopicModel[];
  listSlide: SlideShowModel[];
  artist: ArtistModel;

  constructor(
    private artistService: ArtistService,
    private genreSerivce: GenreService,
    private albumService: AlbumService,
    private topicService: TopicService,
    private userService: UserService,
    private domSanitizer: DomSanitizer,
    private songService: SongService,
    private route: Router,
  ) { }

  ngOnInit() {
    this.artistService.getTop4Artist().subscribe((data: any) => {
      if (data.code === 200) {
        this.listArtist = data.data;
        this.base64(data.data);
        this.artist = this.listArtist[0];
      }
    });

    this.genreSerivce.get7GenreRandom().subscribe((data: any) => {
      if (data.code === 200) {
        this.listGenre = data.data;
        this.base64(data.data);
      }
    });

    this.albumService.get4AlbumRandom().subscribe((data: any) => {
      if (data.code === 200) {
        this.listAlbums = data.data;
        this.base64(data.data);
      }
    });

    this.topicService.get3TopicRandom().subscribe((data: any) => {
      if (data.code === 200) {
        this.listTopic = data.data;
        this.base64(data.data);
      }
    });

    this.userService.findAllSlideshowActive().subscribe((data: any) => {
      if (data.code === 200) {
        this.listSlide = data.data;
        this.base64(data.data);
      }

    });
  }

  base64(list: any) {
    list.forEach(model => {
      if (!model.base64) {
        model.url = 'assets/img/common/default-image.jpg';
      } else {
        model.url = this.domSanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' + model.base64);
      }
    });
  }

  showArtist(artist) {
    this.artist = artist;
  }

  navigate(path: string) {
    if (path.indexOf('song') == -1) {
      this.route.navigateByUrl(path);
    } else {
      this.songService.getSongById(path.substring(path.indexOf('/') + 1, path.length)).subscribe((song: any) => {
        this.userService.setTrack([song.data]);
      });
    }
  }
}
