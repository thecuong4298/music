import { TopicModel } from '../../../../model/topic.model';
import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-topic-home',
  templateUrl: './topic-home.component.html',
  styleUrls: ['./topic-home.component.css']
})
export class TopicHomeComponent implements OnInit {

  @Input() listTopic: TopicModel[];

  constructor(
    private route: Router
  ) { }

  ngOnInit() {
  }

  navigate(event) {
    this.route.navigateByUrl(`topic/${event}`);
  }
}
