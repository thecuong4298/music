import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Song } from '../../../../share/model/song.model';
import { UserService } from '../../../../services/user.service';
import { ToastrService } from 'ngx-toastr';
import { saveMp3 } from '../../../../share/ultil/blob-util';
import { CountryService } from '../../../../services/country.service';
import { Country } from '../../../../share/model/country.model';
import { MatDialog } from '@angular/material';
import { DialogPlaylistComponent } from '../../dialog-playlist/dialog-playlist.component';
import { AccountService } from '../../../../services/common/account.service';
import { routes } from '../../../../app.routing';
import { Router } from '@angular/router';

@Component({
  selector: 'app-chart-home',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ChartComponent implements OnInit {

  country;
  songs: Song[] = [];
  hover;
  countrys: Country[] = [];
  isMonth = false;

  constructor(
    private userService: UserService,
    private toastr: ToastrService,
    private countryService: CountryService,
    private accountService: AccountService,
    private route: Router
  ) { }

  ngOnInit() {
    this.countryService.getCountryActive().subscribe((res: any) => {
      this.countrys = res.data;
      if (this.countrys) {
        this.showChart(this.countrys[0].id);
      }
    });
  }

  hoverOut() {
    this.hover = 0;
  }

  hovers(value: number) {
    this.hover = value;
  }

  download(track: Song) {
    this.userService.getBase64Mp3(track.path).subscribe((res: any) => {
      if (res.code == 500)
        this.toastr.error('Không tìm thấy tệp tin');
      else
        saveMp3(res.data, track.name);
    }, error => {
      this.toastr.error('Có lỗi xảy ra');
    });
  }

  selectTrack(song: Song) {
    this.userService.setTrack([song]);
  }

  showChart(id) {
    this.country = id;
    this.userService.getTopSongByCountry(15, this.country, this.isMonth).subscribe((respon: any) => {
      this.songs = respon.data;
    });
  }

  openPlaylist(songId: number) {
    this.accountService.openAddPlaylist(songId);
  }

  listenAll() {
    this.userService.setTrack(this.songs);
  }

  addTrackToQuence(song: Song) {
    this.userService.addTrack(song);
  }

  viewIsMonth(ismonth: boolean) {
    this.isMonth = ismonth;
    this.showChart(this.country);
  }
  goToChart(){
    this.route.navigateByUrl('/chart');
  }
}
