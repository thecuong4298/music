import { GenreModel } from '../../../../model/genre.model';
import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-genre-home',
  templateUrl: './genre-home.component.html',
  styleUrls: ['./genre-home.component.scss']
})
export class GenreHomeComponent implements OnInit {

  @Input() listGenre: GenreModel[];
  firstGenre: GenreModel;

  constructor(
    private route: Router
  ) { }

  ngOnInit() {
  }

  navigate(event) {
    this.route.navigateByUrl(`genres/${event}`);
  }
}
