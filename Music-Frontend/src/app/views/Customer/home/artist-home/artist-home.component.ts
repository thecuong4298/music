import { ArtistModel } from './../../../../model/artist.model';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-artist-home',
  templateUrl: './artist-home.component.html',
  styleUrls: ['./artist-home.component.scss']
})
export class ArtistHomeComponent implements OnInit {

  @Input() listArtist: ArtistModel[];
  @Input() artist: ArtistModel;
  @Output() showArtist = new EventEmitter<any>();

  constructor(
    private route: Router
  ) { }

  ngOnInit() {
  }

  sendArtist(artist) {
    this.showArtist.emit(artist);
  }

  navigate(event) {
    this.route.navigateByUrl(`artist/${event}`);
  }
}
