import { AlbumModel } from '../../../../model/album.model';
import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-album-home',
  templateUrl: './album-home.component.html',
  styleUrls: ['./album-home.component.scss']
})
export class AlbumHomeComponent implements OnInit {

  @Input() listAlbums: AlbumModel[];

  constructor(
    private route: Router
  ) { }

  ngOnInit() {
  }

  navigate(event) {
    this.route.navigateByUrl(`album/${event}`);
  }
}
