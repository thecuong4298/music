import { BrowserModule } from '@angular/platform-browser';
import { HomeRoutingModule } from './home.routing';
import { NgModule } from '@angular/core';
import { HomeComponent } from './home/home.component';
import { ChartComponent } from './chart/chart.component';
import { MatTabsModule, MatButtonModule } from '@angular/material';
import { CommonModule } from '@angular/common';
import { ArtistHomeComponent } from './artist-home/artist-home.component';
import { AlbumHomeComponent } from './album-home/album-home.component';
import { GenreHomeComponent } from './genre-home/genre-home.component';
import { TopicHomeComponent } from './topic-home/topic-home.component';
import { CommonComponentModule } from '../../../common/common-component.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    HomeComponent,
    ChartComponent,
    ArtistHomeComponent,
    AlbumHomeComponent,
    GenreHomeComponent,
    TopicHomeComponent
  ],
  imports: [
    HomeRoutingModule,
    CommonModule,
    CommonComponentModule,
    TranslateModule,
    MatButtonModule
  ],
  exports: [
    ChartComponent
  ],
  providers: [
  ],
  entryComponents: []
})
export class HomeModule { }
