import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Song } from '../../../../share/model/song.model';
import { TopicModel } from '../../../../model/topic.model';
import { TopicService } from '../../../../services/topic.service';
import { UserService } from '../../../../services/user.service';

@Component({
  selector: 'app-topic-track',
  templateUrl: './topic-track.component.html',
  styleUrls: ['./topic-track.component.scss']
})
export class TopicTrackComponent implements OnInit {
  topic: TopicModel = new TopicModel();
  songs: Song[] = [];
  constructor(
    private route: ActivatedRoute,
    private topicService: TopicService,
    private userService: UserService
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe(param => {
      this.getSong(param.id);
      this.getTopic(param.id);
    });
  }


  getSong(topicId: number) {
    this.topicService.getSongByTopic(topicId).subscribe((data: any) => {
      this.songs = data.data;
    });
  }

  getTopic(topicId: number) {
    this.topicService.getTopicById(topicId).subscribe((data: any) => {
      this.topic = data.data;
    });
  }

  listenAll() {
    this.userService.setTrack(this.songs);
  }
}
