import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TopicService } from '../../../services/topic.service';
import { TopicModel } from '../../../model/topic.model';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-topic',
  templateUrl: './topic.component.html',
  styleUrls: ['./topic.component.scss']
})
export class TopicComponent implements OnInit {

  topics: TopicModel[] = [];

  constructor(
    private topicService: TopicService,
    private route: Router,
    private userService: UserService
  ) { }

  ngOnInit(): void {
    this.topicService.getTopicActive().subscribe((res: any) => {
      this.topics = res.data;
    });
  }

  select(id: number) {
    this.route.navigateByUrl(`topic/${id}`);
  }

  selectAll(topicId) {
    this.topicService.getSongByTopic(topicId).subscribe((data: any) => {
      this.userService.setTrack(data.data);
    });
  }
}
