import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TopicComponent } from './topic.component';
import { TopicTrackComponent } from './album-track/topic-track.component';
import { CommonComponentModule } from '../../../common/common-component.module';
import { AlbumRoutingModule } from './topic.routing';



@NgModule({
  declarations: [
    TopicComponent,
    TopicTrackComponent,
  ],
  imports: [
    CommonModule,
    CommonComponentModule,
    AlbumRoutingModule
  ]
})
export class TopicModule { }
