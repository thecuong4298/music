
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TopicComponent } from './topic.component';
import { TopicTrackComponent } from './album-track/topic-track.component';

const routes: Routes = [
  {
    path: '',
    component: TopicComponent,
    data: {
      title: 'Trang chủ'
    }
  },
  {
    path: ':id',
    component: TopicTrackComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AlbumRoutingModule { }
