
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ArtistComponent } from './artist.component';
import { ArtistTrackComponent } from './artist-track/artist-track.component';

const routes: Routes = [
  {
    path: '',
    component: ArtistComponent,
    data: {
      title: 'Trang chủ'
    }
  },
  {
    path: ':id',
    component: ArtistTrackComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArtistRoutingModule { }
