import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ArtistService } from '../../../../services/artist.service';
import { Song } from '../../../../share/model/song.model';
import { Artist } from '../../../../share/model/artist.model';
import { AlbumService } from '../../../../services/album.service';
import { AlbumModel } from '../../../../model/album.model';
import { UserService } from '../../../../services/user.service';

@Component({
  selector: 'app-artist-track',
  templateUrl: './artist-track.component.html',
  styleUrls: ['./artist-track.component.scss']
})
export class ArtistTrackComponent implements OnInit {

  songs: Song[] = [];
  artist: Artist = new Artist();
  albums: AlbumModel[] = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private artistService: ArtistService,
    private userService: UserService,
    private albumService: AlbumService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(param => {
      this.getSong(param.id);
      this.getArtist(param.id);
      this.getAlbum(param.id);
    });
  }

  getAlbum(artistId: number) {
    this.albumService.getAlbumByArtist(artistId).subscribe((data: any) => {
      this.albums = data.data;
    });
  }

  getSong(artistId: number) {
    this.artistService.getSongByArtist(artistId).subscribe((data: any) => {
      this.songs = data.data;
      console.log(this.songs);
    });
  }

  getArtist(artistId: number) {
    this.artistService.getArtistById(artistId).subscribe((data: any) => {
      this.artist = data.data;
    });
  }

  select(id: number) {
    this.router.navigateByUrl(`album/${id}`);
  }

  listenAll() {
    this.userService.setTrack(this.songs);
  }

}
