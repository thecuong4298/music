import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ArtistService } from '../../../services/artist.service';
import { ArtistByCountry } from '../../../share/model/artist_By_Country.model';
import { Slide } from '../../../share/model/slide.model';
import { Router } from '@angular/router';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-artist',
  templateUrl: './artist.component.html',
  styleUrls: ['./artist.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ArtistComponent implements OnInit {
  itemsPerSlide = 5;
  singleSlideOffset = true;
  noWrap = true;
  artistByCountry: ArtistByCountry[] = [];

  constructor(
    private artistService: ArtistService,
    private route: Router,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.artistService.getAllArtistByCountry().subscribe((res: any) => {
      this.artistByCountry = res.data;
      console.log(this.artistByCountry);
      
    });
  }

  clickdetail(event) {
    this.route.navigateByUrl(`artist/${event}`);
  }

  selectAll(id: number) {
    this.artistService.getSongByArtist(id).subscribe((data: any) => {
      this.userService.setTrack(data.data);
    });
  }
}
