import { NgModule } from '@angular/core';
import { ArtistComponent } from './artist.component';
import { ArtistRoutingModule } from './artist.routing';
import { FormsModule } from '@angular/forms';
import { CommonModule, DatePipe } from '@angular/common';
import { CommonComponentModule } from '../../../common/common-component.module';
import { ArtistTrackComponent } from './artist-track/artist-track.component';

@NgModule({
  declarations: [
    ArtistComponent,
    ArtistTrackComponent
  ],
  imports: [
    FormsModule,
    CommonModule,
    ArtistRoutingModule,
    CommonComponentModule,
  ],
  providers: [
    DatePipe
  ],
  entryComponents: []
})
export class ArtistModule { }
