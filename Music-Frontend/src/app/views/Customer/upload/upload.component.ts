import { LoginMusicComponent } from './../login-music/login-music.component';
import { AccountService } from './../../../services/common/account.service';
import { SongUpload } from './../../../model/song-upload.model';
import { UserService } from './../../../services/user.service';
import { GrenderModel } from './../../../model/grender.model';
import { SearchArtistUploadComponent } from './search-artist-upload/search-artist-upload.component';
import { GenreModel } from './../../../model/genre.model';
import { SongService } from './../../../services/song.service';
import { Song } from './../../../share/model/song.model';
import { ArtistService } from './../../../services/artist.service';
import { ArtistModel } from './../../../model/artist.model';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Component, OnInit, ViewChild, AfterViewInit, OnDestroy, HostListener } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GenreService } from '../../../services/genre.service';
import { ToastrService } from 'ngx-toastr';
import { DialogsService } from '../../../common/confirm-dialog/dialogs.service';
import { environment } from '../../../../environments/environment';
import { ReplaySubject, Subject } from 'rxjs';
import { MatSelect, MatDialog } from '@angular/material';
import { takeUntil, take } from 'rxjs/operators';
import { CountryService } from '../../../services/country.service';
import { CountryModel } from '../../../Model/country.model';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})
export class UploadComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private http: HttpClient,
    private artistService: ArtistService,
    private toastr: ToastrService,
    private songService: SongService,
    private userService: UserService,
    private dialog: MatDialog,
    private countryService: CountryService,
    private genreService: GenreService,
    private accountService: AccountService
  ) { }

  files: any[] = [];
  createForm: FormGroup;
  uploadEmpty: boolean;
  imgPath: any;
  fileList: FileList;
  checkUploadSucces: boolean;
  messageErrorFile = '';
  messageErrorMp3 = '';
  listArtist: Song[];
  check: boolean;
  artistChoose: ArtistModel;
  countries: CountryModel[];
  songUpload: SongUpload = new SongUpload();
  genres: GenreModel[];
  userLogged: any;

  genderes: GrenderModel[] = [
    { value: 0, viewValue: 'Nữ' },
    { value: 1, viewValue: 'Nam' }
  ];


  ngOnInit(): void {
    this.userLogged = this.accountService.currentUserValue;
    this.createForm = this.fb.group(
      {
        songName: ['', Validators.required],
        songArtist: ['', Validators.required],
        // artistGender: [null, Validators.required],
        // artistCountry: [null, Validators.required],
        songCountry: ['', Validators.required],
        songGenre: ['', Validators.required],
        songDes: ['', [Validators.required, Validators.maxLength(500)]]
      }
    );

    this.userLogged = this.accountService.currentUserValue;

    if (!this.userLogged) {
      const dialogLogin = this.dialog.open(LoginMusicComponent, {
        data: null
      });
      dialogLogin.afterClosed().subscribe(result => {
        if (this.accountService.currentUserValue) {
          this.userLogged = this.accountService.currentUserValue;
          this.init();
        }
      });
    } else {
      this.init();
    }
  }

  init() {
    this.getCountries();
    this.getGenres();
    this.check = false;
    this.imgPath = null;
    this.checkUploadSucces = false;
    this.uploadEmpty = true;

  }


  /**
   * on file drop handler
   */
  onFileDropped($event) {
    this.prepareFilesList($event);
  }

  /**
   * handle file from browsing
   */
  fileBrowseHandler(files) {
    if (files) {

      let checkFile;
      const formatFile = files[0].name.substring(files[0].name.length - 3, files[0].name.length);
      if (formatFile !== 'mp3') {
        checkFile = true;
        this.messageErrorMp3 = 'File nhạc không đúng định dạng mp3';
      } else {
        this.messageErrorMp3 = '';
      }

      if (checkFile) {
        return;
      }
      this.prepareFilesList(files);
    }
  }

  /**
   * Delete file from files list
   * @param index (File index)
   */
  deleteFile(index: number) {
    this.files.splice(index, 1);
  }

  /**
   * Convert Files list to normal array list
   * @param files (Files List)
   */
  prepareFilesList(files: Array<any>) {
    this.files = [];

    for (const item of files) {
      item.progress = 0;
      this.files.push(item);
      this.uploadFile(item, 'track');
    }
  }

  selectFile(fileInputEvent: any) {
    this.messageErrorFile = '';
    this.songUpload.pathAvatar = fileInputEvent.target.files[0].name;
    let checkFile;
    const formatFile = this.songUpload.pathAvatar.substring(this.songUpload.pathAvatar.length - 3, this.songUpload.pathAvatar.length);
    if (formatFile !== 'jpg' && formatFile !== 'png') {
      checkFile = true;
      this.uploadEmpty = false;
      this.messageErrorFile = 'Ảnh không đúng định dạng';
      this.imgPath = null;
    } else {
      this.messageErrorFile = '';
      this.uploadEmpty = true;
    }

    if (checkFile) {
      return;
    }

    // tslint:disable-next-line: triple-equals
    if (this.songUpload.pathAvatar.indexOf('.jpg') != -1 || this.songUpload.pathAvatar.indexOf('.png') != -1) {
      const reader = new FileReader();
      reader.readAsDataURL(fileInputEvent.target.files[0]);
      reader.onload = (_event) => {
        this.imgPath = reader.result;
        this.uploadFile(fileInputEvent.target.files[0], 'avatar');
      };
    } else {
      this.imgPath = null;
    }
  }

  hasError(controlName: string, errorName: string) {
    return this.createForm.controls[controlName].hasError(errorName);
  }

  saveSong() {
    this.userService.uploadSong(this.songUpload).subscribe(
      ((data: any) => {
        if (data.code === 200) {
          this.showSuccess('Upload thành công');
          this.files = [];
          this.fileList = null;
          this.createForm.reset();
          this.imgPath = null;
        }
      }), err => {
        this.showError('Có lỗi không xác định xảy ra');
      }
    );

  }

  checkUpload(): boolean {
    this.messageErrorFile = '';
    if (!this.songUpload.pathAvatar || this.songUpload.pathAvatar.length === 0) {
      this.messageErrorFile = 'Ảnh không được để trống';
      return this.uploadEmpty = false;
    } else {
      return this.uploadEmpty = true;
    }
  }

  getValueFromForm() {
    console.log(this.createForm);
    
    if (!this.createForm.invalid && this.songUpload.pathAvatar && this.songUpload.pathTrack) {
      this.songUpload.songName = this.createForm.controls.songName.value.trim();
      this.songUpload.songDescription = this.createForm.controls.songDes.value.trim();
      // this.songUpload.artistCountryId = this.createForm.controls.artistGender.value;
      // this.songUpload.songGenreId = this.createForm.controls.artistGender.value;
      this.songUpload.songCountryId = this.createForm.controls.songCountry.value;

      if (this.check === false) {
        const artistNameless = new ArtistModel();
        artistNameless.name = this.createForm.controls.songArtist.value.trim();
        // artistNameless.gender = this.createForm.controls.artistGender.value;
        // artistNameless.countryId = this.createForm.controls.artistCountry.value;
        artistNameless.status = 1;
        artistNameless.type = 2;

        this.artistService.createOrUpdate(artistNameless).subscribe(
          ((data: any) => {
            if (data.code !== 200) {
              this.showError('Thêm ca sĩ mới không thành công');
            } else {
              this.songUpload.artistId = data.data.id;
              this.saveSong();
            }
          })
        );
      } else {
        this.saveSong();
      }
    }
  }

  uploadFile(fileUpload: any, typeUpload: string) {
    const file: File = fileUpload;
    const formData: FormData = new FormData();
    formData.append('file', file);
    const headers = new HttpHeaders();
    headers.append('Content-type', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    const options = { headers: headers };
    let urlUpload = '';
    if (typeUpload === 'track') {
      urlUpload = '/file/uploadReport/track';
    } else {
      urlUpload = '/file/uploadReport/avatar';
    }
    this.http.post(environment.API_URL + urlUpload, formData, options)
      .subscribe(
        (data: any) => {
          if (data.code === 200) {
            if (typeUpload === 'track') {
              this.songUpload.pathTrack = data.data;
            } else {
              this.songUpload.pathAvatar = data.data;
            }
            this.checkUploadSucces = true;
          } else {
            this.checkUploadSucces = false;
          }
        },
        err => {
          this.showError('Có lỗi không xác định xảy ra');
        }
      );
  }

  getCountries() {
    this.countryService.getCountryActive().subscribe((res: any) => {
      this.countries = res.data;
    });
  }

  getGenres() {
    this.userService.getAllGenreActive().subscribe((res: any) => {
      this.genres = res.data;
    });
  }

  pickArtist(event) {
    if (event.checked) {
      const dialogRef = this.dialog.open(SearchArtistUploadComponent, {
        disableClose: true,
        width: '70vw',
        panelClass: 'myapp-background-dialog'
      });
      this.check = true;

      dialogRef.afterClosed().subscribe(result => {
        if (result.event === 'cancel') {
          this.check = false;
        } else {
          console.log(result.data);

          this.check = true;
          this.artistChoose = result.data;
          this.createForm.controls.songArtist.setValue(result.data.singer);
          // if (result.data.artistid.length > 1) {
          //   this.createForm.controls.artistGender.setValue(null);
          //   this.createForm.controls.artistCountry.setValue(null);
          // } else {
          //   this.createForm.controls.artistGender.setValue(this.artistChoose.gender);
          //   this.createForm.controls.artistCountry.setValue(this.artistChoose.countryId);
          // }
          this.songUpload.artistId = result.data.artistid;
          this.createForm.controls.songArtist.disable();
          // this.createForm.controls.artistCountry.disable();
          // this.createForm.controls.artistGender.disable();
        }
      });
    } else {
      this.check = false;
      this.artistChoose = null;
      this.createForm.controls.songArtist.setValue('');
      this.createForm.controls.artistCountry.setValue(null);
      this.createForm.controls.artistGender.setValue(null);
      this.createForm.controls.songArtist.enable();
      this.createForm.controls.artistCountry.enable();
      this.createForm.controls.artistGender.enable();
    }

  }

  showSuccess(message: string) {
    this.toastr.success(message);
  }

  showError(message: string) {
    this.toastr.error(message);
  }

}

