import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CountryService } from './../../../../services/country.service';
import { ToastrService } from 'ngx-toastr';
import { ArtistModel } from './../../../../model/artist.model';
import { Component, OnInit, Inject } from '@angular/core';
import { ArtistService } from '../../../../services/artist.service';
import { DomSanitizer } from '@angular/platform-browser';
import { CountryModel } from '../../../../Model/country.model';

@Component({
  selector: 'app-search-artist-upload',
  templateUrl: './search-artist-upload.component.html',
  styleUrls: ['./search-artist-upload.component.scss']
})
export class SearchArtistUploadComponent implements OnInit {

  p: any;
  listArtist: ArtistModel[];
  countries: CountryModel[];

  artistId = [];
  artists: ArtistModel[] = [];

  singer = [];

  constructor(
    private artistService: ArtistService,
    private domSanitizer: DomSanitizer,
    private toastr: ToastrService,
    private countryService: CountryService,
    public dialogRef: MatDialogRef<SearchArtistUploadComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) { }

  ngOnInit(): void {
    console.log(this.data);
    
    this.getCountries();
  }

  choose() {

  }

  getSinger() {
    if (this.data) {
      const data = [];
      this.data.singer.forEach((id: number) => {
        const art = this.listArtist.filter(artist => artist.id === id);
        if (art.length > 0) {
          data.push(art[0]);
        }
      });
      this.singer = data;
    }
  }

  search(artistName, select?) {
    this.artistService.findByCondition(artistName).subscribe((artist: any) => {
      if (artist.code === 200) {
        this.listArtist = artist.data;
        if (select) {
          this.getSinger();
        }
        this.base64();
      } else {
        this.listArtist = [];
      }
    }, err => {
      this.showError('Có lỗi không xác định xảy ra');
    });
  }

  base64() {
    this.listArtist.forEach(artist => {
      artist.url = this.domSanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' + artist.base64);
    });
  }

  getCountries() {
    this.countryService.getCountryActive().subscribe((res: any) => {
      this.countries = res.data;
      this.search('', true);
    });
  }

  showSuccess(message: string) {
    this.toastr.success(message);
  }

  showError(message: string) {
    this.toastr.error(message);
  }

  chooseArtist(value) {
    console.log(value);
    
    this.artists = value;
  }

  close() {
    this.dialogRef.close({ event: 'cancel' });
  }

  save() {
    let singer = '';
    const artistid = [];
    this.artists.forEach((artist: ArtistModel) => {
      singer = `${singer}${artist.name}, `;
      artistid.push(artist.id);
    });
    this.dialogRef.close({ data: { singer: singer.substring(0, singer.length - 2), artistid: artistid } });
  }
}
