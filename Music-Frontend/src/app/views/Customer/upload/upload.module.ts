import { AdminModule } from './../../admin/admin.module';
import { MatRadioModule } from '@angular/material/radio';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { NgModule } from '@angular/core';
import { UploadComponent } from './upload.component';
import { CommonModule } from '@angular/common';
import {
  MatInputModule, MatButtonModule, MatFormFieldModule, MatTableModule,
  MatSelectModule, MatOptionModule, MatTabsModule, MatNativeDateModule, MatCheckboxModule
} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { UploadRoutingModule } from './upload.routing';
import { DndDirective } from './dnd.directive';
import { SearchArtistUploadComponent } from './search-artist-upload/search-artist-upload.component';


@NgModule({
  declarations: [
    UploadComponent,
    DndDirective,
    SearchArtistUploadComponent,
  ],
  imports: [
    CommonModule,
    MatInputModule,
    MatButtonModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
    NgxPaginationModule,
    MatSelectModule,
    MatOptionModule,
    MatTabsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatMomentDateModule,
    MatInputModule,
    MatRadioModule,
    UploadRoutingModule,
    MatCheckboxModule,
    AdminModule
  ],
  entryComponents: [SearchArtistUploadComponent]
})
export class UploadModule { }
