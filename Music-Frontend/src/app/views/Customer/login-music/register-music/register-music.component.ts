import { Component, OnInit, ViewEncapsulation, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDatepickerModule, DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material';
import { DatePipe } from '@angular/common';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
export const MY_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};
@Component({
  selector: 'app-register-music',
  templateUrl: './register-music.component.html',
  styleUrls: ['./register-music.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [
    MatDatepickerModule,
    DatePipe,

    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS }
  ],
})
export class RegisterMusicComponent implements OnInit {
  @Input() regis: boolean;
  @Output() login = new EventEmitter<boolean>();
  openLogin = true;
  form;

  mdtMaxDate = new Date(new Date().setDate(new Date().getDate()));
  mdtMinDate = new Date(1900, 12, 1);

  constructor(
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.form = this.fb.group(
      {
        username: ['', Validators.required],
        fullname: ['', Validators.required],
        email: ['', Validators.required],
        phone: ['', Validators.required],
        birthday: ['', Validators.required],
        password: ['', Validators.required],
        repassword: ['', Validators.required]
      }
    )
  }
  hasError(controlName: string, errorName: string) {
    return this.form.controls[controlName].hasError(errorName);
  }
  open() {
    this.regis = false;
    this.login.emit(false);
  }
}
