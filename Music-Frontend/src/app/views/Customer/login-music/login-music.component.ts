import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-login-music',
  templateUrl: './login-music.component.html',
  styleUrls: ['./login-music.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoginMusicComponent implements OnInit {
  regis = false;
  constructor(

    private modalService: MatDialog,
  ) { }

  ngOnInit() {
  }
  
  register(value: any) {
    setTimeout(() => {
      this.regis = value;
    }, 500);
  }
}
