import { AuthenPasswordComponent } from './../../profile/authen-password/authen-password.component';
import { Component, OnInit, Output, EventEmitter, ViewEncapsulation, Input } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { AccountService } from '../../../../services/common/account.service';
import { config } from '../../../../config/application.config';
import { MatDialog } from '@angular/material';
import { STORAGE_KEY } from '../../../../constants';
import { isBuffer } from 'util';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit {
  @Input() regis: boolean;
  @Output() register = new EventEmitter<boolean>();
  message;
  form;
  constructor(
    private fb: FormBuilder,
    private account: AccountService,
    private modalService: MatDialog
  ) { }

  ngOnInit() {
    this.createForm();
    this.register.emit(false);
  }

  createForm() {
    this.form = this.fb.group(
      {
        username: ['', Validators.required],
        password: ['', Validators.required]
      }
    );
  }

  hasError(controlName: string, errorName: string) {
    return this.form.controls[controlName].hasError(errorName);
  }


  login() {
    this.account.login(this.form.value.username, this.form.value.password).subscribe(next => {
      if (!next || next.code === 400) {
        this.message = 'Sai thông tin đăng nhập';
      } else {
        if (next.authorities[0].authority === 'ROLE_ADMIN') {
          this.message = 'Sai thông tin đăng nhập';
          this.account.logout();
          return;
        }
        this.account.accessToken = next.access_token;
        this.account.refreshToken = next.refresh_token;
        localStorage.setItem(STORAGE_KEY.userToken, JSON.stringify(next));
        this.modalService.closeAll();
        // window.location.reload();
      }
    }, error => {
      this.message = 'Sai thông tin đăng nhập';
    });
  }

  openRegister() {
    this.regis = true;
    this.register.emit(true);
  }

  reissuePass() {
    this.modalService.closeAll();
    const dialogRef = this.modalService.open(AuthenPasswordComponent, {
      disableClose: true,
      width: '70vw',
      panelClass: 'myapp-background-dialog'
    });
    dialogRef.afterClosed().subscribe(result => {
    });
}
}
