import { DialogsService } from './../../../../common/confirm-dialog/dialogs.service';
import { StatusModel } from '../../../../model/status.model';
import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, Inject, Optional } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { GenreModel } from '../../../../model/genre.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GenreService } from '../../../../services/genre.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { GenreCreateComponent } from '../genre-create/genre-create.component';
import { environment } from '../../../../../environments/environment';

@Component({
    selector: 'app-genre-edit',
    templateUrl: './genre-edit.component.html',
    styleUrls: ['./genre-edit.component.css']
})
export class GenreEditComponent implements OnInit {

    createForm: FormGroup;
    genre: GenreModel;
    uploadEmpty: boolean;
    imgPath: any;
    fileList: FileList;
    checkUploadSucces: boolean;
    genreId: number;
    messageErrorFile = '';

    statuses: StatusModel[] = [
        { value: 1, viewValue: 'Hoạt động' },
        { value: 0, viewValue: 'Không hoạt động' }
    ];

    constructor(
        private fb: FormBuilder,
        private http: HttpClient,
        private genreService: GenreService,
        private toastr: ToastrService,
        private dialogSv: DialogsService,
        public dialogRef: MatDialogRef<GenreEditComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
    ) {
        this.genre = data.genreEdit;
    }

    ngOnInit(): void {
        this.imgPath = null;
        this.checkUploadSucces = false;
        this.uploadEmpty = true;
        this.createForm = this.fb.group(
            {
                genreName: [this.genre.name, Validators.required],
                genreDes: ['', [Validators.required, Validators.maxLength(500)]],
                genreStatus: [this.genre.status, Validators.required]
            }
        );

    }

    selectFile(fileInputEvent: any) {
        this.genre.path = fileInputEvent.target.files[0].name;
        const formatFile = this.genre.path.substring(this.genre.path.length - 3, this.genre.path.length);
        let checkFile;
        if (formatFile !== 'jpg' && formatFile !== 'png') {
            checkFile = true;
            this.uploadEmpty = false;
            this.messageErrorFile = 'Ảnh không đúng định dạng';
        } else {
            this.messageErrorFile = '';
            checkFile = false;
            this.uploadEmpty = true;
        }

        if (checkFile) {
            return;
        }

        // tslint:disable-next-line: triple-equals
        if (this.genre.path.indexOf('.jpg') != -1 || this.genre.path.indexOf('.png') != -1) {
            const reader = new FileReader();
            reader.readAsDataURL(fileInputEvent.target.files[0]);
            reader.onload = (_event) => {
                this.imgPath = reader.result;
                this.uploadFile(fileInputEvent.target.files[0]);
            };
        } else {
            this.imgPath = null;
        }
    }

    hasError(controlName: string, errorName: string) {
        return this.createForm.controls[controlName].hasError(errorName);
    }

    saveGenre() {
        this.checkUpload();
        this.getValueFromForm();
        this.dialogSv.confirm('', 'Bạn có chắc muốn lưu không?', 'Có', 'Không').subscribe(next => {
            if (next) {
                this.genreService.createOrUpdate(this.genre).subscribe((data: any) => {
                    if (data.code === 200) {
                        this.showSuccess('Cập nhật thành công');
                        this.dialogRef.close();
                        this.genreService.setResetForm(true);
                    }
                }, err => {
                    this.showError('Có lỗi không xác định xảy ra');
                });
            }
        });

    }

    checkUpload(): boolean {
        this.messageErrorFile = '';
        if (!this.genre.path || this.genre.path.length === 0) {
            this.messageErrorFile = 'Ảnh không được để trống';
            return this.uploadEmpty = false;
        } else {
            return this.uploadEmpty = true;
        }
    }

    getValueFromForm() {
        this.genre.name = this.createForm.controls.genreName.value.trim();
        this.genre.description = this.createForm.controls.genreDes.value.trim();
        this.genre.status = this.createForm.controls.genreStatus.value;
    }

    uploadFile(fileUpload: any) {
        const file: File = fileUpload;
        const formData: FormData = new FormData();
        formData.append('file', file);
        const headers = new HttpHeaders();
        headers.append('Content-type', 'multipart/form-data');
        headers.append('Accept', 'application/json');
        const options = { headers: headers };
        this.http.post(environment.API_URL + '/file/uploadReport/genre', formData, options)
            .subscribe(
                (data: any) => {
                    if (data.code === 200) {
                        this.genre.path = data.data;
                        this.checkUploadSucces = true;
                    } else {
                        this.checkUploadSucces = false;
                    }
                },
                () => (this.showError('Có lỗi không xác định xảy ra'))
            );
    }

    showSuccess(message: string) {
        this.toastr.success(message);
    }

    showError(message: string) {
        this.toastr.error(message);
    }

    close() {
        this.dialogRef.close();
    }

}
