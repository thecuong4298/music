import { DialogsService } from './../../../common/confirm-dialog/dialogs.service';
import { GenreModel } from '../../../model/genre.model';
import { GenreService } from './../../../services/genre.service';
import { Component, OnInit, SimpleChanges } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-management-genre',
  templateUrl: './management-genre.component.html',
  styleUrls: ['./management-genre.component.css']
})
export class ManagementGenreComponent implements OnInit {

  constructor(
    private genreService: GenreService,
    private toastr: ToastrService,
    private dialogSv: DialogsService

  ) { }
  listGenre: GenreModel[];

  ngOnInit(): void {
    this.getGenres();
    this.genreService.resetForm$.subscribe(reset => {
      if (reset) {
        this.getGenres();
      }
    });
  }

  search(genreName) {
    this.genreService.findByCondition(genreName).subscribe((genre: any) => {
      if (genre.code === 200) {
        this.listGenre = genre.data;
      } else {
        this.listGenre = [];
      }
    }, err => {
      this.showError('Có lỗi không xác định xảy ra');
    });
  }

  getGenres() {
    this.genreService.getGenres().subscribe((genre: any) => {
      if (genre.code === 200) {
        this.listGenre = genre.data;
        console.log(this.listGenre);
        
      } else {
        this.listGenre = [];
      }
    }, err => {
      this.showError('Có lỗi không xác định xảy ra');
    });
  }

  lockOrUnlock(genreModel: GenreModel) {
    let title = '';
    if (genreModel.status === 1) {
      title = 'Bạn chắc chắn muốn khóa không?';
    } else {
      title = 'Bạn chắc chắn muốn mở khóa không?';
    }
    this.dialogSv.confirm('', title, 'Có', 'Không').subscribe(next => {
      if (next) {
        if (genreModel.status === 1) {
          genreModel.status = 0;
        } else {
          genreModel.status = 1;
        }

        this.genreService.createOrUpdate(genreModel).subscribe((data: any) => {
          if (data.code === 200) {
            this.showSuccess('Cập nhật thành công');
            this.genreService.setResetForm(true);
          }
        }, (err) => {
          this.showError('Có lỗi không xác định xảy ra');
        });
      }
    });
  }

  delete(genreId) {
    console.log(genreId);
    this.dialogSv.confirm('', 'Bạn có chắc muốn xóa không?', 'Có', 'Không').subscribe(next => {
      if (next) {
        this.genreService.delete(genreId).subscribe((data: any) => {
          if (data.code === 200) {
            this.showSuccess('Xóa thành công');
            this.genreService.setResetForm(true);
          }
        }, (err) => {
          this.showError('Có lỗi không xác định xảy ra');
        });
      }
    });
  }

  showSuccess(message: string) {
    this.toastr.success(message);
  }

  showError(message: string) {
    this.toastr.error(message);
  }
}
