import { GenreModel } from '../../../../model/genre.model';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-genre-search',
  templateUrl: './genre-search.component.html',
  styleUrls: ['./genre-search.component.scss']
})
export class GenreSearchComponent implements OnInit {

  @Output() search = new EventEmitter<string>();

  searchFormGroup: FormGroup;
  listGenre: GenreModel[];
  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.searchFormGroup = this.fb.group(
      {
        genreName: [''],
      }
    );
  }

  get genreName() {
    return this.searchFormGroup.get('genreName');
  }

  searchByCondition() {
    this.search.emit(this.genreName.value);
  }

}
