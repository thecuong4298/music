import { DialogsService } from './../../../../common/confirm-dialog/dialogs.service';
import { ToastrService } from 'ngx-toastr';
import { GenreService } from './../../../../services/genre.service';
import { environment } from './../../../../../environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { GenreModel } from '../../../../model/genre.model';
import { Component, OnInit, Output, EventEmitter, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
    selector: 'app-genre-create',
    templateUrl: './genre-create.component.html',
    styleUrls: ['./genre-create.component.css']
})
export class GenreCreateComponent implements OnInit {

    createForm: FormGroup;
    genre: GenreModel;
    uploadEmpty: boolean;
    imgPath: any;
    fileList: FileList;
    checkUploadSucces: boolean;
    messageErrorFile = '';

    constructor(
        private fb: FormBuilder,
        private http: HttpClient,
        private genreService: GenreService,
        private toastr: ToastrService,
        private dialogSv: DialogsService,
        public dialogRef: MatDialogRef<GenreCreateComponent>,
    ) { }

    ngOnInit(): void {
        this.genre = new GenreModel();
        this.imgPath = null;
        this.checkUploadSucces = false;
        this.uploadEmpty = true;
        this.createForm = this.fb.group(
            {
                genreName: ['', Validators.required],
                genreDes: ['', [Validators.required, Validators.maxLength(500)]]
            }
        );
    }

    selectFile(fileInputEvent: any) {
        this.messageErrorFile = '';
        this.genre.path = fileInputEvent.target.files[0].name;
        let checkFile;
        const formatFile = this.genre.path.substring(this.genre.path.length - 3, this.genre.path.length);
        if (formatFile !== 'jpg' && formatFile !== 'png') {
            checkFile = true;
            this.uploadEmpty = false;
            this.messageErrorFile = 'Ảnh không đúng định dạng';
        } else {
            this.messageErrorFile = '';
            this.uploadEmpty = true;
        }

        if (checkFile) {
            return;
        }

        if (this.genre.path.indexOf('.jpg') != -1 || this.genre.path.indexOf('.png') != -1) {
            const reader = new FileReader();
            reader.readAsDataURL(fileInputEvent.target.files[0]);
            reader.onload = (_event) => {
                this.imgPath = reader.result;
                this.uploadFile(fileInputEvent.target.files[0]);
            };
        } else {
            this.imgPath = null;
        }
    }

    hasError(controlName: string, errorName: string) {
        return this.createForm.controls[controlName].hasError(errorName);
    }

    saveGenre() {
        this.checkUpload();
        this.getValueFromForm();
        if (this.checkUploadSucces) {
            this.dialogSv.confirm('', 'Bạn có chắc muốn lưu không?', 'Có', 'Không').subscribe(next => {
                if (next) {
                    this.genreService.createOrUpdate(this.genre).subscribe((data: any) => {
                        if (data.code === 200) {
                            this.showSuccess('Thêm mới thành công');
                            this.dialogRef.close();
                            this.genreService.setResetForm(true);
                            this.genre = null;
                        }
                    }, (err) => {
                        this.showError('Có lỗi không xác định xảy ra');
                    });
                }
            });
        }
    }

    checkUpload(): boolean {
        this.messageErrorFile = '';
        if (!this.genre.path || this.genre.path.length === 0) {
            this.messageErrorFile = 'Ảnh không được để trống';
            return this.uploadEmpty = false;
        } else {
            return this.uploadEmpty = true;
        }
    }

    getValueFromForm() {
        this.genre.name = this.createForm.controls.genreName.value.trim();
        this.genre.description = this.createForm.controls.genreDes.value;
        this.genre.status = 1;
    }

    uploadFile(fileUpload: any) {
        const file: File = fileUpload;
        const formData: FormData = new FormData();
        formData.append('file', file);
        const headers = new HttpHeaders();
        headers.append('Content-type', 'multipart/form-data');
        headers.append('Accept', 'application/json');
        const options = { headers: headers };
        this.http.post(environment.API_URL + '/file/uploadReport/genre', formData, options)
            .subscribe(
                (data: any) => {
                    if (data.code === 200) {
                        this.genre.path = data.data;
                        this.checkUploadSucces = true;
                    } else {
                        this.checkUploadSucces = false;
                    }
                },
                err => (this.showError('Có lỗi không xác định xảy ra')),
            );
    }

    showSuccess(message: string) {
        this.toastr.success(message);
    }

    showError(message: string) {
        this.toastr.error(message);
    }

    close() {
        this.dialogRef.close();
    }
}
