import { ToastrService } from 'ngx-toastr';
import { GenreEditComponent } from './../genre-edit/genre-edit.component';
import { GenreCreateComponent } from './../genre-create/genre-create.component';
import { MatDialog } from '@angular/material';
import { GenreModel } from '../../../../model/genre.model';
import { Component, OnInit, Input, ViewChild, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { GenreService } from '../../../../services/genre.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-genre-list',
  templateUrl: './genre-list.component.html',
  styleUrls: ['./genre-list.component.css']
})
export class GenreListComponent implements OnInit {
  p: any;
  @Input() listGenre: GenreModel[];
  @Output() lockOrUnlock = new EventEmitter<any>();
  @Output() delete = new EventEmitter<any>();
  genreEdit: GenreModel;
  constructor(
    private dialog: MatDialog,
    private genreService: GenreService,
    private domSanitizer: DomSanitizer,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
  }

  openCreate() {
    this.genreService.setResetForm(false);
    const dialogRef = this.dialog.open(GenreCreateComponent, {
      width: '70vw',
      panelClass: 'myapp-background-dialog'
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  openEdit(genreId) {
    this.genreService.setResetForm(false);
    this.getGenreById(genreId);
  }

  getGenreById(genreId) {
    this.genreService.getById(genreId).subscribe((data: any) => {
      if (data.code === 200) {
        this.genreEdit = data.data;
        if (this.genreEdit) {
          const dialogRef = this.dialog.open(GenreEditComponent, {
            width: '70vw',
            panelClass: 'myapp-background-dialog',
            data: { genreEdit: this.genreEdit }
          });

          dialogRef.afterClosed().subscribe(result => {
          });
        }
      } else {
        this.showError(data.mstrErrors);
      }
    }, () => {
      this.showError('Có lỗi không xác định xảy ra');
    });
  }

  lockUnlock(genre) {
    this.lockOrUnlock.emit(genre);
  }

  deleteGenre(genreId) {
    this.delete.emit(genreId);
  }

  showSuccess(message: string) {
    this.toastr.success(message);
  }

  showError(message: string) {
    this.toastr.error(message);
  }
}
