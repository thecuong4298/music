import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TopicModel } from '../../../../model/topic.model';
import { MatDialog } from '@angular/material';
import { TopicService } from '../../../../services/topic.service';
import { DomSanitizer } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
import { TopicCreateComponent } from '../topic-create/topic-create.component';
import { TopicEditComponent } from '../topic-edit/topic-edit.component';

@Component({
  selector: 'app-topic-list',
  templateUrl: './topic-list.component.html',
  styleUrls: ['./topic-list.component.css']
})
export class TopicListComponent implements OnInit {
  p: any;
  @Input() listTopic: TopicModel[];
  @Output() lockOrUnlock = new EventEmitter<any>();
  @Output() delete = new EventEmitter<any>();
  topicEdit: TopicModel;
  constructor(
    private dialog: MatDialog,
    private topicService: TopicService,
    private domSanitizer: DomSanitizer,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
  }

  openCreate() {
    this.topicService.setResetForm(false);
    const dialogRef = this.dialog.open(TopicCreateComponent, {
      width: '70vw',
      panelClass: 'myapp-background-dialog'
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  openEdit(topicId) {
    this.topicService.setResetForm(false);
    this.getTopicById(topicId);
  }

  getTopicById(topicId) {
    this.topicService.getById(topicId).subscribe((data: any) => {
      if (data.code === 200) {
        this.topicEdit = data.data;
        this.topicEdit.url = this.domSanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' + this.topicEdit.base64);
        if (this.topicEdit) {
          const dialogRef = this.dialog.open(TopicEditComponent, {
            width: '70vw',
            panelClass: 'myapp-background-dialog',
            data: { topicEdit: this.topicEdit }
          });

          dialogRef.afterClosed().subscribe(result => {
          });
        }
      } else {
        this.showError(data.mstrErrors);
      }
    }, () => {
      this.showError('Có lỗi không xác định xảy ra');
    });

  }

  lockUnlock(topic) {
    this.lockOrUnlock.emit(topic);
  }

  deleteTopic(topicId) {
    this.delete.emit(topicId);
  }

  showSuccess(message: string) {
    this.toastr.success(message);
  }

  showError(message: string) {
    this.toastr.error(message);
  }

}
