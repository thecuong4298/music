import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TopicModel } from '../../../../model/topic.model';
import { StatusModel } from '../../../../model/status.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { TopicService } from '../../../../services/topic.service';
import { ToastrService } from 'ngx-toastr';
import { DialogsService } from '../../../../common/confirm-dialog/dialogs.service';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { TopicCreateComponent } from '../topic-create/topic-create.component';
import { environment } from '../../../../../environments/environment';
import { ListSelectSongComponent } from '../../../../common/list-select-song/list-select-song.component';

@Component({
  selector: 'app-topic-edit',
  templateUrl: './topic-edit.component.html',
  styleUrls: ['./topic-edit.component.css']
})
export class TopicEditComponent implements OnInit {
  createForm: FormGroup;
  topic: TopicModel;
  uploadEmpty: boolean;
  imgPath: any;
  fileList: FileList;
  checkUploadSucces: boolean;
  topicId: number;
  messageErrorFile = '';
  listSong = [];

  statuses: StatusModel[] = [
    { value: 1, viewValue: 'Hoạt động' },
    { value: 0, viewValue: 'Không hoạt động' }
  ];

  constructor(
    private fb: FormBuilder,
    private http: HttpClient,
    private topicService: TopicService,
    private toastr: ToastrService,
    private dialogSv: DialogsService,
    public dialogRef: MatDialogRef<TopicCreateComponent>,
    private dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    this.topic = data.topicEdit;
  }

  ngOnInit(): void {
    this.imgPath = null;
    this.checkUploadSucces = false;
    this.uploadEmpty = true;
    this.getSong();
    this.createForm = this.fb.group(
      {
        topicName: [this.topic.name, Validators.required],
        topicDes: [this.topic.description, Validators.required],
        topicStatus: [this.topic.status, Validators.required]
      }
    );
  }

  selectFile(fileInputEvent: any) {
    this.topic.path = fileInputEvent.target.files[0].name;
    const formatFile = this.topic.path.substring(this.topic.path.length - 3, this.topic.path.length);
    let checkFile;
    if (formatFile !== 'jpg' && formatFile !== 'png') {
      checkFile = true;
      this.uploadEmpty = false;
      this.messageErrorFile = 'Ảnh không đúng định dạng';
    } else {
      this.messageErrorFile = '';
      this.uploadEmpty = true;
    }

    if (checkFile) {
      return;
    }

    if (this.topic.path.indexOf('.jpg') != -1 || this.topic.path.indexOf('.png') != -1) {
      const reader = new FileReader();
      reader.readAsDataURL(fileInputEvent.target.files[0]);
      reader.onload = (_event) => {
        this.imgPath = reader.result;
        this.uploadFile(fileInputEvent.target.files[0]);
      };
    } else {
      this.imgPath = null;
    }
  }

  hasError(controlName: string, errorName: string) {
    return this.createForm.controls[controlName].hasError(errorName);
  }

  saveTopic() {
    this.checkUpload();
    this.getValueFromForm();
    this.dialogSv.confirm('', 'Bạn có chắc muốn lưu không?', 'Có', 'Không').subscribe(next => {
      if (next) {
        this.topicService.createOrUpdate(this.topic, this.listSong).subscribe((data: any) => {
          if (data.code === 200) {
            this.showSuccess('Cập nhật thành công');
            this.dialogRef.close();
            this.topicService.setResetForm(true);
          }
        }, err => {
          this.showError('Có lỗi không xác định xảy ra');
        });
      }
    });

  }

  checkUpload(): boolean {
    this.messageErrorFile = '';
    if (!this.topic.path || this.topic.path.length === 0) {
      this.messageErrorFile = 'Ảnh không được để trống';
      return this.uploadEmpty = false;
    } else {
      return this.uploadEmpty = true;
    }
  }

  getValueFromForm() {
    this.topic.name = this.createForm.controls.topicName.value.trim();
    this.topic.description = this.createForm.controls.topicDes.value;
    this.topic.status = this.createForm.controls.topicStatus.value;
  }

  uploadFile(fileUpload: any) {
    const file: File = fileUpload;
    const formData: FormData = new FormData();
    formData.append('file', file);
    const headers = new HttpHeaders();
    headers.append('Content-type', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    const options = { headers: headers };
    this.http.post(environment.API_URL + '/file/uploadReport/topic', formData, options)
      .subscribe(
        (data: any) => {
          if (data.code === 200) {
            this.topic.path = data.data;
            this.checkUploadSucces = true;
          } else {
            this.checkUploadSucces = false;
          }
        },
        () => (this.showError('Có lỗi không xác định xảy ra'))
      );
  }

  showSuccess(message: string) {
    this.toastr.success(message);
  }

  showError(message: string) {
    this.toastr.error(message);
  }

  close() {
    this.dialogRef.close();
  }
  getSong() {
    this.topicService.getSongByTopic(this.topic.id).subscribe((data: any) => {
      this.listSong = data.data;
    });
  }

  openSong() {
    const dialogRef = this.dialog.open(ListSelectSongComponent, {
      disableClose: true,
      width: '60vw',
      panelClass: 'myapp-background-dialog',
      data: { songs: this.listSong }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.event !== 'cancel') {
        console.log(result.data);
        this.listSong = result.data.songs;

      }
    });
  }

  delete(songId) {
    this.listSong = this.listSong.filter((song) => song.id !== songId);
  }
}
