import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { GenreModel } from '../../../../model/genre.model';

@Component({
  selector: 'app-topic-search',
  templateUrl: './topic-search.component.html',
  styleUrls: ['./topic-search.component.css']
})
export class TopicSearchComponent implements OnInit {
  @Output() search = new EventEmitter<string>();

  searchFormGroup: FormGroup;
  listGenre: GenreModel[];

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.searchFormGroup = this.fb.group(
      {
        topicName: [''],
      }
    );
  }

  get topicName() {
    return this.searchFormGroup.get('topicName');
  }

  searchByCondition() {
    this.search.emit(this.topicName.value);
  }

}
