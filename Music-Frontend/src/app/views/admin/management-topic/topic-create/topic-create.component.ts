import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TopicModel } from '../../../../model/topic.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { TopicService } from '../../../../services/topic.service';
import { ToastrService } from 'ngx-toastr';
import { DialogsService } from '../../../../common/confirm-dialog/dialogs.service';
import { MatDialogRef, MatDialog } from '@angular/material';
import { environment } from '../../../../../environments/environment';
import { ListSelectSongComponent } from '../../../../common/list-select-song/list-select-song.component';
import { Song } from '../../../../share/model/song.model';

@Component({
  selector: 'app-topic-create',
  templateUrl: './topic-create.component.html',
  styleUrls: ['./topic-create.component.css']
})
export class TopicCreateComponent implements OnInit {
  createForm: FormGroup;
  topic: TopicModel;
  uploadEmpty: boolean;
  imgPath: any;
  fileList: FileList;
  checkUploadSucces: boolean;
  messageErrorFile = '';
  listSong: Song[];

  constructor(
    private fb: FormBuilder,
    private http: HttpClient,
    private topicService: TopicService,
    private toastr: ToastrService,
    private dialogSv: DialogsService,
    public dialogRef: MatDialogRef<TopicCreateComponent>,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.topic = new TopicModel();
    this.imgPath = null;
    this.checkUploadSucces = false;
    this.uploadEmpty = true;
    this.createForm = this.fb.group(
      {
        topicName: ['', Validators.required],
        topicDes: ['', Validators.required]
      }
    );
  }

  selectFile(fileInputEvent: any) {
    this.messageErrorFile = '';
    this.topic.path = fileInputEvent.target.files[0].name;
    let checkFile;
    const formatFile = this.topic.path.substring(this.topic.path.length - 3, this.topic.path.length);
    if (formatFile !== 'jpg' && formatFile !== 'png') {
      checkFile = true;
      this.uploadEmpty = false;
      this.messageErrorFile = 'Ảnh không đúng định dạng';
    } else {
      this.messageErrorFile = '';
      this.uploadEmpty = true;
    }

    if (checkFile) {
      return;
    }

    if (this.topic.path.indexOf('.jpg') != -1 || this.topic.path.indexOf('.png') != -1) {
      const reader = new FileReader();
      reader.readAsDataURL(fileInputEvent.target.files[0]);
      reader.onload = (_event) => {
        this.imgPath = reader.result;
        this.uploadFile(fileInputEvent.target.files[0]);
      };
    } else {
      this.imgPath = null;
    }
  }

  hasError(controlName: string, errorName: string) {
    return this.createForm.controls[controlName].hasError(errorName);
  }

  saveTopic() {
    this.checkUpload();
    this.getValueFromForm();
    if (this.checkUploadSucces) {
      this.dialogSv.confirm('', 'Bạn có chắc muốn lưu không?', 'Có', 'Không').subscribe(next => {
        if (next) {
          this.topicService.createOrUpdate(this.topic, this.listSong).subscribe((data: any) => {
            if (data.code === 200) {
              this.showSuccess('Thêm mới thành công');
              this.dialogRef.close();
              this.topicService.setResetForm(true);
            }
          }, (err) => {
            this.showError('Có lỗi không xác định xảy ra');
          });
        }
      });
    }
  }

  checkUpload(): boolean {
    this.messageErrorFile = '';
    if (!this.topic.path || this.topic.path.length === 0) {
      this.messageErrorFile = 'Ảnh không được để trống';
      return this.uploadEmpty = false;
    } else {
      return this.uploadEmpty = true;
    }
  }

  getValueFromForm() {
    this.topic.name = this.createForm.controls.topicName.value.trim();
    this.topic.description = this.createForm.controls.topicDes.value;
    this.topic.status = 1;
  }

  uploadFile(fileUpload: any) {
    const file: File = fileUpload;
    const formData: FormData = new FormData();
    formData.append('file', file);
    const headers = new HttpHeaders();
    headers.append('Content-type', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    const options = { headers: headers };
    this.http.post(environment.API_URL + '/file/uploadReport/topic', formData, options)
      .subscribe(
        (data: any) => {
          if (data.code === 200) {
            this.topic.path = data.data;
            this.checkUploadSucces = true;
          } else {
            this.checkUploadSucces = false;
          }
        },
        err => (this.showError('Có lỗi không xác định xảy ra')),
      );
  }

  showSuccess(message: string) {
    this.toastr.success(message);
  }

  showError(message: string) {
    this.toastr.error(message);
  }

  close() {
    this.dialogRef.close();
  }

  getSong() {
    // getSong(topicId: number) {
    //   this.topicService.getSongByTopic(topicId).subscribe((data: any) => {
    //     this.songs = data.data;
    //   });
    // }
  }

  openSong() {
    const dialogRef = this.dialog.open(ListSelectSongComponent, {
      disableClose: true,
      width: '60vw',
      panelClass: 'myapp-background-dialog',
      data: {songs: this.listSong}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.event !== 'cancel') {
        console.log(result.data);
        this.listSong = result.data.songs;

      }
    });
  }
  delete(songId) {
    this.listSong = this.listSong.filter((song) => song.id !== songId);
  }
}
