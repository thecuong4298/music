import { Component, OnInit } from '@angular/core';
import { DialogsService } from '../../../common/confirm-dialog/dialogs.service';
import { DomSanitizer } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
import { TopicService } from '../../../services/topic.service';
import { TopicModel } from '../../../model/topic.model';

@Component({
  selector: 'app-management-topic',
  templateUrl: './management-topic.component.html',
  styleUrls: ['./management-topic.component.css']
})
export class ManagementTopicComponent implements OnInit {

  constructor(
    private topicService: TopicService,
    private toastr: ToastrService,
    private domSanitizer: DomSanitizer,
    private dialogSv: DialogsService
  ) { }

  listTopic: TopicModel[];

  ngOnInit(): void {
    this.getTopics();
    this.topicService.resetForm$.subscribe(reset => {
      if (reset) {
        this.getTopics();
      }
    });
  }

  base64() {
    this.listTopic.forEach(topic => {
      topic.url = this.domSanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' + topic.base64);
    });
  }

  getTopics() {
    this.topicService.getAllTopic().subscribe((topic: any) => {
      if (topic.code === 200) {
        this.listTopic = topic.data;
        this.base64();
      } else {
        this.listTopic = [];
      }
    }, err => {
      this.showError('Có lỗi không xác định xảy ra');
    });
  }

  showSuccess(message: string) {
    this.toastr.success(message);
  }

  showError(message: string) {
    this.toastr.error(message);
  }

  delete(topicId) {
    console.log(topicId);
    this.dialogSv.confirm('', 'Bạn có chắc muốn xóa không?', 'Có', 'Không').subscribe(next => {
      if (next) {
        this.topicService.delete(topicId).subscribe((data: any) => {
          if (data.code === 200) {
            this.showSuccess('Xóa thành công');
            this.topicService.setResetForm(true);
          }
        }, (err) => {
          this.showError('Có lỗi không xác định xảy ra');
        });
      }
    });
  }

  lockOrUnlock(topicModel: TopicModel) {
    let title = '';
    if (topicModel.status === 1) {
      title = 'Bạn chắc chắn muốn khóa không?';
    } else {
      title = 'Bạn chắc chắn muốn mở khóa không?';
    }
    this.dialogSv.confirm('', title, 'Có', 'Không').subscribe(next => {
      if (next) {
        if (topicModel.status === 1) {
          topicModel.status = 0;
        } else {
          topicModel.status = 1;
        }

        this.topicService.createOrUpdate(topicModel, null).subscribe((data: any) => {
          if (data.code === 200) {
            this.showSuccess('Cập nhật thành công');
            this.topicService.setResetForm(true);
          }
        }, (err) => {
          this.showError('Có lỗi không xác định xảy ra');
        });
      }
    });
  }

  search(topicName) {
    this.topicService.findByCondition(topicName).subscribe((topic: any) => {
      if (topic.code === 200) {
        this.listTopic = topic.data;
        this.base64();
      } else {
        this.listTopic = [];
      }
    }, err => {
      this.showError('Có lỗi không xác định xảy ra');
    });
  }
}
