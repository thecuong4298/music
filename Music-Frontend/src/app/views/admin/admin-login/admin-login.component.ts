import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { AccountService } from '../../../services/common/account.service';
import { STORAGE_KEY } from '../../../constants';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.scss']
})
export class AdminLoginComponent implements OnInit {
  form: FormGroup;
  message;
  constructor(
    private fb: FormBuilder,
    private account: AccountService,
    private route: Router
  ) { }

  ngOnInit() {
    this.form = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  login() {
    this.account.login(this.form.value.username, this.form.value.password).subscribe(next => {
      if (!next || next.code === 400) {
        this.message = 'Sai thông tin đăng nhập';
      } else {
        if (next.authorities[0].authority !== 'ROLE_ADMIN') {
          this.message = 'Sai thông tin đăng nhập';
          this.account.logout();
          return;
        }
        this.account.accessToken = next.access_token;
        this.account.refreshToken = next.refresh_token;
        localStorage.setItem(STORAGE_KEY.userToken, JSON.stringify(next));
        this.route.navigateByUrl('admin');
      }
    });
  }
}
