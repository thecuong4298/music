import { ToastrService } from 'ngx-toastr';
import { UserEditComponent } from '../user-edit/user-edit.component';
import { UserCreateComponent } from '../user-create/user-create.component';
import { MatDialog } from '@angular/material';
import { Component, OnInit, Input, ViewChild, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { UserModel } from '../../../../model/user.model';
import { UserService } from '../../../../services/user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  p: any;
  d = new Date();
  @Input() listUser: UserModel[];
  @Output() lockOrUnlock = new EventEmitter<any>();
  @Output() delete = new EventEmitter<any>();
  userEdit: UserModel;
  constructor(
    private dialog: MatDialog,
    private userService: UserService,
    private domSanitizer: DomSanitizer,
    private toastr: ToastrService,
  ) { }

  ngOnInit(): void {
  }

  openCreate() {
    this.userService.setResetForm(false);
    const dialogRef = this.dialog.open(UserCreateComponent, {

      width: '70vw',
      panelClass: 'myapp-background-dialog'
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  openEdit(userId) {
    this.userService.setResetForm(false);
    this.getUserById(userId);
  }

  getUserById(userId) {
    this.userService.getById(userId).subscribe((data: any) => {
      if (data.code === 200) {
        this.userEdit = data.data;
        this.userEdit.url = this.domSanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' + this.userEdit.base64);
        const dialogRef = this.dialog.open(UserEditComponent, {
          width: '70vw',
          panelClass: 'myapp-background-dialog',
          data: { userEdit: this.userEdit }
        });

        dialogRef.afterClosed().subscribe(result => {
        });
      } else {
        this.showError(data.mstrErrors);
      }
    }, () => {
      this.showError('Có lỗi không xác định xảy ra');
    });

  }

  lockUnlock(user) {
    this.lockOrUnlock.emit(user);
  }

  deleteGenre(userId) {
    this.delete.emit(userId);
  }

  showSuccess(message: string) {
    this.toastr.success(message);
  }

  showError(message: string) {
    this.toastr.error(message);
  }
}
