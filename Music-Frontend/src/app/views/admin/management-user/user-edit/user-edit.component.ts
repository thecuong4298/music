import { DialogsService } from '../../../../common/confirm-dialog/dialogs.service';
import { StatusModel } from '../../../../model/status.model';
import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, Inject, Optional } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UserCreateComponent } from '../user-create/user-create.component';
import { environment } from '../../../../../environments/environment';
import { UserService } from '../../../../services/user.service';
import { UserModel } from '../../../../model/user.model';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css']
})
export class UserEditComponent implements OnInit {

  createForm: FormGroup;
  user: UserModel;
  uploadEmpty: boolean;
  imgPath: any;
  fileList: FileList;
  checkUploadSucces: boolean;
  userId: number;
  messageErrorFile = '';

  statuses: StatusModel[] = [
    { value: 1, viewValue: 'Hoạt động' },
    { value: 0, viewValue: 'Không hoạt động' }
  ];

  constructor(
    private fb: FormBuilder,
    private http: HttpClient,
    private userService: UserService,
    private toastr: ToastrService,
    private dialogSv: DialogsService,
    public dialogRef: MatDialogRef<UserCreateComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    this.user = data.userEdit;
  }

  ngOnInit(): void {
    this.imgPath = null;
    this.checkUploadSucces = false;
    this.uploadEmpty = true;
    this.createForm = this.fb.group(
      {
        userName: [this.user.username, Validators.required],
        userBirth: [this.user.birthday,Validators.required],
        userEmail: [this.user.email, Validators.required],
        userFullName:[this.user.fullName,Validators.required],
        userPassWord: [this.user.password,Validators.required],
        userStatus: [this.user.status, Validators.required],
      }
    );
    this.uploadEmpty = true;
  }

  selectFile(fileInputEvent: any) {
    this.user.path = fileInputEvent.target.files[0].name;
    const formatFile = this.user.path.substring(this.user.path.length - 3, this.user.path.length);
    let checkFile;
    if (formatFile !== 'jpg' && formatFile !== 'png') {
      checkFile = true;
      this.uploadEmpty = false;
      this.messageErrorFile = 'Ảnh không đúng định dạng';
    } else {
      this.messageErrorFile = '';
      this.uploadEmpty = true;
    }

    if (checkFile) {
      return;
    }

    if (this.user.path.indexOf('.jpg') != -1 || this.user.path.indexOf('.png') != -1) {
      const reader = new FileReader();
      reader.readAsDataURL(fileInputEvent.target.files[0]);
      reader.onload = (_event) => {
        this.imgPath = reader.result;
        this.uploadFile(fileInputEvent.target.files[0]);
      };
    } else {
      this.imgPath = null;
    }
  }

  hasError(controlName: string, errorName: string) {
    return this.createForm.controls[controlName].hasError(errorName);
  }

  saveUserCreate() {
    this.checkUpload();
    this.getValueFromForm();
    this.dialogSv.confirm('', 'Bạn có chắc muốn lưu không?', 'Có', 'Không').subscribe(next => {
      if (next) {
        this.user.password = null;
        this.userService.saveUser(this.user).subscribe((data: any) => {
          if (data.code === 200) {
            this.showSuccess('Cập nhật thành công');
            this.dialogRef.close();
            this.userService.setResetForm(true);
          }
        }, err => {
          this.showError('Có lỗi không xác định xảy ra');
        });
      }
    });

  }

  checkUpload(): boolean {
    this.messageErrorFile = '';
    if (!this.user.path || this.user.path.length === 0) {
      this.messageErrorFile = 'Ảnh không được để trống';
      return this.uploadEmpty = false;
    } else {
      return this.uploadEmpty = true;
    }
  }

  getValueFromForm() {
    this.user.username = this.createForm.controls.userName.value.trim();
    this.user.birthday = this.createForm.controls.userBirth.value;
    this.user.email = this.createForm.controls.userEmail.value;
    this.user.fullName = this.createForm.controls.userFullName.value;
    this.user.password = this.createForm.controls.userPassWord.value;
    this.user.status = this.createForm.controls.userStatus.value;
  }

  uploadFile(fileUpload: any) {
    const file: File = fileUpload;
    const formData: FormData = new FormData();
    formData.append('file', file);
    const headers = new HttpHeaders();
    headers.append('Content-type', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    const options = { headers: headers };
    this.http.post(environment.API_URL + '/file/uploadReport/user', formData, options)
      .subscribe(
        (data: any) => {
          if (data.code === 200) {
            this.user.pathAvatar = data.data;
            this.checkUploadSucces = true;
          } else {
            this.checkUploadSucces = false;
          }
        },
        () => (this.showError('Có lỗi không xác định xảy ra'))
      );
  }

  showSuccess(message: string) {
    this.toastr.success(message);
  }

  showError(message: string) {
    this.toastr.error(message);
  }

  close() {
    this.dialogRef.close();
  }

}
