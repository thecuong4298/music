import { DialogsService } from '../../../../common/confirm-dialog/dialogs.service';
import { ToastrService } from 'ngx-toastr';
import { GenreService } from '../../../../services/genre.service';
import { environment } from '../../../../../environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { GenreModel } from '../../../../model/genre.model';
import { Component, OnInit, Output, EventEmitter, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UserModel } from '../../../../model/user.model';
import { UserService } from '../../../../services/user.service';
import { StatusModel } from '../../../../model/status.model';
import { GrenderModel } from '../../../../model/grender.model';

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.css']
})
export class UserCreateComponent implements OnInit {

  createForm: FormGroup;
  user: UserModel;
  uploadEmpty: boolean;
  imgPath: any;
  fileList: FileList;
  checkUploadSucces: boolean;
  messageErrorFile = '';
  
  userGrenders: GrenderModel[] = [
    { value: 1, viewValue: 'Nam' },
    { value: 0, viewValue: 'Nữ' }
  ];

  constructor(
    private fb: FormBuilder,
    private http: HttpClient,
    private userService: UserService,
    private toastr: ToastrService,
    private dialogSv: DialogsService,
    public dialogRef: MatDialogRef<UserCreateComponent>,
  ) { }

  ngOnInit(): void {
    this.user = new UserModel();
    this.imgPath = null;
    this.checkUploadSucces = false;
    this.uploadEmpty = true;
    this.createForm = this.fb.group(
      {
        userName: ['', Validators.required],
        userEmail: ['', Validators.required],
        userFullName: ['',Validators.required],
        userPhone: ['',Validators.required],
        userGrender: ['', Validators.required],
        passWord: ['',Validators.required]



      }
    );
  }

  selectFile(fileInputEvent: any) {
    this.messageErrorFile = '';
    this.user.path = fileInputEvent.target.files[0].name;
    let checkFile;
    const formatFile = this.user.path.substring(this.user.path.length - 3, this.user.path.length);
    if (formatFile !== 'jpg' && formatFile !== 'png') {
      checkFile = true;
      this.uploadEmpty = false;
      this.messageErrorFile = 'Ảnh không đúng định dạng';
    } else {
      this.messageErrorFile = '';
      this.uploadEmpty = true;
    }

    if (checkFile) {
      return;
    }

    if (this.user.path.indexOf('.jpg') != -1 || this.user.path.indexOf('.png') != -1) {
      const reader = new FileReader();
      reader.readAsDataURL(fileInputEvent.target.files[0]);
      reader.onload = (_event) => {
        this.imgPath = reader.result;
        this.uploadFile(fileInputEvent.target.files[0]);
      };
    } else {
      this.imgPath = null;
    }
  }

  hasError(controlName: string, errorName: string) {
    return this.createForm.controls[controlName].hasError(errorName);
  }

  saveUserCreate() {
    this.checkUpload();
    this.getValueFromForm();
    if (this.checkUploadSucces) {
      this.dialogSv.confirm('', 'Bạn có chắc muốn lưu không?', 'Có', 'Không').subscribe(next => {
        if (next) {
          this.userService.saveUser(this.user).subscribe((data: any) => {
            if (data.code === 200) {
              this.showSuccess('Thêm mới thành công');
              this.dialogRef.close();
              this.userService.setResetForm(true);
            }
          }, (err) => {
            this.showError('Có lỗi không xác định xảy ra');
          });
        }
      });
    }
  }

  checkUpload(): boolean {
    this.messageErrorFile = '';
    if (!this.user.path || this.user.path.length === 0) {
      this.messageErrorFile = 'Ảnh không được để trống';
      return this.uploadEmpty = false;
    } else {
      return this.uploadEmpty = true;
    }
  }

  getValueFromForm() {
    console.log("=>>>>>>",this.user.username,this.user.email,this.user.fullName, this.user.phone);
    this.user.username = this.createForm.controls.userName.value;
    this.user.email = this.createForm.controls.userEmail.value;
    this.user.fullName = this.createForm.controls.userFullName.value;
    this.user.phone = this.createForm.controls.userPhone.value;
    this.user.password = this.createForm.controls.passWord.value;
    this.user.gender = this.createForm.controls.userGrender.value;
    this.user.status = 1;
  }

  uploadFile(fileUpload: any) {
    const file: File = fileUpload;
    const formData: FormData = new FormData();
    formData.append('file', file);
    const headers = new HttpHeaders();
    headers.append('Content-type', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    const options = { headers: headers };
    this.http.post(environment.API_URL + '/file/uploadReport/user', formData, options)
      .subscribe(
        (data: any) => {
          if (data.code === 200) {
            this.user.pathAvatar = data.data;
            this.checkUploadSucces = true;
          } else {
            this.checkUploadSucces = false;
          }
        },
        err => (this.showError('Có lỗi không xác định xảy ra')),
      );
  }

  showSuccess(message: string) {
    this.toastr.success(message);
  }

  showError(message: string) {
    this.toastr.error(message);
  }

  close() {
    this.dialogRef.close();
  }
}
