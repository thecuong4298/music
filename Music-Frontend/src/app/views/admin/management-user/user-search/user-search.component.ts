import { GenreModel } from '../../../../model/genre.model';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { UserModel } from '../../../../model/user.model';

@Component({
  selector: 'app-user-search',
  templateUrl: './user-search.component.html',
  styleUrls: ['./user-search.component.scss']
})
export class UserSearchComponent implements OnInit {

  @Output() search = new EventEmitter<string>();

  searchFormGroup: FormGroup;
  listUser: UserModel[];
  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.searchFormGroup = this.fb.group(
      {
        userName: [''],
      }
    );
  }

  get userName() {
    return this.searchFormGroup.get('userName');
  }

  searchByCondition() {
    this.search.emit(this.userName.value);
  }

}
