import { DialogsService } from '../../../common/confirm-dialog/dialogs.service';
import { Component, OnInit, SimpleChanges } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { DomSanitizer } from '@angular/platform-browser';
import { UserService } from '../../../services/user.service';
import { UserModel } from '../../../model/user.model';
import { AccountService } from '../../../services/common/account.service';

@Component({
  selector: 'app-management-user-customer',
  templateUrl: './management-user-customer.component.html',
  styleUrls: ['./management-user-customer.component.css']
})
export class ManagementUserCustomerComponent implements OnInit {

  constructor(
    private userService: UserService,
    private toastr: ToastrService,
    private domSanitizer: DomSanitizer,
    private dialogSv: DialogsService,
    private account: AccountService

  ) { }
  listUser: UserModel[];

  ngOnInit(): void {
    this.getUsers();
    this.userService.resetForm$.subscribe(reset => {
      if (reset) {
        this.getUsers();
      }
    });
  }

  search(userName) {
    this.userService.findByCondition(userName).subscribe((users: any) => {
      if (users.code === 200) {
        this.listUser = users.data;
        this.base64();
      } else {
        this.listUser = [];
      }
    }, err => {
      this.showError('Có lỗi không xác định xảy ra');
    });
  }

  base64() {
    this.listUser.forEach(users => {
      users.url = this.domSanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' + users.base64);
    });
  }

  getUsers() {
    this.userService.getAllUser().subscribe((users: any) => {
      if (users.code === 200) {
        this.listUser = users.data;
        this.base64();
      } else if(users.code == 403) {
        this.account.logout();
      }
    }, err => {
      this.showError('Có lỗi không xác định xảy ra');
    });
  }

  lockOrUnlock(userModel: UserModel) {
    let title = '';
    if (userModel.status === 1) {
      title = 'Bạn chắc chắn muốn khóa không?';
    } else {
      title = 'Bạn chắc chắn muốn mở khóa không?';
    }
    this.dialogSv.confirm('', title, 'Có', 'Không').subscribe(next => {
      if (next) {
        if (userModel.status === 1) {
          userModel.status = 0;
        } else {
          userModel.status = 1;
        }

        this.userService.saveUser(userModel).subscribe((data: any) => {
          if (data.code === 200) {
            this.showSuccess('Cập nhật thành công');
            this.userService.setResetForm(true);
          }
        }, (err) => {
          this.showError('Có lỗi không xác định xảy ra');
        });
      }
    });
  }

  delete(userId) {
    console.log(userId);
    this.dialogSv.confirm('', 'Bạn có chắc muốn xóa không?', 'Có', 'Không').subscribe(next => {
      if (next) {
        this.userService.delete(userId).subscribe((data: any) => {
          if (data.code === 200) {
            this.showSuccess('Xóa thành công');
            this.userService.setResetForm(true);
          }
        }, (err) => {
          this.showError('Có lỗi không xác định xảy ra');
        });
      }
    });
  }

  showSuccess(message: string) {
    this.toastr.success(message);
  }

  showError(message: string) {
    this.toastr.error(message);
  }
}
