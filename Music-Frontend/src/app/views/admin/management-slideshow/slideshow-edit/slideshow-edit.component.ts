import { DialogsService } from '../../../../common/confirm-dialog/dialogs.service';
import { StatusModel } from '../../../../Model/status.model';
import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, Inject, Optional } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { environment } from '../../../../../environments/environment';
import { SlideShowModel } from '../../../../Model/slideshow.model';
import { SlideShowService } from '../../../../services/slideshow.service';
import { SliderShowCreateComponent } from '../slideshow-create/slideshow-create.component';
import { AlbumService } from '../../../../services/album.service';
import { TopicService } from '../../../../services/topic.service';
import { SongService } from '../../../../services/song.service';
import { SelectAlbumComponent } from '../../../../common/select-album/select-album.component';

@Component({
  selector: 'app-slideshow-edit',
  templateUrl: './slideshow-edit.component.html',
  styleUrls: ['./slideshow-edit.component.css']
})
export class SlideShowEditComponent implements OnInit {

  createForm: FormGroup;
  slideshow: SlideShowModel;
  uploadEmpty: boolean;
  imgPath: any;
  fileList: FileList;
  checkUploadSucces: boolean;
  genreId: number;
  messageErrorFile = '';

  statuses: StatusModel[] = [
    { value: 1, viewValue: 'Hoạt động' },
    { value: 0, viewValue: 'Không hoạt động' }
  ];

  constructor(
    private fb: FormBuilder,
    private http: HttpClient,
    private slideshowService: SlideShowService,
    private toastr: ToastrService,
    private dialogSv: DialogsService,
    public dialogRef: MatDialogRef<SliderShowCreateComponent>,
    private albumService: AlbumService,
    private topicService: TopicService,
    private dialog: MatDialog,
    private songService: SongService,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    this.slideshow = data.slideshowEdit;
  }
  select;
  id;

  ngOnInit(): void {
    this.imgPath = this.data.slideshowEdit.url;
    this.checkUploadSucces = false;
    this.uploadEmpty = true;
    this.id = this.slideshow.path.substring(this.slideshow.path.indexOf('/') + 1, this.slideshow.path.length);
    this.getItem();
  }

  Form() {
    this.createForm = this.fb.group(
      {
        slideTitle: [this.slideshow.title, Validators.required],
        description: [this.slideshow.description, Validators.required],
        type: [this.slideshow.path.substring(0, this.slideshow.path.indexOf('/')), Validators.required],
        select: [this.select.name]
      }
    );
    console.log(this.createForm.value);
    
  }

  selectFile(fileInputEvent: any) {
    this.slideshow.path = fileInputEvent.target.files[0].name;
    const formatFile = this.slideshow.path.substring(this.slideshow.path.length - 3, this.slideshow.path.length);
    let checkFile;
    if (formatFile !== 'jpg' && formatFile !== 'png') {
      checkFile = true;
      this.uploadEmpty = false;
      this.messageErrorFile = 'Ảnh không đúng định dạng';
    } else {
      this.messageErrorFile = '';
      this.uploadEmpty = true;
    }

    if (checkFile) {
      return;
    }

    if (this.slideshow.path.indexOf('.jpg') != -1 || this.slideshow.path.indexOf('.png') != -1) {
      const reader = new FileReader();
      reader.readAsDataURL(fileInputEvent.target.files[0]);
      reader.onload = (_event) => {
        this.imgPath = reader.result;
        this.uploadFile(fileInputEvent.target.files[0]);
      };
    } else {
      this.imgPath = null;
    }
  }

  hasError(controlName: string, errorName: string) {
    return this.createForm.controls[controlName].hasError(errorName);
  }

  saveSlideShow() {
    this.checkUpload();
    this.getValueFromForm();
    console.log(this.createForm.value);
    
    this.dialogSv.confirm('', 'Bạn có chắc muốn lưu không?', 'Có', 'Không').subscribe(next => {
      if (next) {
        this.slideshowService.createOrUpdate(this.slideshow).subscribe((data: any) => {
          if (data.code === 200) {
            this.showSuccess('Cập nhật thành công');
            this.dialogRef.close();
            this.slideshowService.setResetForm(true);
          }
        }, err => {
          this.showError('Có lỗi không xác định xảy ra');
        });
      }
    });

  }

  getItem() {
    if (this.data.type == 'album') {
      this.albumService.getAlbumById(this.id).subscribe((album: any) => {
        if (album.code === 200) {
          this.select = album.data;
          this.Form();
        } else {
          this.select = null;
        }
      }, err => {
        this.showError('Có lỗi không xác định xảy ra');
      });
    } else if (this.data.type == 'topic') {
      this.topicService.getTopicById(this.id).subscribe((topic: any) => {
        if (topic.code === 200) {
          this.select = topic.data;
          this.Form();
        } else {
          this.select = null;
        }
      }, err => {
        this.showError('Có lỗi không xác định xảy ra');
      });
    } else {
      this.songService.getSongById(this.id).subscribe((song: any) => {
        if (song.code === 200) {
          this.select = song.data;
          this.Form();
        } else {
          this.select = null;
        }
      }, err => {
        this.showError('Có lỗi không xác định xảy ra');
      });
    }
  }

  checkUpload(): boolean {
    this.messageErrorFile = '';
    if (!this.slideshow.path || this.slideshow.path.length === 0) {
      this.messageErrorFile = 'Ảnh không được để trống';
      return this.uploadEmpty = false;
    } else {
      return this.uploadEmpty = true;
    }
  }

  getValueFromForm() {
    this.slideshow.description = this.createForm.controls.description.value.trim();
    this.slideshow.title = this.createForm.controls.slideTitle.value;
    this.slideshow.path = this.createForm.controls.type.value + '/' + this.select.id;
    this.slideshow.status = 1;
  }

  uploadFile(fileUpload: any) {
    const file: File = fileUpload;
    const formData: FormData = new FormData();
    formData.append('file', file);
    const headers = new HttpHeaders();
    headers.append('Content-type', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    const options = { headers: headers };
    this.http.post(environment.API_URL + '/file/uploadReport/slideshow', formData, options)
      .subscribe(
        (data: any) => {
          if (data.code === 200) {
            this.slideshow.pathImg = data.data;
            this.checkUploadSucces = true;
          } else {
            this.checkUploadSucces = false;
          }
        },
        () => (this.showError('Có lỗi không xác định xảy ra'))
      );
  }

  showSuccess(message: string) {
    this.toastr.success(message);
  }

  showError(message: string) {
    this.toastr.error(message);
  }

  close() {
    this.dialogRef.close();
  }
  openType() {
    if (!this.createForm.controls.type.value || this.createForm.controls.type.value.length == 0) {
      return;
    }
    const dialogRef = this.dialog.open(SelectAlbumComponent, {
      disableClose: true,
      width: '60vw',
      panelClass: 'myapp-background-dialog',
      data: { type: this.createForm.controls.type.value }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.event !== 'cancel') {
        console.log(result.data);
        this.select = result.data.item;
        this.createForm.controls.select.setValue(this.select.name);
      }
    });
  }
}
