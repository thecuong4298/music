import { ToastrService } from 'ngx-toastr';
import { SlideShowEditComponent } from '../slideshow-edit/slideshow-edit.component';
import { MatDialog } from '@angular/material';
import { Component, OnInit, Input, ViewChild, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { SlideShowModel } from '../../../../Model/slideshow.model';
import { SlideShowService } from '../../../../services/slideshow.service';
import { SliderShowCreateComponent } from '../slideshow-create/slideshow-create.component';

@Component({
  selector: 'app-slideshow-list',
  templateUrl: './slideshow-list.component.html',
  styleUrls: ['./slideshow-list.component.css']
})
export class SlideShowListComponent implements OnInit {
  p: any;
  @Input() listSlideShow: SlideShowModel[];
  @Output() lockOrUnlock = new EventEmitter<any>();
  @Output() delete = new EventEmitter<any>();
  slideshowEdit: SlideShowModel;
  constructor(
    private dialog: MatDialog,
    private slideshowService: SlideShowService,
    private domSanitizer: DomSanitizer,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
  }

  openCreate() {
    this.slideshowService.setResetForm(false);
    const dialogRef = this.dialog.open(SliderShowCreateComponent, {
      width: '70vw',
      panelClass: 'myapp-background-dialog'
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  openEdit(slideshowId) {
    this.slideshowService.setResetForm(false);
    this.getSlideShoweById(slideshowId);
  }

  getSlideShoweById(slideshowId) {
    this.slideshowService.getById(slideshowId).subscribe((data: any) => {
      if (data.code === 200) {
        this.slideshowEdit = data.data;
        this.slideshowEdit.url = this.domSanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' + this.slideshowEdit.base64);
        if (this.slideshowEdit) {
          const dialogRef = this.dialog.open(SlideShowEditComponent, {
            width: '70vw',
            panelClass: 'myapp-background-dialog',
            data: { slideshowEdit: this.slideshowEdit }
          });

          dialogRef.afterClosed().subscribe(result => {
          });
        }
      } else {
        this.showError(data.mstrErrors);
      }
    }, () => {
      this.showError('Có lỗi không xác định xảy ra');
    });

  }

  lockUnlock(slideshow) {
    this.lockOrUnlock.emit(slideshow);
  }

  deleteGenre(slideshowId) {
    this.delete.emit(slideshowId);
  }

  showSuccess(message: string) {
    this.toastr.success(message);
  }

  showError(message: string) {
    this.toastr.error(message);
  }
}
