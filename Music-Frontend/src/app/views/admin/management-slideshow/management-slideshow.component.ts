import { DialogsService } from '../../../common/confirm-dialog/dialogs.service';
import { Component, OnInit, SimpleChanges } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { DomSanitizer } from '@angular/platform-browser';
import { SlideShowService } from '../../../services/slideshow.service';
import { SlideShowModel } from '../../../Model/slideshow.model';

@Component({
  selector: 'app-management-slideshow',
  templateUrl: './management-slideshow.component.html',
  styleUrls: ['./management-slideshow.component.css']
})
export class ManagementSlideShowComponent implements OnInit {

  constructor(
    private slideshowservice: SlideShowService,
    private toastr: ToastrService,
    private domSanitizer: DomSanitizer,
    private dialogSv: DialogsService

  ) { }
  listSlideShow: SlideShowModel[];

  ngOnInit(): void {
    this.getGenres();
    this.slideshowservice.resetForm$.subscribe(reset => {
      if (reset) {
        this.getGenres();
      }
    });
  }

  search(slideShowName) {
    this.slideshowservice.findByCondition(slideShowName).subscribe((slide: any) => {
      if (slide.code === 200) {
        this.listSlideShow = slide.data;
        this.base64();
      } else {
        this.listSlideShow = [];
      }
    }, err => {
      this.showError('Có lỗi không xác định xảy ra');
    });
  }

  base64() {
    this.listSlideShow.forEach(slide => {
      slide.url = this.domSanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' + slide.base64);
    });
  }

  getGenres() {
    this.slideshowservice.getSlide().subscribe((slide: any) => {
   
      if (slide.code === 200) {
        this.listSlideShow = slide.data;
        this.base64();
      } else {
        this.listSlideShow = [];
      }
    }, err => {
      this.showError('Có lỗi không xác định xảy ra');
    });
  }

  lockOrUnlock(slideshowModele: SlideShowModel) {
    let title = '';
    if (slideshowModele.status === 1) {
      title = 'Bạn chắc chắn muốn khóa không?';
    } else {
      title = 'Bạn chắc chắn muốn mở khóa không?';
    }
    this.dialogSv.confirm('', title, 'Có', 'Không').subscribe(next => {
      if (next) {
        if (slideshowModele.status === 1) {
          slideshowModele.status = 0;
        } else {
          slideshowModele.status = 1;
        }

        this.slideshowservice.createOrUpdate(slideshowModele).subscribe((data: any) => {
          if (data.code === 200) {
            this.showSuccess('Cập nhật thành công');
            this.slideshowservice.setResetForm(true);
          }
        }, (err) => {
          this.showError('Có lỗi không xác định xảy ra');
        });
      }
    });
  }

  delete(slideId) {
    console.log(slideId);
    this.dialogSv.confirm('', 'Bạn có chắc muốn xóa không?', 'Có', 'Không').subscribe(next => {
      if (next) {
        this.slideshowservice.delete(slideId).subscribe((data: any) => {
          if (data.code === 200) {
            this.showSuccess('Xóa thành công');
            this.slideshowservice.setResetForm(true);
          }
        }, (err) => {
          this.showError('Có lỗi không xác định xảy ra');
        });
      }
    });
  }

  showSuccess(message: string) {
    this.toastr.success(message);
  }

  showError(message: string) {
    this.toastr.error(message);
  }
}
