import { DialogsService } from '../../../../common/confirm-dialog/dialogs.service';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../../../../environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Component, OnInit, Output, EventEmitter, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { SlideShowModel } from '../../../../Model/slideshow.model';
import { SlideShowService } from '../../../../services/slideshow.service';
import { SelectAlbumComponent } from '../../../../common/select-album/select-album.component';

@Component({
  selector: 'app-slideshow-create',
  templateUrl: './slideshow-create.component.html',
  styleUrls: ['./slideshow-create.component.css']
})
export class SliderShowCreateComponent implements OnInit {

  createForm: FormGroup;
  slideshow: SlideShowModel;
  uploadEmpty: boolean;
  imgPath: any;
  fileList: FileList;
  checkUploadSucces: boolean;
  messageErrorFile = '';
  select;

  constructor(
    private fb: FormBuilder,
    private http: HttpClient,
    private slideShowService: SlideShowService,
    private toastr: ToastrService,
    private dialog: MatDialog,
    private dialogSv: DialogsService,
    public dialogRef: MatDialogRef<SliderShowCreateComponent>,
  ) { }

  ngOnInit(): void {
    this.slideshow = new SlideShowModel();
    this.imgPath = null;
    this.checkUploadSucces = false;
    this.uploadEmpty = true;
    this.createForm = this.fb.group(
      {
        slideDes: ['', Validators.required],
        slideTitle: ['', Validators.required],
        type: ['', Validators.required],
        select: ['']
      }
    );
  }

  selectFile(fileInputEvent: any) {
    this.messageErrorFile = '';
    this.slideshow.path = fileInputEvent.target.files[0].name;
    let checkFile;
    const formatFile = this.slideshow.path.substring(this.slideshow.path.length - 3, this.slideshow.path.length);
    if (formatFile !== 'jpg' && formatFile !== 'png') {
      checkFile = true;
      this.uploadEmpty = false;
      this.messageErrorFile = 'Ảnh không đúng định dạng';
    } else {
      this.messageErrorFile = '';
      this.uploadEmpty = true;
    }

    if (checkFile) {
      return;
    }

    if (this.slideshow.path.indexOf('.jpg') != -1 || this.slideshow.path.indexOf('.png') != -1) {
      const reader = new FileReader();
      reader.readAsDataURL(fileInputEvent.target.files[0]);
      reader.onload = (_event) => {
        this.imgPath = reader.result;
        this.uploadFile(fileInputEvent.target.files[0]);
      };
    } else {
      this.imgPath = null;
    }
  }

  hasError(controlName: string, errorName: string) {
    return this.createForm.controls[controlName].hasError(errorName);
  }

  saveSlideShow() {
    this.checkUpload();
    this.getValueFromForm();
    if(!this.select){
      return;
    }
    if (this.checkUploadSucces) {
      this.dialogSv.confirm('', 'Bạn có chắc muốn lưu không?', 'Có', 'Không').subscribe(next => {
        if (next) {
          this.slideShowService.createOrUpdate(this.slideshow).subscribe((data: any) => {
            if (data.code === 200) {
              this.showSuccess('Thêm mới thành công');
              this.dialogRef.close();
              this.slideShowService.setResetForm(true);
            }
          }, (err) => {
            this.showError('Có lỗi không xác định xảy ra');
          });
        }
      });
    }
  }

  checkUpload(): boolean {
    this.messageErrorFile = '';
    if (!this.slideshow.path || this.slideshow.path.length === 0) {
      this.messageErrorFile = 'Ảnh không được để trống';
      return this.uploadEmpty = false;
    } else {
      return this.uploadEmpty = true;
    }
  }

  getValueFromForm() {
    this.slideshow.description = this.createForm.controls.slideDes.value.trim();
    this.slideshow.title = this.createForm.controls.slideTitle.value;
    this.slideshow.path = this.createForm.controls.type.value + '/' + this.select.id;
    this.slideshow.status = 1;
  }

  uploadFile(fileUpload: any) {
    const file: File = fileUpload;
    const formData: FormData = new FormData();
    formData.append('file', file);
    const headers = new HttpHeaders();
    headers.append('Content-type', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    const options = { headers: headers };
    this.http.post(environment.API_URL + '/file/uploadReport/slide', formData, options)
      .subscribe(
        (data: any) => {
          if (data.code === 200) {
            this.slideshow.pathImg = data.data;
            this.checkUploadSucces = true;
          } else {
            this.checkUploadSucces = false;
          }
        },
        err => (this.showError('Có lỗi không xác định xảy ra')),
      );
  }

  showSuccess(message: string) {
    this.toastr.success(message);
  }

  showError(message: string) {
    this.toastr.error(message);
  }

  close() {
    this.dialogRef.close();
  }

  openType() {
    if (!this.createForm.controls.type.value || this.createForm.controls.type.value.length == 0) {
      return;
    }
    const dialogRef = this.dialog.open(SelectAlbumComponent, {
      disableClose: true,
      width: '60vw',
      panelClass: 'myapp-background-dialog',
      data: { type: this.createForm.controls.type.value }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.event !== 'cancel') {
        console.log(result.data);
        this.select = result.data.item;
        this.createForm.controls.select.setValue(this.select.name);
      }
    });
  }
}
