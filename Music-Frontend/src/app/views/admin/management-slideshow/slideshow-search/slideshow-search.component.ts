import { GenreModel } from '../../../../Model/genre.model';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { SlideShowModel } from '../../../../Model/slideshow.model';

@Component({
  selector: 'app-slideshow-search',
  templateUrl: './slideshow-search.component.html',
  styleUrls: ['./slideshow-search.component.scss']
})
export class SlideShowSearchComponent implements OnInit {

  @Output() search = new EventEmitter<string>();

  searchFormGroup: FormGroup;
  listSlideShow: SlideShowModel[];
  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.searchFormGroup = this.fb.group(
      {
        slideShowName: [''],
      }
    );
  }

  get slideShowName() {
    return this.searchFormGroup.get('slideShowName');
  }

  searchByCondition() {
    this.search.emit(this.slideShowName.value);
  }

}
