import { ManagementUploadComponent } from './management-upload/management-upload.component';
import { ManagementGenreComponent } from './management-genre/management-genre.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminRoutingModule } from './admin.routing';
import { MatInputModule, MatButtonModule, MatFormFieldModule, MatTableModule, MatSelectModule, MatOptionModule, MatDatepickerModule, DateAdapter, MatNativeDateModule, MatCheckboxModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GenreListComponent } from './management-genre/genre-list/genre-list.component';
import { GenreSearchComponent } from './management-genre/genre-search/genre-search.component';
import { GenreCreateComponent } from './management-genre/genre-create/genre-create.component';
import { GenreEditComponent } from './management-genre/genre-edit/genre-edit.component';
import { ManagementUserCustomerComponent } from './management-user/management-user-customer.component';
import { UserEditComponent } from './management-user/user-edit/user-edit.component';
import { UserSearchComponent } from './management-user/user-search/user-search.component';
import { UserCreateComponent } from './management-user/user-create/user-create.component';
import { UserListComponent } from './management-user/user-list/user-list.component';
import { ManagementSlideShowComponent } from './management-slideshow/management-slideshow.component';
import { SlideShowEditComponent } from './management-slideshow/slideshow-edit/slideshow-edit.component';
import { SlideShowSearchComponent } from './management-slideshow/slideshow-search/slideshow-search.component';
import { SlideShowListComponent } from './management-slideshow/slideshow-list/slideshow-list.component';
import { SliderShowCreateComponent } from './management-slideshow/slideshow-create/slideshow-create.component';
import { ManagementTopicComponent } from './management-topic/management-topic.component';
import { TopicListComponent } from './management-topic/topic-list/topic-list.component';
import { TopicSearchComponent } from './management-topic/topic-search/topic-search.component';
import { TopicCreateComponent } from './management-topic/topic-create/topic-create.component';
import { TopicEditComponent } from './management-topic/topic-edit/topic-edit.component';
import { ManagementAlbumComponent } from './management-album/management-album.component';
import { AlbumListComponent } from './management-album/album-list/album-list.component';
import { AlbumCreateComponent } from './management-album/album-create/album-create.component';
import { AlbumEditComponent } from './management-album/album-edit/album-edit.component';
import { CommonComponentModule } from '../../common/common-component.module';
import { AlbumSearchComponent } from './management-album/album-search/album-search.component';
import { ManagementArtisComponent } from './management-artis/management-artis.component';
import { ArtistListComponent } from './management-artis/artist-list/artist-list.component';
import { ArtistCreateComponent } from './management-artis/artist-create/artist-create.component';
import { ArtistEditComponent } from './management-artis/artist-edit/artist-edit.component';
import { ArtistSearchComponent } from './management-artis/artist-search/artist-search.component';
import { ManagementSongComponent } from './management-song/management-song.component';
import { SongListComponent } from './management-song/song-list/song-list.component';
import { SongCreateComponent } from './management-song/song-create/song-create.component';
import { SongSearchComponent } from './management-song/song-search/song-search.component';
import { NgxAudioPlayerModule } from 'ngx-audio-player';
import { SongEditComponent } from './management-song/song-edit/song-edit.component';
import { DetailUploadComponent } from './management-upload/detail-upload/detail-upload.component';
import { TooltipModule } from 'ngx-bootstrap/tooltip';

@NgModule({
  declarations: [
    ManagementGenreComponent,
    GenreListComponent,
    GenreSearchComponent,
    GenreCreateComponent,
    GenreEditComponent,
    ManagementUserCustomerComponent,
    UserCreateComponent,
    UserEditComponent,
    UserSearchComponent,
    UserListComponent,
    ManagementSlideShowComponent,
    SlideShowEditComponent,
    SlideShowSearchComponent,
    SlideShowListComponent,
    SliderShowCreateComponent,
    ManagementTopicComponent,
    TopicListComponent,
    TopicSearchComponent,
    TopicCreateComponent,
    TopicEditComponent,
    ManagementAlbumComponent,
    AlbumListComponent,
    AlbumCreateComponent,
    AlbumEditComponent,
    AlbumSearchComponent,
    ManagementArtisComponent,
    ArtistListComponent,
    ArtistCreateComponent,
    ArtistEditComponent,
    ArtistSearchComponent,
    ManagementSongComponent,
    SongListComponent,
    SongCreateComponent,
    SongSearchComponent,
    SongEditComponent,
    ManagementUploadComponent,
    DetailUploadComponent
  ],
  imports: [
    CommonModule,
    MatInputModule,
    MatButtonModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
    NgxPaginationModule,
    MatSelectModule,
    MatOptionModule,
    AdminRoutingModule,
    MatDatepickerModule,
    MatNativeDateModule,
    CommonComponentModule,
    NgxAudioPlayerModule,
    MatCheckboxModule,
    TooltipModule.forRoot()
  ],
  exports: [
    ArtistSearchComponent, ArtistListComponent
  ],
  providers: [],
  entryComponents: [GenreCreateComponent, GenreEditComponent, UserCreateComponent, UserEditComponent,
    SliderShowCreateComponent, SlideShowEditComponent, TopicCreateComponent, TopicEditComponent,
    AlbumCreateComponent, AlbumEditComponent, ArtistCreateComponent, ArtistEditComponent,
    SongCreateComponent, SongEditComponent, DetailUploadComponent],
})
export class AdminModule { }
