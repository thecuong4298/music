import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { Song } from '../../../../share/model/song.model';
import { MatDialog } from '@angular/material';
import { SongService } from '../../../../services/song.service';
import { DomSanitizer } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
import { SongCreateComponent } from '../song-create/song-create.component';
import { SongEditComponent } from '../song-edit/song-edit.component';

@Component({
  selector: 'app-song-list',
  templateUrl: './song-list.component.html',
  styleUrls: ['./song-list.component.css']
})
export class SongListComponent implements OnInit {
  p: any;
  @Input() listSong: Song[];
  @Output() lockOrUnlock = new EventEmitter<any>();
  @Output() delete = new EventEmitter<any>();
  songEdit: Song;

  constructor(
    private dialog: MatDialog,
    private songService: SongService,
    private domSanitizer: DomSanitizer,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    console.log(this.listSong);
  }

  openCreate() {
    this.songService.setResetForm(false);
    const dialogRef = this.dialog.open(SongCreateComponent, {
      width: '70vw',
      panelClass: 'myapp-background-dialog'
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  openEdit(songId) {
    this.songService.setResetForm(false);
    this.getSongById(songId);
  }


  lockUnlock(song) {
    this.lockOrUnlock.emit(song);
  }

  deleteSong(songId) {
    this.delete.emit(songId);
  }

  showSuccess(message: string) {
    this.toastr.success(message);
  }

  showError(message: string) {
    this.toastr.error(message);
  }

  getSongById(songid) {
    this.songService.getSongById(songid).subscribe((data: any) => {
      if (data.code === 200) {
        if (data.data) {
          const dialogRef = this.dialog.open(SongEditComponent, {
            width: '70vw',
            panelClass: 'myapp-background-dialog',
            data: { song: data.data }
          });

          dialogRef.afterClosed().subscribe(result => {
          });
        }
      } else {
        this.showError(data.mstrErrors);
      }
    }, () => {
      this.showError('Có lỗi không xác định xảy ra');
    });
  }
}
