import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Song } from '../../../../share/model/song.model';

@Component({
  selector: 'app-song-search',
  templateUrl: './song-search.component.html',
  styleUrls: ['./song-search.component.css']
})
export class SongSearchComponent implements OnInit {
  @Output() search = new EventEmitter<string>();

  searchFormGroup: FormGroup;
  listSong: Song[];

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.searchFormGroup = this.fb.group(
      {
        songName: [''],
      }
    );
  }

  get songName() {
    return this.searchFormGroup.get('songName');
  }

  searchByCondition() {
    this.search.emit(this.songName.value);
  }

}
