import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AlbumModel } from '../../../../Model/album.model';
import { Song } from '../../../../share/model/song.model';
import { MatDialogRef, MAT_DATE_LOCALE, MAT_DATE_FORMATS, DateAdapter, MatDatepickerModule, MatDialog } from '@angular/material';
import { DialogsService } from '../../../../common/confirm-dialog/dialogs.service';
import { ToastrService } from 'ngx-toastr';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SongService } from '../../../../services/song.service';
import { environment } from '../../../../../environments/environment';
import { CountryModel } from '../../../../Model/country.model';
import { CountryService } from '../../../../services/country.service';
import { GenreModel } from '../../../../Model/genre.model';
import { GenreService } from '../../../../services/genre.service';
import { AlbumService } from '../../../../services/album.service';
import { MY_FORMATS } from '../../../Customer/login-music/register-music/register-music.component';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { SearchArtistUploadComponent } from '../../../Customer/upload/search-artist-upload/search-artist-upload.component';

@Component({
  selector: 'app-song-create',
  templateUrl: './song-create.component.html',
  styleUrls: ['./song-create.component.scss'],
  providers: [
    MatDatepickerModule,
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS }
  ],
  encapsulation: ViewEncapsulation.None
})
export class SongCreateComponent implements OnInit {
  createForm: FormGroup;
  song: Song;
  uploadEmpty: boolean;
  imgPath: any;
  soundPath: any;
  fileList: FileList;
  checkUploadSucces: boolean;
  messageErrorFile = '';
  messageErrorImg = '';
  countries: CountryModel[];
  genreses: GenreModel[];
  albums: AlbumModel[];
  singerId = [];
  checkSinger;

  constructor(
    private fb: FormBuilder,
    private http: HttpClient,
    private songService: SongService,
    private toastr: ToastrService,
    private dialogSv: DialogsService,
    private countryService: CountryService,
    private genreService: GenreService,
    private albumService: AlbumService,
    private dialog: MatDialog,
    public dialogRef: MatDialogRef<SongCreateComponent>,
  ) { }

  ngOnInit(): void {
    this.song = new Song();
    this.imgPath = null;
    this.soundPath = null;
    this.checkUploadSucces = false;
    this.uploadEmpty = true;
    this.getCountries();
    this.getGenreses();
    this.getAlbums();
    this.createForm = this.fb.group(
      {
        songName: ['', Validators.required],
        songDes: ['', Validators.required],
        songReleaseDate: ['', Validators.required],
        songCountryId: ['', Validators.required],
        songGenreId: ['', Validators.required],
        songAlbumId: [''],
        lyric: [''],
        singer: ['']
      }
    );
  }

  getCountries() {
    this.countryService.getCountryActive().subscribe((res: any) => {
      this.countries = res.data;
    });
  }

  getGenreses() {
    this.genreService.getGenres().subscribe((res: any) => {
      this.genreses = res.data;
    });
  }

  getAlbums() {
    this.albumService.getAlbumActive().subscribe((res: any) => {
      this.albums = res.data;
    });
  }

  selectFileImage(fileImageInputEvent: any) {
    this.messageErrorFile = '';
    this.song.pathAvatar = 'song/' + fileImageInputEvent.target.files[0].name;
    let checkFile;
    const formatFile = this.song.pathAvatar.substring(this.song.pathAvatar.length - 3, this.song.pathAvatar.length);
    if (formatFile !== 'jpg' && formatFile !== 'png') {
      checkFile = true;
      this.uploadEmpty = false;
      this.messageErrorFile = 'Ảnh không đúng định dạng';
    } else {
      this.messageErrorFile = '';
      this.uploadEmpty = true;
    }

    if (checkFile) {
      return;
    }

    if (this.song.pathAvatar.indexOf('.jpg') != -1 || this.song.pathAvatar.indexOf('.png') != -1) {
      const reader = new FileReader();
      reader.readAsDataURL(fileImageInputEvent.target.files[0]);
      reader.onload = (_event) => {
        this.imgPath = reader.result;
        this.uploadFileImage(fileImageInputEvent.target.files[0]);
      };
    } else {
      this.imgPath = null;
    }
  }

  selectFileSound(fileSoundInputEvent: any) {
    this.messageErrorFile = '';
    this.song.path = fileSoundInputEvent.target.files[0].name;
    let checkFile;
    const formatFile = this.song.path.substring(this.song.path.length - 3, this.song.path.length);
    if (formatFile !== 'mp3' && formatFile !== 'mp4') {
      checkFile = true;
      this.uploadEmpty = false;
      this.messageErrorFile = 'File không đúng định dạng';
    } else {
      this.messageErrorFile = '';
      this.uploadEmpty = true;
    }

    if (checkFile) {
      return;
    }

    if (this.song.path.indexOf('.mp3') != -1 || this.song.path.indexOf('.mp4') != -1) {
      const reader = new FileReader();
      reader.readAsDataURL(fileSoundInputEvent.target.files[0]);
      reader.onload = (_event) => {
        this.soundPath = reader.result;
        this.uploadFile(fileSoundInputEvent.target.files[0]);
      };
    } else {
      this.soundPath = null;
    }
  }

  hasError(controlName: string, errorName: string) {
    return this.createForm.controls[controlName].hasError(errorName);
  }

  saveSong() {
    if (!this.singerId) {
      this.checkSinger = 'Bạn phải chọn ca sĩ';
      return;
    }
    if (!this.checkUpload()) {
      return;
    }
    this.getValueFromForm();
    if (this.check()) {
      this.dialogSv.confirm('', 'Bạn có chắc muốn lưu không?', 'Có', 'Không').subscribe(next => {
        if (next) {
          this.songService.createOrUpdate(this.song, this.singerId).subscribe((data: any) => {
            if (data.code === 200) {
              this.showSuccess('Thêm mới thành công');
              this.dialogRef.close();
              this.songService.setResetForm(true);
            }
          }, (err) => {
            this.showError('Có lỗi không xác định xảy ra');
          });
        }
      });
    }
  }

  check() {
    if (this.checkUploadSucces && this.createForm.valid && this.messageErrorFile.length == 0 && this.messageErrorImg.length == 0) {
      return true;
    }
    return false;
  }

  checkUpload(): boolean {
    // this.messageErrorFile = '';
    if (!this.song.path || this.song.path.length === 0) {
      this.messageErrorFile = 'Bài hát không được để trống';
      return false;
    }
    if (!this.song.pathAvatar || this.song.pathAvatar.length === 0) {
      this.messageErrorImg = 'Ảnh không được để trống';
      return false;
    }
    return true;
  }

  getValueFromForm() {
    this.song.name = this.createForm.controls.songName.value.trim();
    this.song.description = this.createForm.controls.songDes.value;
    this.song.releaseDate = this.createForm.controls.songReleaseDate.value;
    this.song.countryId = this.createForm.controls.songCountryId.value;
    this.song.genreId = this.createForm.controls.songGenreId.value;
    this.song.albumId = this.createForm.controls.songAlbumId.value;
    this.song.status = 1;
    this.song.lyric = this.createForm.controls.lyric.value;
  }

  uploadFile(fileUpload: any) {
    const file: File = fileUpload;
    const formData: FormData = new FormData();
    formData.append('file', file);
    const headers = new HttpHeaders();
    headers.append('Content-type', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    const options = { headers: headers };
    this.http.post(environment.API_URL + '/file/uploadSong', formData, options)
      .subscribe(
        (data: any) => {
          if (data.code === 200) {
            this.soundPath = data.data[1];
            this.song.path = data.data[0];
            this.checkUploadSucces = true;
            this.messageErrorFile = '';
          } else {
            this.checkUploadSucces = false;
          }
        },
        err => (this.showError('Có lỗi không xác định xảy ra')),
      );
  }

  showSuccess(message: string) {
    this.toastr.success(message);
  }

  showError(message: string) {
    this.toastr.error(message);
  }

  close() {
    this.dialogRef.close();
  }

  uploadFileImage(fileUpload: any) {
    const file: File = fileUpload;
    const formData: FormData = new FormData();
    formData.append('file', file);
    const headers = new HttpHeaders();
    headers.append('Content-type', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    const options = { headers: headers };
    this.http.post(environment.API_URL + '/file/uploadReport/song', formData, options)
      .subscribe(
        (data: any) => {
          if (data.code === 200) {
            this.song.pathAvatar = data.data;
            this.messageErrorImg = '';
            this.checkUploadSucces = true;
          } else {
            this.checkUploadSucces = false;
          }
        },
        err => (this.showError('Có lỗi không xác định xảy ra')),
      );
  }
  openSinger() {
    const dialogRef = this.dialog.open(SearchArtistUploadComponent, {
      disableClose: true,
      width: '60vw',
      panelClass: 'myapp-background-dialog',
      data: {singer: this.singerId}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.event !== 'cancel') {
        this.singerId = result.data.artistid;
        this.createForm.controls.singer.setValue(result.data.singer
        );
        this.checkSinger = '';
      }
    });
  }
}
