import { Component, OnInit } from '@angular/core';
import { SongService } from '../../../services/song.service';
import { ToastrService } from 'ngx-toastr';
import { DomSanitizer } from '@angular/platform-browser';
import { DialogsService } from '../../../common/confirm-dialog/dialogs.service';
import { Song } from '../../../share/model/song.model';

@Component({
  selector: 'app-management-song',
  templateUrl: './management-song.component.html',
  styleUrls: ['./management-song.component.css']
})
export class ManagementSongComponent implements OnInit {

  constructor(
    private songService: SongService,
    private toastr: ToastrService,
    private domSanitizer: DomSanitizer,
    private dialogSv: DialogsService
  ) { }

  listSong: Song[];

  ngOnInit(): void {
    this.getSongs();
    this.songService.resetForm$.subscribe(reset => {
      if (reset) {
        this.getSongs();
      }
    });
  }

  getSongs() {
    this.songService.getAllSong().subscribe((song: any) => {
      if (song.code === 200) {
        this.listSong = song.data;
        console.log(this.listSong);
      } else {
        this.listSong = [];
        console.log(song);
      }
    }, err => {
      this.showError('Có lỗi không xác định xảy ra');
    });
  }

  search(songName) {
    this.songService.findByCondition(songName).subscribe((song: any) => {
      if (song.code === 200) {
        this.listSong = song.data;
      } else {
        this.listSong = [];
      }
    }, err => {
      this.showError('Có lỗi không xác định xảy ra');
    });
  }

  lockOrUnlock(song: Song) {
    let title = '';
    if (song.status === 1) {
      title = 'Bạn chắc chắn muốn khóa không?';
    } else {
      title = 'Bạn chắc chắn muốn mở khóa không?';
    }
    this.dialogSv.confirm('', title, 'Có', 'Không').subscribe(next => {
      if (next) {
        if (song.status === 1) {
          song.status = 0;
        } else {
          song.status = 1;
        }

        this.songService.createOrUpdate(song, null).subscribe((data: any) => {
          if (data.code === 200) {
            this.showSuccess('Cập nhật thành công');
            this.songService.setResetForm(true);
          }
        }, (err) => {
          this.showError('Có lỗi không xác định xảy ra');
        });
      }
    });
  }

  delete(songId) {
    console.log(songId);
    this.dialogSv.confirm('', 'Bạn có chắc muốn xóa không?', 'Có', 'Không').subscribe(next => {
      if (next) {
        this.songService.delete(songId).subscribe((data: any) => {
          if (data.code === 200) {
            this.showSuccess('Xóa thành công');
            this.songService.setResetForm(true);
          }
        }, (err) => {
          this.showError('Có lỗi không xác định xảy ra');
        });
      }
    });
  }

  showSuccess(message: string) {
    this.toastr.success(message);
  }

  showError(message: string) {
    this.toastr.error(message);
  }

}
