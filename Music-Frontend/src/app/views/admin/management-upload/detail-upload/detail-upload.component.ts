import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SongUpload } from './../../../../model/song-upload.model';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-detail-upload',
  templateUrl: './detail-upload.component.html',
  styleUrls: ['./detail-upload.component.scss']
})
export class DetailUploadComponent implements OnInit {

  songUpload: SongUpload;
  constructor(
    public dialogRef: MatDialogRef<DetailUploadComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    this.songUpload = data.songUpload;
    console.log(this.songUpload);
  }

  ngOnInit(): void {
  }

}
