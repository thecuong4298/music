import { SongUpload } from './../../../model/song-upload.model';
import { DetailUploadComponent } from './detail-upload/detail-upload.component';
import { DialogsService } from './../../../common/confirm-dialog/dialogs.service';
import { ToastrService } from 'ngx-toastr';
import { SongService } from './../../../services/song.service';
import { Song } from './../../../share/model/song.model';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-management-upload',
  templateUrl: './management-upload.component.html',
  styleUrls: ['./management-upload.component.scss']
})
export class ManagementUploadComponent implements OnInit {

  songs: Song[] = [];
  p;
  songUpload: SongUpload;
  constructor(
    private songService: SongService,
    private toastr: ToastrService,
    private dialogSv: DialogsService,
    private dialog: MatDialog,
  ) { }

  ngOnInit(): void {
    this.getAllSong();
  }

  getAllSong() {
    this.songService.getAllSongUploadByCustomer().subscribe((result: any) => {
      if (result.code === 200) {
        this.songs = result.data;
        console.log(result.data);
        
      } else {
        this.songs = [];
      }
    }, () => this.showError('Có lỗi không xác định xảy ra'));
  }

  accept(songId: number) {
    this.dialogSv.confirm('', 'Bạn có chắc muốn chấp nhận bài hát không?', 'Có', 'Không').subscribe(next => {
      if (next) {
        this.acceptOrRejectSongUpload(songId, 1);
      }
    });
  }

  reject(songId: number) {
    this.dialogSv.confirm('', 'Bạn có chắc muốn từ chối bài hát không?', 'Có', 'Không').subscribe(next => {
      if (next) {
        this.acceptOrRejectSongUpload(songId, 0);
      }
    });
  }

  acceptOrRejectSongUpload(songId: number, type: number) {
    this.songService.acceptOrRejectSongUpload(songId, type).subscribe((res: any) => {
      if (res.code === 200) {
        this.showSuccess('Cập nhật thành công');
        this.getAllSong();
      }
    }, () => this.showError('Có lỗi không xác định xảy ra'));
  }

  detail(songId: number) {
    this.songService.getDetailSongUpload(songId).subscribe((result: any) => {
      if (result.code === 200) {
        this.songUpload = result.data;
        const dialogRef = this.dialog.open(DetailUploadComponent, {
          width: '70vw',
          panelClass: 'myapp-background-dialog',
          data: { songUpload: this.songUpload }
        });
      } else {
        this.showError('Không tìm thấy bài hát')
      }
    }, () => this.showError('Có lỗi không xác định xảy ra'));
  }

  showSuccess(message: string) {
    this.toastr.success(message);
  }

  showError(message: string) {
    this.toastr.error(message);
  }

}
