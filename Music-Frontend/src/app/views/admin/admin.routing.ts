import { ManagementUploadComponent } from './management-upload/management-upload.component';
import { ManagementGenreComponent } from './management-genre/management-genre.component';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ManagementUserCustomerComponent } from './management-user/management-user-customer.component';
import { ManagementSlideShowComponent } from './management-slideshow/management-slideshow.component';
import { ManagementTopicComponent } from './management-topic/management-topic.component';
import { ManagementAlbumComponent } from './management-album/management-album.component';
import { ManagementArtisComponent } from './management-artis/management-artis.component';
import { ManagementSongComponent } from './management-song/management-song.component';

const routes: Routes = [
    {
        path: '',
        component: ManagementUserCustomerComponent,
        data: {
            title: 'Quản lý người dùng'
        }
    },
    {
        path: 'management-genre',
        component: ManagementGenreComponent,
        data: {
            title: 'Quản lý thể loại'
        }
    },
    {
        path: 'management-user',
        component: ManagementUserCustomerComponent,
        data: {
            title: 'Quản lý người dùng'
        }
    },
    {
        path: 'management-slideshow',
        component: ManagementSlideShowComponent,
        data: {
            title: 'Quản lý tin tức'
        }
    },
    {
        path: 'management-topic',
        component: ManagementTopicComponent,
        data: {
            title: 'Quản lý chủ đề'
        }
    },
    {
        path: 'management-album',
        component: ManagementAlbumComponent,
        data: {
            title: 'Quản lý Album'
        }
    },
    {
        path: 'management-artist',
        component: ManagementArtisComponent,
        data: {
            title: 'Quản lý Ca si'
        }
    },
    {
        path: 'management-song',
        component: ManagementSongComponent,
        data: {
            title: 'Quản lý bai hat'
        }
    },
    {
        path: 'management-upload',
        component: ManagementUploadComponent,
        data: {
            title: 'Quản lý upload'
        }
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdminRoutingModule { }
