import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AlbumModel } from '../../../../model/album.model';
import { StatusModel } from '../../../../model/status.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AlbumService } from '../../../../services/album.service';
import { ToastrService } from 'ngx-toastr';
import { DialogsService } from '../../../../common/confirm-dialog/dialogs.service';
import { MatDialogRef, MAT_DIALOG_DATA, MatDatepickerModule, DateAdapter, MAT_DATE_LOCALE, MAT_DATE_FORMATS } from '@angular/material';
import { AlbumCreateComponent } from '../album-create/album-create.component';
import { environment } from '../../../../../environments/environment';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { MY_FORMATS } from '../../../Customer/login-music/register-music/register-music.component';

@Component({
  selector: 'app-album-edit',
  templateUrl: './album-edit.component.html',
  styleUrls: ['./album-edit.component.css'],
  providers: [
    MatDatepickerModule,
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS }
  ]
})
export class AlbumEditComponent implements OnInit {
  createForm: FormGroup;
  album: AlbumModel;
  uploadEmpty: boolean;
  imgPath: any;
  fileList: FileList;
  checkUploadSucces: boolean;
  albumId: number;
  messageErrorFile = '';

  statuses: StatusModel[] = [
    { value: 1, viewValue: 'Hoạt động' },
    { value: 0, viewValue: 'Không hoạt động' }
  ];

  constructor(
    private fb: FormBuilder,
    private http: HttpClient,
    private albumService: AlbumService,
    private toastr: ToastrService,
    private dialogSv: DialogsService,
    public dialogRef: MatDialogRef<AlbumCreateComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    this.album = data.albumEdit;
  }

  ngOnInit(): void {
    console.log(this.album);

    this.imgPath = null;
    this.checkUploadSucces = false;
    this.uploadEmpty = true;
    this.createForm = this.fb.group(
      {
        albumName: [this.album.name, Validators.required],
        albumDes: [this.album.description, Validators.required],
        albumReleaseDate: [this.album.releaseDate, Validators.required],
        albumStatus: [this.album.status, Validators.required]
      }
    );
  }

  selectFile(fileInputEvent: any) {
    this.album.path = fileInputEvent.target.files[0].name;
    const formatFile = this.album.path.substring(this.album.path.length - 3, this.album.path.length);
    let checkFile;
    if (formatFile !== 'jpg' && formatFile !== 'png') {
      checkFile = true;
      this.uploadEmpty = false;
      this.messageErrorFile = 'Ảnh không đúng định dạng';
    } else {
      this.messageErrorFile = '';
      this.uploadEmpty = true;
    }

    if (checkFile) {
      return;
    }

    if (this.album.path.indexOf('.jpg') != -1 || this.album.path.indexOf('.png') != -1) {
      const reader = new FileReader();
      reader.readAsDataURL(fileInputEvent.target.files[0]);
      reader.onload = (_event) => {
        this.imgPath = reader.result;
        this.uploadFile(fileInputEvent.target.files[0]);
      };
    } else {
      this.imgPath = null;
    }
  }

  hasError(controlName: string, errorName: string) {
    return this.createForm.controls[controlName].hasError(errorName);
  }

  saveAlbum() {
    this.checkUpload();
    this.getValueFromForm();
    this.dialogSv.confirm('', 'Bạn có chắc muốn lưu không?', 'Có', 'Không').subscribe(next => {
      if (next) {
        this.albumService.createOrUpdate(this.album).subscribe((data: any) => {
          if (data.code === 200) {
            this.showSuccess('Cập nhật thành công');
            this.dialogRef.close();
            this.albumService.setResetForm(true);
          }
        }, err => {
          this.showError('Có lỗi không xác định xảy ra');
        });
      }
    });

  }

  checkUpload(): boolean {
    this.messageErrorFile = '';
    if (!this.album.path || this.album.path.length === 0) {
      this.messageErrorFile = 'Ảnh không được để trống';
      return this.uploadEmpty = false;
    } else {
      return this.uploadEmpty = true;
    }
  }

  getValueFromForm() {
    this.album.name = this.createForm.controls.albumName.value.trim();
    this.album.description = this.createForm.controls.albumDes.value;
    this.album.releaseDate = this.createForm.controls.albumReleaseDate.value;
    this.album.status = this.createForm.controls.albumStatus.value;
  }

  uploadFile(fileUpload: any) {
    const file: File = fileUpload;
    const formData: FormData = new FormData();
    formData.append('file', file);
    const headers = new HttpHeaders();
    headers.append('Content-type', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    const options = { headers: headers };
    this.http.post(environment.API_URL + '/file/uploadReport/album', formData, options)
      .subscribe(
        (data: any) => {
          if (data.code === 200) {
            this.album.path = data.data;
            this.checkUploadSucces = true;
          } else {
            this.checkUploadSucces = false;
          }
        },
        () => (this.showError('Có lỗi không xác định xảy ra'))
      );
  }

  showSuccess(message: string) {
    this.toastr.success(message);
  }

  showError(message: string) {
    this.toastr.error(message);
  }

  close() {
    this.dialogRef.close();
  }
}
