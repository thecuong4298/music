import { Component, OnInit } from '@angular/core';
import { AlbumService } from '../../../services/album.service';
import { ToastrService } from 'ngx-toastr';
import { DomSanitizer } from '@angular/platform-browser';
import { DialogsService } from '../../../common/confirm-dialog/dialogs.service';
import { AlbumModel } from '../../../model/album.model';

@Component({
  selector: 'app-management-album',
  templateUrl: './management-album.component.html',
  styleUrls: ['./management-album.component.css']
})
export class ManagementAlbumComponent implements OnInit {

  constructor(
    private albumService: AlbumService,
    private toastr: ToastrService,
    private domSanitizer: DomSanitizer,
    private dialogSv: DialogsService
  ) { }

  listAlbum: AlbumModel[];

  ngOnInit(): void {
    this.getAlbums();
    this.albumService.resetForm$.subscribe(reset => {
      if (reset) {
        this.getAlbums();
      }
    });
  }

  base64() {
    this.listAlbum.forEach(topic => {
      topic.url = this.domSanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' + topic.base64);
    });
  }

  getAlbums() {
    this.albumService.getAllAlbum().subscribe((topic: any) => {
      if (topic.code === 200) {
        this.listAlbum = topic.data;
        console.log(this.listAlbum);

        this.base64();
      } else {
        this.listAlbum = [];
      }
    }, err => {
      this.showError('Có lỗi không xác định xảy ra');
    });
  }

  delete(albumId) {
    console.log(albumId);
    this.dialogSv.confirm('', 'Bạn có chắc muốn xóa không?', 'Có', 'Không').subscribe(next => {
      if (next) {
        this.albumService.delete(albumId).subscribe((data: any) => {
          if (data.code === 200) {
            this.showSuccess('Xóa thành công');
            this.albumService.setResetForm(true);
          }
        }, (err) => {
          this.showError('Có lỗi không xác định xảy ra');
        });
      }
    });
  }

  search(albumName) {
    this.albumService.findByCondition(albumName).subscribe((album: any) => {
      if (album.code === 200) {
        this.listAlbum = album.data;
        this.base64();
      } else {
        this.listAlbum = [];
      }
    }, err => {
      this.showError('Có lỗi không xác định xảy ra');
    });
  }

  lockOrUnlock(albumModel: AlbumModel) {
    let title = '';
    if (albumModel.status === 1) {
      title = 'Bạn chắc chắn muốn khóa không?';
    } else {
      title = 'Bạn chắc chắn muốn mở khóa không?';
    }
    this.dialogSv.confirm('', title, 'Có', 'Không').subscribe(next => {
      if (next) {
        if (albumModel.status === 1) {
          albumModel.status = 0;
        } else {
          albumModel.status = 1;
        }

        this.albumService.createOrUpdate(albumModel).subscribe((data: any) => {
          if (data.code === 200) {
            this.showSuccess('Cập nhật thành công');
            this.albumService.setResetForm(true);
          }
        }, (err) => {
          this.showError('Có lỗi không xác định xảy ra');
        });
      }
    });
  }

  showSuccess(message: string) {
    this.toastr.success(message);
  }

  showError(message: string) {
    this.toastr.error(message);
  }

}
