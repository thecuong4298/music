import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AlbumModel } from '../../../../model/album.model';
import { MatDialog } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
import { AlbumService } from '../../../../services/album.service';
import { AlbumCreateComponent } from '../album-create/album-create.component';
import { AlbumEditComponent } from '../album-edit/album-edit.component';

@Component({
  selector: 'app-album-list',
  templateUrl: './album-list.component.html',
  styleUrls: ['./album-list.component.css']
})
export class AlbumListComponent implements OnInit {
  p: any;
  @Input() listAlbum: AlbumModel[];
  @Output() lockOrUnlock = new EventEmitter<any>();
  @Output() delete = new EventEmitter<any>();
  albumEdit: AlbumModel;

  constructor(
    private dialog: MatDialog,
    private albumService: AlbumService,
    private domSanitizer: DomSanitizer,
    private toastr: ToastrService

  ) { }

  ngOnInit(): void {
    console.log(this.listAlbum);
  }

  openCreate() {
    this.albumService.setResetForm(false);
    const dialogRef = this.dialog.open(AlbumCreateComponent, {
      width: '70vw',
      panelClass: 'myapp-background-dialog'
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  openEdit(albumId) {
    this.albumService.setResetForm(false);
    this.getAlbumById(albumId);
  }

  getAlbumById(albumId) {
    this.albumService.getAlbumById(albumId).subscribe((data: any) => {
      if (data.code === 200) {
        this.albumEdit = data.data;
        this.albumEdit.url = this.domSanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' + this.albumEdit.base64);
        if (this.albumEdit) {
          const dialogRef = this.dialog.open(AlbumEditComponent, {
            width: '70vw',
            panelClass: 'myapp-background-dialog',
            data: { albumEdit: this.albumEdit }
          });

          dialogRef.afterClosed().subscribe(result => {
          });
        }
      } else {
        this.showError(data.mstrErrors);
      }
    }, () => {
      this.showError('Có lỗi không xác định xảy ra');
    });

  }

  lockUnlock(album) {
    this.lockOrUnlock.emit(album);
  }

  deleteAlbum(albumId) {
    this.delete.emit(albumId);
  }

  showSuccess(message: string) {
    this.toastr.success(message);
  }

  showError(message: string) {
    this.toastr.error(message);
  }

}
