import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AlbumModel } from '../../../../model/album.model';

@Component({
  selector: 'app-album-search',
  templateUrl: './album-search.component.html',
  styleUrls: ['./album-search.component.css']
})
export class AlbumSearchComponent implements OnInit {
  @Output() search = new EventEmitter<string>();

  searchFormGroup: FormGroup;
  listAlbum: AlbumModel[];
  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.searchFormGroup = this.fb.group(
      {
        albumName: [''],
      }
    );
  }

  get albumName() {
    return this.searchFormGroup.get('albumName');
  }

  searchByCondition() {
    this.search.emit(this.albumName.value);
  }

}
