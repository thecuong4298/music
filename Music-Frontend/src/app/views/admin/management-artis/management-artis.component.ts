import { Component, OnInit } from '@angular/core';
import { ArtistService } from '../../../services/artist.service';
import { ToastrService } from 'ngx-toastr';
import { DomSanitizer } from '@angular/platform-browser';
import { DialogsService } from '../../../common/confirm-dialog/dialogs.service';
import { ArtistModel } from '../../../Model/artist.model';
import { CountryService } from '../../../services/country.service';
import { CountryModel } from '../../../Model/country.model';

@Component({
  selector: 'app-management-artis',
  templateUrl: './management-artis.component.html',
  styleUrls: ['./management-artis.component.css']
})
export class ManagementArtisComponent implements OnInit {

  countries: CountryModel[] = [];

  constructor(
    private artistService: ArtistService,
    private toastr: ToastrService,
    private domSanitizer: DomSanitizer,
    private dialogSv: DialogsService,
    private countryService: CountryService
  ) { }

  listArtist: ArtistModel[];

  ngOnInit(): void {
    this.getArtists();
    this.getCountries();
    this.artistService.resetForm$.subscribe(reset => {
      if (reset) {
        this.getArtists();
      }
    });
  }

  
  getCountries(){
    this.countryService.getCountryActive().subscribe((res: any) => {
      this.countries = res.data;
    });
  }

  getArtists() {
    this.artistService.getAllArtist().subscribe((artist: any) => {
      if (artist.code === 200) {
        this.listArtist = artist.data;
      } else {
        this.listArtist = [];
      }
    }, err => {
      this.showError('Có lỗi không xác định xảy ra');
    });
  }

  delete(artistId) {
    console.log(artistId);
    this.dialogSv.confirm('', 'Bạn có chắc muốn xóa không?', 'Có', 'Không').subscribe(next => {
      if (next) {
        this.artistService.delete(artistId).subscribe((data: any) => {
          if (data.code === 200) {
            this.showSuccess('Xóa thành công');
            this.artistService.setResetForm(true);
          }
        }, (err) => {
          this.showError('Có lỗi không xác định xảy ra');
        });
      }
    });
  }

  search(artistName) {
    this.artistService.findByCondition(artistName).subscribe((artist: any) => {
      if (artist.code === 200) {
        this.listArtist = artist.data;
      } else {
        this.listArtist = [];
      }
    }, err => {
      this.showError('Có lỗi không xác định xảy ra');
    });
  }

  lockOrUnlock(artistModel: ArtistModel) {
    let title = '';
    if (artistModel.status === 1) {
      title = 'Bạn chắc chắn muốn khóa không?';
    } else {
      title = 'Bạn chắc chắn muốn mở khóa không?';
    }
    this.dialogSv.confirm('', title, 'Có', 'Không').subscribe(next => {
      if (next) {
        if (artistModel.status === 1) {
          artistModel.status = 0;
        } else {
          artistModel.status = 1;
        }

        this.artistService.createOrUpdate(artistModel).subscribe((data: any) => {
          if (data.code === 200) {
            this.showSuccess('Cập nhật thành công');
            this.artistService.setResetForm(true);
          }
        }, (err) => {
          this.showError('Có lỗi không xác định xảy ra');
        });
      }
    });
  }

  showSuccess(message: string) {
    this.toastr.success(message);
  }

  showError(message: string) {
    this.toastr.error(message);
  }

}
