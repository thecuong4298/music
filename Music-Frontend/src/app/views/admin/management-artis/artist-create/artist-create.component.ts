import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ArtistModel } from '../../../../model/artist.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ArtistService } from '../../../../services/artist.service';
import { ToastrService } from 'ngx-toastr';
import { DialogsService } from '../../../../common/confirm-dialog/dialogs.service';
import { MatDialogRef } from '@angular/material';
import { environment } from '../../../../../environments/environment';
import { CountryModel } from '../../../../Model/country.model';
import { GrenderModel } from '../../../../Model/grender.model';
import { CountryService } from '../../../../services/country.service';

@Component({
  selector: 'app-artist-create',
  templateUrl: './artist-create.component.html',
  styleUrls: ['./artist-create.component.css']
})
export class ArtistCreateComponent implements OnInit {
  createForm: FormGroup;
  artist: ArtistModel;
  uploadEmpty: boolean;
  imgPath: any;
  fileList: FileList;
  checkUploadSucces: boolean;
  messageErrorFile = '';

  countries: CountryModel[];

  genderes: GrenderModel[] = [
    { value: 0, viewValue: 'Nữ' },
    { value: 1, viewValue: 'Nam' }
  ];

  constructor(
    private fb: FormBuilder,
    private http: HttpClient,
    private artistService: ArtistService,
    private toastr: ToastrService,
    private dialogSv: DialogsService,
    private countryService: CountryService,
    public dialogRef: MatDialogRef<ArtistCreateComponent>,
  ) { }

  ngOnInit(): void {
    this.artist = new ArtistModel();
    this.imgPath = null;
    this.checkUploadSucces = false;
    this.uploadEmpty = true;
    this.getCountries();
    this.createForm = this.fb.group(
      {
        artistName: ['', Validators.required],
        artistDes: ['', Validators.required],
        artistBirthDay: ['', Validators.required],
        artistCountryId: ['', Validators.required],
        artistGender: ['', Validators.required]
      }
    );
  }

  selectFile(fileInputEvent: any) {
    this.messageErrorFile = '';
    this.artist.path = fileInputEvent.target.files[0].name;
    let checkFile;
    const formatFile = this.artist.path.substring(this.artist.path.length - 3, this.artist.path.length);
    if (formatFile !== 'jpg' && formatFile !== 'png') {
      checkFile = true;
      this.uploadEmpty = false;
      this.messageErrorFile = 'Ảnh không đúng định dạng';
    } else {
      this.messageErrorFile = '';
      this.uploadEmpty = true;
    }

    if (checkFile) {
      return;
    }

    if (this.artist.path.indexOf('.jpg') != -1 || this.artist.path.indexOf('.png') != -1) {
      const reader = new FileReader();
      reader.readAsDataURL(fileInputEvent.target.files[0]);
      reader.onload = (_event) => {
        this.imgPath = reader.result;
        this.uploadFile(fileInputEvent.target.files[0]);
      };
    } else {
      this.imgPath = null;
    }
  }

  getCountries() {
    this.countryService.getCountryActive().subscribe((res: any) => {
      this.countries = res.data;
    });
  }

  hasError(controlName: string, errorName: string) {
    return this.createForm.controls[controlName].hasError(errorName);
  }

  saveArtist() {
    this.checkUpload();
    this.getValueFromForm();
    if (this.checkUploadSucces) {
      this.dialogSv.confirm('', 'Bạn có chắc muốn lưu không?', 'Có', 'Không').subscribe(next => {
        if (next) {
          this.artistService.createOrUpdate(this.artist).subscribe((data: any) => {
            if (data.code === 200) {
              this.showSuccess('Thêm mới thành công');
              this.dialogRef.close();
              this.artistService.setResetForm(true);
            }
          }, (err) => {
            this.showError('Có lỗi không xác định xảy ra');
          });
        }
      });
    }
  }

  checkUpload(): boolean {
    this.messageErrorFile = '';
    if (!this.artist.path || this.artist.path.length === 0) {
      this.messageErrorFile = 'Ảnh không được để trống';
      return this.uploadEmpty = false;
    } else {
      return this.uploadEmpty = true;
    }
  }

  getValueFromForm() {
    this.artist.name = this.createForm.controls.artistName.value.trim();
    this.artist.description = this.createForm.controls.artistDes.value;
    this.artist.birthday = this.createForm.controls.artistBirthDay.value;
    this.artist.countryId = this.createForm.controls.artistCountryId.value;
    this.artist.gender = this.createForm.controls.artistGender.value;
    this.artist.status = 1;
  }

  uploadFile(fileUpload: any) {
    const file: File = fileUpload;
    const formData: FormData = new FormData();
    formData.append('file', file);
    const headers = new HttpHeaders();
    headers.append('Content-type', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    const options = { headers: headers };
    this.http.post(environment.API_URL + '/file/uploadReport/artist', formData, options)
      .subscribe(
        (data: any) => {
          if (data.code === 200) {
            this.artist.path = data.data;
            this.checkUploadSucces = true;
          } else {
            this.checkUploadSucces = false;
          }
        },
        err => (this.showError('Có lỗi không xác định xảy ra')),
      );
  }

  showSuccess(message: string) {
    this.toastr.success(message);
  }

  showError(message: string) {
    this.toastr.error(message);
  }

  close() {
    this.dialogRef.close();
  }

}
