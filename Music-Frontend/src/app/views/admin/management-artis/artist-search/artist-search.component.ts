import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ArtistModel } from '../../../../Model/artist.model';

@Component({
  selector: 'app-artist-search',
  templateUrl: './artist-search.component.html',
  styleUrls: ['./artist-search.component.css']
})
export class ArtistSearchComponent implements OnInit {
  @Output() search = new EventEmitter<string>();

  searchFormGroup: FormGroup;
  listArtist: ArtistModel[];

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.searchFormGroup = this.fb.group(
      {
        artistName: [''],
      }
    );
  }

  get artistName() {
    return this.searchFormGroup.get('artistName');
  }

  searchByCondition() {
    this.search.emit(this.artistName.value);
  }

}
