import { Component, OnInit, Inject, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ArtistModel } from '../../../../Model/artist.model';
import { StatusModel } from '../../../../Model/status.model';
import { CountryModel } from '../../../../Model/country.model';
import { GrenderModel } from '../../../../Model/grender.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ArtistService } from '../../../../services/artist.service';
import { ToastrService } from 'ngx-toastr';
import { DialogsService } from '../../../../common/confirm-dialog/dialogs.service';
import { MatDialogRef, MAT_DIALOG_DATA, MatDatepickerModule, MAT_DATE_FORMATS, MAT_DATE_LOCALE, DateAdapter } from '@angular/material';
import { ArtistCreateComponent } from '../artist-create/artist-create.component';
import { environment } from '../../../../../environments/environment';
import { CountryService } from '../../../../services/country.service';
import { MY_FORMATS } from '../../../Customer/profile/profile-account/profile-account.component';
import { MomentDateAdapter } from '@angular/material-moment-adapter';

@Component({
  selector: 'app-artist-edit',
  templateUrl: './artist-edit.component.html',
  styleUrls: ['./artist-edit.component.css'],
  providers: [
    MatDatepickerModule,
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS }
  ]
})
export class ArtistEditComponent implements OnInit {
  createForm: FormGroup;
  countries: CountryModel[];
  artist: ArtistModel;
  uploadEmpty: boolean;
  imgPath: any;
  fileList: FileList;
  checkUploadSucces: boolean;
  artistId: number;
  messageErrorFile = '';

  statuses: StatusModel[] = [
    { value: 1, viewValue: 'Hoạt động' },
    { value: 0, viewValue: 'Không hoạt động' }
  ];


  genderes: GrenderModel[] = [
    { value: 0, viewValue: 'Nu'},
    { value: 1, viewValue: 'Nam'}
  ];

  constructor(
    private fb: FormBuilder,
    private http: HttpClient,
    private artistService: ArtistService,
    private toastr: ToastrService,
    private dialogSv: DialogsService,
    private countryService: CountryService,
    public dialogRef: MatDialogRef<ArtistCreateComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    this.artist = data.artistEdit;
    this.countries = data.countries;
  }

  ngOnInit(): void {
    this.imgPath = null;
    this.checkUploadSucces = false;
    this.uploadEmpty = true;
    this.createForm = this.fb.group(
      {
        artistName: [this.artist.name, Validators.required],
        artistDes: [this.artist.description, Validators.required],
        artistBirthDay: [this.artist.birthday, Validators.required],
        artistCountryId: [this.artist.countryId, Validators.required],
        artistGender: [this.artist.gender, Validators.required],
        artistStatus: [this.artist.status, Validators.required]
      }
    );
  }

  selectFile(fileInputEvent: any) {
    this.artist.path = fileInputEvent.target.files[0].name;
    const formatFile = this.artist.path.substring(this.artist.path.length - 3, this.artist.path.length);
    let checkFile;
    if (formatFile !== 'jpg' && formatFile !== 'png') {
      checkFile = true;
      this.uploadEmpty = false;
      this.messageErrorFile = 'Ảnh không đúng định dạng';
    } else {
      this.messageErrorFile = '';
      this.uploadEmpty = true;
    }

    if (checkFile) {
      return;
    }

    if (this.artist.path.indexOf('.jpg') != -1 || this.artist.path.indexOf('.png') != -1) {
      const reader = new FileReader();
      reader.readAsDataURL(fileInputEvent.target.files[0]);
      reader.onload = (_event) => {
        this.imgPath = reader.result;
        this.uploadFile(fileInputEvent.target.files[0]);
      };
    } else {
      this.imgPath = null;
    }
  }

  hasError(controlName: string, errorName: string) {
    return this.createForm.controls[controlName].hasError(errorName);
  }


  saveArtist() {
    this.checkUpload();
    this.getValueFromForm();
    this.dialogSv.confirm('', 'Bạn có chắc muốn lưu không?', 'Có', 'Không').subscribe(next => {
      if (next) {
        this.artistService.createOrUpdate(this.artist).subscribe((data: any) => {
          if (data.code === 200) {
            this.showSuccess('Cập nhật thành công');
            this.dialogRef.close();
            this.artistService.setResetForm(true);
          }
        }, err => {
          this.showError('Có lỗi không xác định xảy ra');
        });
      }
    });

  }

  getCountries(){
    this.countryService.getCountryActive().subscribe((res: any) => {
      this.countries = res.data;
    });
  }

  checkUpload(): boolean {
    this.messageErrorFile = '';
    if (!this.artist.path || this.artist.path.length === 0) {
      this.messageErrorFile = 'Ảnh không được để trống';
      return this.uploadEmpty = false;
    } else {
      return this.uploadEmpty = true;
    }
  }

  getValueFromForm() {
    this.artist.name = this.createForm.controls.artistName.value.trim();
    this.artist.description = this.createForm.controls.artistDes.value;
    this.artist.birthday= this.createForm.controls.artistBirthDay.value;
    this.artist.countryId = this.createForm.controls.artistCountryId.value;
    this.artist.gender = this.createForm.controls.artistGender.value;
    this.artist.status = this.createForm.controls.artistStatus.value;
  }

  uploadFile(fileUpload: any) {
    const file: File = fileUpload;
    const formData: FormData = new FormData();
    formData.append('file', file);
    const headers = new HttpHeaders();
    headers.append('Content-type', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    const options = { headers: headers };
    this.http.post(environment.API_URL + '/file/uploadReport/artist', formData, options)
      .subscribe(
        (data: any) => {
          if (data.code === 200) {
            this.artist.path = data.data;
            this.checkUploadSucces = true;
          } else {
            this.checkUploadSucces = false;
          }
        },
        () => (this.showError('Có lỗi không xác định xảy ra'))
      );
  }

  showSuccess(message: string) {
    this.toastr.success(message);
  }

  showError(message: string) {
    this.toastr.error(message);
  }

  close() {
    this.dialogRef.close();
  }

}
