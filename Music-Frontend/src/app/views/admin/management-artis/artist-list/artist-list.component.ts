import { Component, OnInit, Input, EventEmitter, Output, Inject, OnChanges } from '@angular/core';
import { ArtistModel } from '../../../../Model/artist.model';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { ArtistService } from '../../../../services/artist.service';
import { DomSanitizer } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
import { ArtistCreateComponent } from '../artist-create/artist-create.component';
import { ArtistEditComponent } from '../artist-edit/artist-edit.component';
import { CountryModel } from '../../../../Model/country.model';

@Component({
  selector: 'app-artist-list',
  templateUrl: './artist-list.component.html',
  styleUrls: ['./artist-list.component.css']
})
export class ArtistListComponent implements OnInit, OnChanges {
  p: any;
  @Input() listArtist: ArtistModel[];
  @Input() countries: CountryModel[];
  @Input() showChoose: boolean;
  @Input() singer: any;
  @Output() lockOrUnlock = new EventEmitter<any>();
  @Output() delete = new EventEmitter<any>();
  @Output() chooseArtist = new EventEmitter<any>();
  artistEdit: ArtistModel;
  artists = [];
  check = [];

  constructor(
    private dialog: MatDialog,
    private artistService: ArtistService,
    private domSanitizer: DomSanitizer,
    private toastr: ToastrService,
  ) { }

  ngOnInit(): void {
    if (this.singer && this.singer.length > 0) {
      this.artists.push(this.singer);
    }
  }

  ngOnChanges(changes: import("@angular/core").SimpleChanges): void {
    if ('singer' in changes) {
      if (this.singer && this.singer.length > 0) {
        this.singer.forEach((sing) => {
          this.artists.push(sing);
        });
      }
    }
  }

  checkSelect(sid) {
    if (this.artists.filter((artist) => artist.id === sid).length > 0) {
      return true;
    }
    return false;
  }

  openCreate() {
    this.artistService.setResetForm(false);
    const dialogRef = this.dialog.open(ArtistCreateComponent, {
      width: '70vw',
      panelClass: 'myapp-background-dialog'
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  openEdit(artistId) {
    this.artistService.setResetForm(false);
    this.getArtistById(artistId);
  }

  getArtistById(artistId) {
    this.artistService.getArtistById(artistId).subscribe((data: any) => {
      if (data.code === 200) {
        this.artistEdit = data.data;
        this.artistEdit.url = this.domSanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' + this.artistEdit.base64);
        if (this.artistEdit) {
          const dialogRef = this.dialog.open(ArtistEditComponent, {
            width: '70vw',
            panelClass: 'myapp-background-dialog',
            data: { artistEdit: this.artistEdit, countries: this.countries }
          });

          dialogRef.afterClosed().subscribe(result => {
          });
        }
      } else {
        this.showError(data.mstrErrors);
      }
    }, () => {
      this.showError('Có lỗi không xác định xảy ra');
    });

  }

  lockUnlock(artist) {
    this.lockOrUnlock.emit(artist);
  }

  deleteArtist(artistId) {
    this.delete.emit(artistId);
  }

  showSuccess(message: string) {
    this.toastr.success(message);
  }

  showError(message: string) {
    this.toastr.error(message);
  }

  getCountryName(id) {
    const country = this.countries.filter(cou => cou.id == id);
    if (country.length > 0) {
      return country[0].name;
    }
    return '';
  }

  choose(artist, isCheck) {
    console.log(this.artists);
    if (isCheck.checked) {
      this.artists.push(artist);
    } else {
      this.artists = this.artists.filter((vartist: ArtistModel) => vartist.id !== artist.id);
    }
    this.chooseArtist.emit(this.artists);
  }
}
