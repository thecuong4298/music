const DEFAULT_LANGUAGE = 'vi';
export const config = {
  user_default_language: 'app_DHK_default_language',
  defaultLanguage: DEFAULT_LANGUAGE,
  nav: 'nav.',
  TOKEN : '_userToken'
}