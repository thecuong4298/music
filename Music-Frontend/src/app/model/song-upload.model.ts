export class SongUpload {
    songName: string;
    artistName: string;
    artistGender: string;
    artistCountryId: number;
    songGenreName: string;
    songGenreId: number;
    songCountryName: string;
    songCountryId: number;
    songDescription: string;
    pathAvatar: string;
    pathTrack: string;
    artistId: number[];
    base64Track: string;
    base64Avatar: string;
    userName: string;
    dateOfUpload: Date;

}
