export class GenreModel {
    id: number;
    description: string;
    name: string;
    path: string;
    status: number;
    base64: string;
    url: any;
}
