export class Track {
    id?: number;
    index?: number;
    link: any;
    singer: string;
    title: string;
    img: string;
    lyric?: string;
}

export class Quenue {
    reload: boolean;
    tracks: Track[];
}
