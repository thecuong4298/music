export class CountryModel {
    id: number;
    description: string;
    name: string;
    status: number;
}