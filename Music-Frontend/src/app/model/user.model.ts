export class UserModel {
    id: number;
    username: string;
    fullName: string;
    email: string;
    phone: string;
    birthday: Date;
    gender: string;
    roleId: number;
    createDate: Date;
    pathAvatar: string;
    password: string;
    status: number;
    base64: string;
    url: any;
    path: string;
}
