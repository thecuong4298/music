import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
  name: 'Base64',
  pure: false
})
export class Base64 implements PipeTransform {
  constructor(
    private domSanitizer: DomSanitizer
  ) { }
  transform(base64, content?) {
    let contentType = 'data:image/jpg;base64,';
    if (content) {
      contentType = content;
    }
    return this.domSanitizer.bypassSecurityTrustResourceUrl(contentType + base64);
  }
}
