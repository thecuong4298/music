export class PlaylistModel {
    id: number;
    name: string;
    userId: string;
    createDate: string;
    type: string;
    status: number;
    base64: string;
    url: any;
}
