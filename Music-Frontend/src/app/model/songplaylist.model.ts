export class SongPlaylist {
    id: number;
    songId: number;
    playlistId: number;
    status: number;
}