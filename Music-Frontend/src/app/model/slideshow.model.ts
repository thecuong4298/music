export class SlideShowModel {
    id: number;
    description: string;
    name: string;
    title: string;
    path: string;
    status: number;
    base64: string;
    url: any;
    pathImg: string;
}
