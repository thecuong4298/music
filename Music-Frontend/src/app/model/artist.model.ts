export class ArtistModel {
    id: number;
    name: string;
    birthday: string;
    gender: number;
    type: number;
    description: string;
    path: string;
    countryId: number;
    countryName: string;
    status: number;
    base64: string;
    url: any;
}
