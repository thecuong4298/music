export class AlbumModel {
    id: number;
    name: string;
    description: string;
    path: string;
    releaseDate: Date;
    status: number;
    base64: string;
    url: any;
}
