export class TopicModel {
    id: number;
    name: string;
    description: string;
    path: string;
    status: number;
    base64: string;
    url: any;
}
