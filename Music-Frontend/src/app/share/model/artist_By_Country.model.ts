import { Country } from './country.model';
import { Artist } from './artist.model';
import { Slide } from './slide.model';

export class ArtistByCountry {
    country: Country;
    artistList: Artist[];
    slide: Slide[];
}
