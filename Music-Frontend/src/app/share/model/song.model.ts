export class Song {
    albumId: number;
    approved: number;
    countryId: number;
    description: string;
    genreId: number;
    id: number;
    name: string;
    path: string;
    pathAvatar: string;
    releaseDate: Date;
    status: number;
    time: number;
    userId: number;
    url: any;
    base64: string;
    lyric: string;
    singer: string;
    view: string;
    type: number;
    songBase64: string;
}
