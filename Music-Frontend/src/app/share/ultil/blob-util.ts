import * as fileSaver from 'file-saver';

/** @private */
declare var BlobBuilder: any;
/** @private */
declare var MozBlobBuilder: any;
/** @private */
declare var MSBlobBuilder: any;
/** @private */
declare var WebKitBlobBuilder: any;
/** @private */
declare var webkitURL: any;


export function createBlob(parts: Array<any>, properties?: BlobPropertyBag | string): Blob {
  parts = parts || [];
  properties = properties || {};
  if (typeof properties === 'string') {
    properties = { type: properties };
  }
  try {
    return new Blob(parts, properties);
  } catch (e) {
    if (e.name !== 'TypeError') {
      throw e;
    }
    const Builder = typeof BlobBuilder !== 'undefined'
      ? BlobBuilder : typeof MSBlobBuilder !== 'undefined'
        ? MSBlobBuilder : typeof MozBlobBuilder !== 'undefined'
          ? MozBlobBuilder : WebKitBlobBuilder;
    const builder = new Builder();
    for (let i = 0; i < parts.length; i += 1) {
      builder.append(parts[i]);
    }
    return builder.getBlob(properties.type);
  }
}

export function base64StringToBlob(base64: string, type?: string): Blob {
  const parts = [binaryStringToArrayBuffer(atob(base64))];
  return type ? createBlob(parts, { type: type }) : createBlob(parts);
}

export function binaryStringToArrayBuffer(binary: string): ArrayBuffer {
  const length = binary.length;
  const buf = new ArrayBuffer(length);
  const arr = new Uint8Array(buf);
  let i = -1;
  while (++i < length) {
    arr[i] = binary.charCodeAt(i);
  }
  return buf;
}
/**
 * Cường đẹp zaiii
 */
export function saveMp3(base64: string, title: string) {
  const blob = base64StringToBlob(base64, 'audio/mp3');
  fileSaver.saveAs(blob, title);
}
