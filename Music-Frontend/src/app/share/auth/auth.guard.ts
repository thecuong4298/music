import { STORAGE_KEY } from './../../constants/index';
import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  CanActivateChild,
  CanLoad,
  Route,
  Router,
  RouterStateSnapshot,
  UrlSegment
} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanActivateChild, CanLoad {
  constructor(
    private router: Router
  ) { }

  canActivate(route: ActivatedRouteSnapshot, currentState: RouterStateSnapshot) {
    const accessToken = localStorage.getItem(STORAGE_KEY.access_token);
    if (accessToken && accessToken != null && accessToken.length > 0) {
      // logged in so return true
      return true;
    }

    // not logged in so redirect to login page with the return url
    this.router.navigate(['/#/login'], { queryParams: { returnUrl: currentState.url } });
    return false;
  }

  canActivateChild(route: ActivatedRouteSnapshot, currentState: RouterStateSnapshot) {
    const accessToken = localStorage.getItem(STORAGE_KEY.access_token);
    if (accessToken && accessToken != null && accessToken.length > 0) {
      // logged in so return true
      return true;
    }

    // not logged in so redirect to login page with the return url
    this.router.navigate(['/account/login'], { queryParams: { returnUrl: currentState.url } });
    return false;
  }

  canLoad(route: Route, segments: UrlSegment[]) {
    return true;
  }
}

