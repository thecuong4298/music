import { AccountService } from './../services/common/account.service';
import { END_POINT, APP_CONFIG } from './../constants/index';
import { UserToken } from '../model/user-token';
import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, switchMap, tap } from 'rxjs/operators';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  isRefreshingToken = false;
  constructor(private authService: AccountService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(tap((response) => {
      if (response && response instanceof HttpResponse && response.body.code === 401) {
        return this.handle401Error(request, next);
      }
    }, (err: any) => {
      if (err instanceof HttpErrorResponse) {
        if (err instanceof HttpErrorResponse && (err as HttpErrorResponse).status === 401) {
          return this.handle401Error(request, next);
        } else {
          return throwError(err);
        }
      }
    }));
  }

  handle401Error(req: HttpRequest<any>, next: HttpHandler) {
    if (req.url.indexOf(END_POINT.authen) < 0 && !this.isRefreshingToken) {
      if (!this.isRefreshingToken) {
        this.isRefreshingToken = true;
        return this.authService.doRefreshToken().pipe(
          switchMap((res: UserToken) => {
            this.authService.refreshToken = res.refresh_token;
            this.authService.accessToken = res.access_token;
            return next.handle(this.alterToken(req));
          })
          , catchError(error => this.logoutUser(error)))
          .subscribe(result => this.isRefreshingToken = false);
      } else {
        this.authService.logout();
      }
    } else {
      return APP_CONFIG.loginErrorCode;
    }
  }

  alterToken(req: HttpRequest<any>): HttpRequest<any> {
    return req.clone({ setHeaders: { Authorization: 'Bearer ' + this.authService.accessToken } });
  }

  logoutUser(error) {
    this.authService.logout();
    return throwError(error);
  }


}
