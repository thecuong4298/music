export const API_BASE_URL = 'http://localhost:8080';

export const OAUTH2_REDIRECT_URI = 'http://localhost:4200/oauth2/redirect'

export const GOOGLE_AUTH_URL = API_BASE_URL + '/oauth2/authorize/google?redirect_uri=' + OAUTH2_REDIRECT_URI;
export const FACEBOOK_AUTH_URL = API_BASE_URL + '/oauth2/authorize/facebook?redirect_uri=' + OAUTH2_REDIRECT_URI;
export const GITHUB_AUTH_URL = API_BASE_URL + '/oauth2/authorize/github?redirect_uri=' + OAUTH2_REDIRECT_URI;

export const APP_CONFIG = {
  defaultLanguage: 'vi',
  app: 'Poly awersome music',
  appName: 'Awesome Music',
  user: 'Manhtd',
  email: '<3>',
  twitter: '<3',
  avatar: '<3',
  logo: 'logo.png',
  logoM: 'logo.png',
  BASE_URL: 'http://localhost:4200',
  // tslint:disable-next-line:max-line-length
  copyright: '2019 © Web framework 2.0 by&nbsp;<a href="http://viettel.vn" class="text-primary fw-500" title="Viettel Digital" target="_blank">Viettel Digital</a>',

  loginErrorCode: 'LOGIN_FAILED',
  showInTab: true,
  paginationOption: [5, 10, 20]
};


export const END_POINT = {
  authen: 'oauth/token',
};

export const STORAGE_KEY = {
  currentLanguage: 'defaultLanguage',
  userToken: 'userToken',
  access_token: 'access_token',
  refresh_token: 'refresh_token',
  playlist: 'playlist'
};

