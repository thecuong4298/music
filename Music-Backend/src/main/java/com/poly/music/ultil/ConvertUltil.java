package com.poly.music.ultil;

import com.poly.music.dto.SongDTO;
import com.poly.music.dto.TrackDTO;
import com.poly.music.entity.Artist;
import com.poly.music.entity.Song;
import com.poly.music.repo.ArtistRepo;
import com.poly.music.repo.SongRepo;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;

public class ConvertUltil {

  @Autowired
  private ArtistRepo artistRepo;

  public static String convertBase64(String path) {
    try {
      FileInputStream fis = null;
      File f = new File("./upload/" + path);
      fis = new FileInputStream(f);
      byte byteArray[] = new byte[(int) f.length()];
      fis.read(byteArray);
      return Base64.encodeBase64String(byteArray);
    } catch (Exception e) {
      System.out.println(e.getMessage());
      return null;
    }
  }

  public static ByteArrayInputStream convertFileToByteArrayInputStream(String path) {
    try {
      File file = new File(path);
      InputStream inputStream = new FileInputStream(file);
      return new ByteArrayInputStream(inputStream.toString().getBytes());
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public static String parseObjectToString(Object object) {
    if (object == null) {
      return null;
    }
    return object.toString();
  }

  public static Long parseObjectToLong(Object object) {
    if (object == null) {
      return null;
    }
    return Long.parseLong(object.toString());
  }

  public static Integer parseObjectToInteger(Object object) {
    if (object == null) {
      return null;
    }
    return Integer.parseInt(object.toString());
  }

  public static List<TrackDTO> convertFromSong(List<SongDTO> songs, String mstrUpload) {
    List<TrackDTO> trackDTOList = new ArrayList<>();
    for (SongDTO song : songs) {
      TrackDTO trackDTO = new TrackDTO();
      trackDTO.setId(song.getId());
      trackDTO.setTitle(song.getName());
      trackDTO.setLink(song.getPath());
      trackDTO.setSinger(song.getSinger());
      trackDTO.setImg(ConvertUltil.convertBase64(song.getPathAvatar()));
      trackDTO.setLyric(song.getLyric());
      trackDTOList.add(trackDTO);
    }
    return trackDTOList;
  }
}
