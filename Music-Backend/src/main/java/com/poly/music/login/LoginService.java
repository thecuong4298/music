package com.poly.music.login;

import com.poly.music.entity.User;
import com.poly.music.repo.RoleRepo;
import com.poly.music.repo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service(value = "login")
public class LoginService implements UserDetailsService {

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private RoleRepo roleRepo;

    @Override
    public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {

        Optional<User> optionalUser = userRepo.findByUsername(name);
        CustomeUser userDetails = null;

        if (optionalUser.isPresent()) {
            userDetails = new CustomeUser();
            userDetails.setRole(roleRepo.findById(optionalUser.get().getRoleId()).get());
            userDetails.setUser(optionalUser.get());
        } else {
            throw new UsernameNotFoundException("User not exist with name : " + name);
        }
        return userDetails;

    }
}
