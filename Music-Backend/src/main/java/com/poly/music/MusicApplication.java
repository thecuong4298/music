package com.poly.music;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.mail.MessagingException;
import java.io.IOException;

@SpringBootApplication
public class MusicApplication {



  public static void main(String[] args) {
    System.out.println(new BCryptPasswordEncoder().encode("654321a@"));
      SpringApplication.run(MusicApplication.class, args);
  }

//  @Override
//    public void run(String... args) {
//
//        System.out.println("Sending Email...");
//
//        try {
//            sendEmail();
//            //sendEmailWithAttachment();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        System.out.println("Done");
//
//    }



}
