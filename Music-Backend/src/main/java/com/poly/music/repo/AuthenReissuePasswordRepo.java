package com.poly.music.repo;

import com.poly.music.entity.AuthenReissuePassword;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface AuthenReissuePasswordRepo extends JpaRepository<AuthenReissuePassword, Long> {

    List<AuthenReissuePassword> findAllByOtpCodeAndUsernameAndStatusOrderByCreateTimeDesc(String otpCode, String username, Integer status);

}
