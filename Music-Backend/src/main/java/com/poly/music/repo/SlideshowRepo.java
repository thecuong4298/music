package com.poly.music.repo;

import com.poly.music.entity.SlideShow;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SlideshowRepo extends JpaRepository<SlideShow, Long> {
  List<SlideShow> findAllByTitleContaining(String title);
  List<SlideShow> getAllByStatus(Integer status);
}
