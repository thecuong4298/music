package com.poly.music.repo;

import com.poly.music.dto.SingerDTO;
import com.poly.music.entity.Song;

import java.util.List;

public interface SongRepoCustom {

    List<Song> findSongInPlaylistById(Long idPlaylist);

    void resetViewWeekInSong();

    void resetViewMonthInSong();

    SingerDTO getSingDTOBySongId(Long songid);

    void deleteSongArtistBySongId(Long songid);

    void deleteSongTopicBySongId(Long songid);
    List<Song> getAllSongUploadByCustomer();

    void acceptOrRejectSongUpload(Long id, Integer approved);

    Object[] getArtistBySongId(Long songid);
}
