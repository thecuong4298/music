package com.poly.music.repo;

import com.poly.music.dto.SongDTO;
import com.poly.music.entity.Album;
import com.poly.music.entity.Artist;
import java.util.List;

public interface ArtistRepoCustom {
  List<SongDTO> getSongByArtist(Long artistId);

  List<Artist> getTop4Artist();

  List<SongDTO> getSongsByTopic(Long topicId);

  List<SongDTO> getTopSongByCountry(Integer topNumber, Long countryId, boolean isMonth);

  List<SongDTO> searchSong(String search, boolean maxResult);

  List<Artist> getArtistByName(String name, boolean maxResult);

  List<Album> getAlbumByName(String name, boolean maxResult);

}
