package com.poly.music.repo.impl;

import com.poly.music.dto.SongDTO;
import com.poly.music.entity.Album;
import com.poly.music.entity.Artist;
import com.poly.music.entity.Song;
import com.poly.music.repo.ArtistRepoCustom;
import com.poly.music.service.ArtistService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ArtistRepoCustomImpl implements ArtistRepoCustom {

  @PersistenceContext
  private EntityManager entityManager;

  @Autowired
  private ArtistService artistService;

  @Override
  public List<SongDTO> getSongByArtist(Long artistId) {
    try {
      StringBuilder sql = new StringBuilder()
          .append(
              "SELECT s.id as songid, s.name as song_name, s.path, s.time, s.type, r.name, s.view, s.path_avatar, s.lyric from song s ")
          .append("join song_artist a on s.id = a.song_id join artist r ")
          .append("on a.artist_id = r.id WHERE s.type = 1 and a.artist_id = :artistId and s.status = 1");
      List<Object[]> objectList = entityManager.createNativeQuery(sql.toString())
          .setParameter("artistId", artistId).getResultList();
      List<SongDTO> listSong = new ArrayList<>();
      if (objectList != null && !objectList.isEmpty()) {
        listSong = objectList.stream()
            .map(SongDTO::new).collect(Collectors.toList());
      }
      for(SongDTO songDTO: listSong) {
        songDTO.setSinger(artistService.getArtistBySongId(songDTO.getId()));
      }
      return listSong;
    } catch (Exception e) {
      e.printStackTrace();
    }
    return new ArrayList<>();
  }

  @Override
  public List<Artist> getTop4Artist() {
    try {
      StringBuilder sql = new StringBuilder()
          .append("FROM Artist WHERE status = 1 ORDER BY countLike DESC");
      List<Artist> list = entityManager.createQuery(sql.toString()).setMaxResults(4)
          .getResultList();
      return list;
    } catch (Exception e) {
      return new ArrayList<>();
    }
  }

  @Override
  public List<SongDTO> getSongsByTopic(Long topicId) {
    try {
      StringBuilder sql = new StringBuilder()
          .append(
              "SELECT s.id as songid, s.name as song_name, s.path, s.time, s.type, s.approved , s.view, s.path_avatar, s.lyric from song s ")
          .append("WHERE ")
          .append("s.id in (select song_id from song_topic where topic_id = :topicId) and s.type = 1 and s.status = 1");
      List<Object[]> objectList = entityManager.createNativeQuery(sql.toString())
          .setParameter("topicId", topicId).getResultList();
      List<SongDTO> listSong = new ArrayList<>();
      if (objectList != null && !objectList.isEmpty()) {
        listSong = objectList.stream()
            .map(SongDTO::new).collect(Collectors.toList());
      }
      for(SongDTO songDTO: listSong) {
        songDTO.setSinger(artistService.getArtistBySongId(songDTO.getId()));
      }
      return listSong;
    } catch (Exception e) {
      e.printStackTrace();
    }
    return new ArrayList<>();
  }

  @Override
  public List<SongDTO> getTopSongByCountry(Integer topNumber, Long countryId, boolean isMonth) {
    try {
      StringBuilder sql = new StringBuilder()
          .append(
              "SELECT s.id as songid, s.name as song_name, s.path, s.time, s.type, s.approved, s.view, s.path_avatar, s.lyric from song s ")
          .append("WHERE ")
          .append(
              "s.country_id = :countryId and s.type = 1 and s.status = 1 order by ");
      if(isMonth)
        sql.append("s.view_month DESC, s.view desc");
      else
        sql.append("s.view_week desc, s.view_month DESC, s.view desc");
      List<Object[]> objectList = entityManager.createNativeQuery(sql.toString())
          .setParameter("countryId", countryId).setMaxResults(topNumber).getResultList();
      List<SongDTO> listSong = new ArrayList<>();
      if (objectList != null && !objectList.isEmpty()) {
        listSong = objectList.stream()
            .map(SongDTO::new).collect(Collectors.toList());
      }
      for(SongDTO songDTO: listSong) {
        songDTO.setSinger(artistService.getArtistBySongId(songDTO.getId()));
      }
      return listSong;
    } catch (Exception e) {
      e.printStackTrace();
    }
    return new ArrayList<>();
  }

  @Override
  public List<SongDTO> searchSong(String search, boolean maxResult) {
    try {
      StringBuilder sql = new StringBuilder()
          .append(
              "SELECT s.id as songid, s.name as song_name, s.path, s.time, s.type, r.name, s.view, s.path_avatar, s.lyric from song s ")
          .append("join song_artist a on s.id = a.song_id join artist r ")
          .append("on a.artist_id = r.id WHERE s.type = 1 s.name like N'%")
          .append(search).append("%' and s.status = 1");
      Query query = entityManager.createNativeQuery(sql.toString());
      if (maxResult) {
        query.setMaxResults(6);
      }
      List<Object[]> objectList = query.getResultList();
      List<SongDTO> listSong = new ArrayList<>();
      if (objectList != null && !objectList.isEmpty()) {
        listSong = objectList.stream()
            .map(SongDTO::new).collect(Collectors.toList());
      }
      for(SongDTO songDTO: listSong) {
        songDTO.setSinger(artistService.getArtistBySongId(songDTO.getId()));
      }
      return listSong;
    } catch (Exception e) {
      e.printStackTrace();
    }
    return new ArrayList<>();
  }

  @Override
  public List<Artist> getArtistByName(String name, boolean maxResult) {
    try {
      StringBuilder sql = new StringBuilder()
          .append(
              "from Artist where name like '%")
          .append(name)
          .append("%' and status = 1");
      Query query = entityManager.createQuery(sql.toString());
      if (maxResult) {
        query.setMaxResults(5);
      }
      return query.getResultList();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return new ArrayList<>();
  }

  @Override
  public List<Album> getAlbumByName(String name, boolean maxResult) {
    try {
      StringBuilder sql = new StringBuilder()
          .append(
              "from Album where name like '%")
          .append(name)
          .append("%' and status = 1");
      Query query = entityManager.createQuery(sql.toString());
      if (maxResult) {
        query.setMaxResults(5);
      }
      return query.getResultList();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return new ArrayList<>();
  }

}


