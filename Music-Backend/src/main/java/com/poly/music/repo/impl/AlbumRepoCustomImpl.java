package com.poly.music.repo.impl;

import com.poly.music.dto.SongDTO;
import com.poly.music.entity.Album;
import com.poly.music.entity.Genre;
import com.poly.music.entity.Song;
import com.poly.music.repo.AlbumRepoCustom;
import com.poly.music.repo.ArtistRepo;
import com.poly.music.service.ArtistService;
import java.util.ArrayList;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class AlbumRepoCustomImpl implements AlbumRepoCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private ArtistService artistService;

    @Override
    public List<Album> get4AlbumRandom() {
        try {
            StringBuilder sql = new StringBuilder()
                    .append("From Album WHERE status = 1 ORDER BY RAND()");
            List<Album> list = entityManager.createQuery(sql.toString()).setMaxResults(4).getResultList();
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<SongDTO> getSongBy(String by, Long id) {
        try {
            StringBuilder sql = new StringBuilder()
                .append("SELECT s.id as songid, s.name as song_name, s.path, s.time, s.type, s.approved, s.view, s.path_avatar, s.lyric from song s ")
                .append("WHERE s.")
                .append(by).append(" = :id and s.type = 1 and s.status = 1");
            List<Object[]> objectList = entityManager.createNativeQuery(sql.toString()).setParameter("id", id).getResultList();
            List<SongDTO> listSong = new ArrayList<>();
            if (objectList != null && !objectList.isEmpty()) {
                listSong = objectList.stream()
                    .map(SongDTO::new).collect(Collectors.toList());
            }
            for(SongDTO songDTO: listSong) {
                songDTO.setSinger(artistService.getArtistBySongId(songDTO.getId()));
            }
            return listSong;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    @Override
    public List<Album> getByArtist(Long artistId) {
        try {
            StringBuilder sql = new StringBuilder()
                .append("SELECT ab.id, ab.name, ab.path_avatar from album ab ")
                .append("where status = 1 and id in (SELECT s.album_id from song s ")
                .append("join song_artist a on s.id = a.song_id join artist r ")
                .append("on a.artist_id = r.id WHERE a.artist_id = :id )");
            List<Object[]> objectList = entityManager.createNativeQuery(sql.toString()).setParameter("id", artistId).getResultList();
            if (objectList != null && !objectList.isEmpty()) {
                return objectList.stream()
                    .map(Album::new).collect(Collectors.toList());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }
}
