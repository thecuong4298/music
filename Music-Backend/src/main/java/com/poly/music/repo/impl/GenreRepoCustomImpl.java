package com.poly.music.repo.impl;

import com.poly.music.entity.Genre;
import com.poly.music.entity.Song;
import com.poly.music.repo.GenreRepoCustom;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class GenreRepoCustomImpl implements GenreRepoCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Genre> get7GenreRandom() {
        try {
            StringBuilder sql = new StringBuilder()
                    .append("From Genre WHERE status = 1 ORDER BY RAND() ");
            List<Genre> list = entityManager.createQuery(sql.toString()).setMaxResults(6).getResultList();
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
