package com.poly.music.repo;

import com.poly.music.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleRepo extends JpaRepository<Role, Long> {

  Role getById(Long id);

}
