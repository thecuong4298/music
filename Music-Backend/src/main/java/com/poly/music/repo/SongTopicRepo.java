package com.poly.music.repo;

import com.poly.music.entity.SongTopic;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SongTopicRepo extends JpaRepository<SongTopic, Long> {

}
