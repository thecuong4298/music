package com.poly.music.repo;

import com.poly.music.entity.SongArtist;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SongArtistRepo extends JpaRepository<SongArtist, Long> {
  List<SongArtist> getAllBySongIdAndStatus(Long songId, Integer status);

  List<SongArtist> findAllBySongIdAndStatus(Long songId, Integer status);

}
