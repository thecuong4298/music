package com.poly.music.repo;

import com.poly.music.entity.Role;

import java.util.List;
import java.util.Optional;

public interface RoleRepoCustom {

    List<Role> findRolesByUser();

}
