package com.poly.music.repo;

import com.poly.music.entity.Genre;

import java.util.List;

public interface GenreRepoCustom {

    List<Genre> get7GenreRandom();

}
