package com.poly.music.repo.impl;

import com.poly.music.dto.SingerDTO;
import com.poly.music.entity.Song;
import com.poly.music.entity.Topic;
import com.poly.music.repo.SongRepoCustom;
import java.util.ArrayList;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class SongRepoCustomImpl implements SongRepoCustom {

  @PersistenceContext
  private EntityManager entityManager;

  @Override
  public List<Song> findSongInPlaylistById(Long idPlaylist) {
    try {
      StringBuilder sql =
          new StringBuilder()
              .append("SELECT s From Song s WHERE s.status = 1 AND s.id IN ")
              .append("(SELECT sp.songId FROM SongPlaylist sp WHERE sp.playlistId = :idPlaylist)");
      List<Song> list =
          entityManager
              .createQuery(sql.toString())
              .setParameter("idPlaylist", idPlaylist)
              .getResultList();
      return list;
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  @Override
  @Transactional
  public void resetViewWeekInSong() {
    try {
      StringBuilder sql = new StringBuilder().append("update Song set viewWeek = 0");
      entityManager.createQuery(sql.toString()).executeUpdate();

    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  @Transactional
  public void resetViewMonthInSong() {
    try {
      StringBuilder sql = new StringBuilder().append("update Song set viewMonth = 0");
      entityManager.createQuery(sql.toString());

    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public SingerDTO getSingDTOBySongId(Long songid) {
    StringBuilder sql = new StringBuilder()
        .append("select a.name, a.id from artist a ")
        .append("join song_artist s on a.id = s.artist_id WHERE s.song_id = :songId");
    List<Object[]> list =
        entityManager.createNativeQuery(sql.toString())
            .setParameter("songId", songid).getResultList();
    if (!list.isEmpty() && list.size() > 0) {
      SingerDTO singerDTO = new SingerDTO();
      String singer = "";
      List<Long> id = new ArrayList<>();
      for (Object[] obj : list) {
        singer = singer + obj[0].toString() + ", ";
        id.add(Long.parseLong(obj[1].toString()));
      }
      singerDTO.setSinger(singer.substring(0, singer.length() - 2));
      singerDTO.setId(id);
      return singerDTO;
    }
    return new SingerDTO();
  }

  @Override
  public void deleteSongArtistBySongId(Long songid) {
    StringBuilder sql = new StringBuilder()
        .append("DELETE FROM SongArtist WHERE songId =:id");
    entityManager.createQuery(sql.toString()).setParameter("id", songid).executeUpdate();
  }

  @Override
  public void deleteSongTopicBySongId(Long songid) {
    StringBuilder sql = new StringBuilder()
        .append("DELETE FROM SongTopic WHERE topicId =:id");
    entityManager.createQuery(sql.toString()).setParameter("id", songid).executeUpdate();
  }

  public Object[] getArtistBySongId(Long songid) {
    StringBuilder sql =
        new StringBuilder()
            .append("select a.name, a.id from artist a ")
            .append("join song_artist s on a.id = s.artist_id WHERE s.song_id = :songId");
    List<Object[]> list =
        entityManager
            .createNativeQuery(sql.toString())
            .setParameter("songId", songid)
            .setMaxResults(1)
            .getResultList();
    if (!list.isEmpty() && list.size() > 0) {
      return list.get(0);
    }
    return new Object[0];
  }

  @Override
  public List<Song> getAllSongUploadByCustomer() {
    try {
      StringBuilder sql =
          new StringBuilder()
              .append("From Song s WHERE s.status = 1 AND type = 2 ORDER BY releaseDate");
      List<Song> list = entityManager.createQuery(sql.toString()).getResultList();
      return list;
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  @Override
  @Transactional
  public void acceptOrRejectSongUpload(Long id, Integer approved) {
    try {
      StringBuilder sql =
          new StringBuilder().append("update Song set approved =:approved WHERE id=: id");
      entityManager
          .createQuery(sql.toString())
          .setParameter("id", id)
          .setParameter("approved", approved)
          .executeUpdate();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
