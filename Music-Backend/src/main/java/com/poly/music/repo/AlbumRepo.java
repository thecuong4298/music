package com.poly.music.repo;

import com.poly.music.entity.Album;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AlbumRepo extends JpaRepository<Album, Long>, AlbumRepoCustom {

  List<Album> getAllByStatus(Integer status);

  List<Album> getById(Long id);
  
  List<Album> findAllByNameContaining(String albumName);
}
