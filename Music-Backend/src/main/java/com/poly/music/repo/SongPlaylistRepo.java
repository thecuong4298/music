package com.poly.music.repo;

import com.poly.music.entity.SongPlaylist;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SongPlaylistRepo extends JpaRepository<SongPlaylist, Long> {

    List<SongPlaylist> findAllByPlaylistIdAndSongId(Long playlistId, Long songId);

    List<SongPlaylist> findAllByPlaylistId(Long playlistId);

}
