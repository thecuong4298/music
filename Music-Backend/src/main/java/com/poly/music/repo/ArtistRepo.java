package com.poly.music.repo;

import com.poly.music.entity.Artist;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ArtistRepo extends JpaRepository<Artist, Long>, ArtistRepoCustom {

  List<Artist> getAllByStatus(Integer status);

  List<Artist> getAllByCountryIdAndStatus(Long countryId, Integer status);

  Optional<Artist> getById(Long artistId);

  List<Artist> findAllByNameContaining(String artistName);

  List<Artist> getAllByNameContaining(String name);
}
