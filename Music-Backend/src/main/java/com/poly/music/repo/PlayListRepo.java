package com.poly.music.repo;

import com.poly.music.entity.Playlist;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PlayListRepo extends JpaRepository<Playlist, Long>,PlayListRepoCustom {

    List<Playlist> findAllByUserIdAndStatus(Long userId, Integer status);

}
