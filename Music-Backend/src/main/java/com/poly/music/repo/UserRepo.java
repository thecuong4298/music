package com.poly.music.repo;

import com.poly.music.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepo extends JpaRepository<User, Long>, UserRepoCustom {

  //  List<User> findAllByName(String userName);
  List<User> findAllByUsernameContaining(String userName);

  List<User> getAllByStatus(Integer status);

  Optional<User> findByUsername(String username);

  Optional<User> findByEmailAndUsernameAndStatus(String email, String username, Integer status);
}
