package com.poly.music.repo;

import com.poly.music.entity.Topic;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TopicRepo extends JpaRepository<Topic, Long> , TopicRepoCustom {

  List<Topic> getAllByStatus(Integer status);
  
  List<Topic> findAllByNameContaining(String topicName);

}
