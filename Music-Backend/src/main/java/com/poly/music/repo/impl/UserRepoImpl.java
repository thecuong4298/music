package com.poly.music.repo.impl;

import com.poly.music.entity.Role;
import com.poly.music.entity.Song;
import com.poly.music.entity.User;
import com.poly.music.repo.UserRepoCustom;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class UserRepoImpl implements UserRepoCustom {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<User> get7UserRandom()  {
        try {
            StringBuilder sql = new StringBuilder()
                    .append("From User WHERE status = 1 ORDER BY RAND() ");
            List<User> list = entityManager.createQuery(sql.toString()).setMaxResults(6).getResultList();
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

//    @Override
//    public List<Role> getRoleBy(String ro, Long id)     try {
//        StringBuilder sql = new StringBuilder()
//                .append("SELECT s.id as songid, s.name as song_name, s.path, s.time, s.base64, r.name, s.view from song s ")
//                .append("join song_artist a on s.id = a.song_id join artist r ")
//                .append("on a.artist_id = r.id WHERE s.")
//                .append(by).append(" = :id and s.status = 1");
//        List<Object[]> objectList = entityManager.createNativeQuery(sql.toString()).setParameter("id", id).getResultList();
//        if (objectList != null && !objectList.isEmpty()) {
//            return objectList.stream()
//                    .map(Song::new).collect(Collectors.toList());
//        }
//    } catch (Exception e) {
//        e.printStackTrace();
//    }
//        return new ArrayList<>();
//}
}
