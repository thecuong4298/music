package com.poly.music.repo;

import com.poly.music.dto.SongDTO;
import com.poly.music.entity.Album;
import com.poly.music.entity.Genre;

import com.poly.music.entity.Song;
import java.util.List;

public interface AlbumRepoCustom {

    List<Album> get4AlbumRandom();

    List<SongDTO> getSongBy(String by, Long id);

    List<Album> getByArtist(Long artistId);
}
