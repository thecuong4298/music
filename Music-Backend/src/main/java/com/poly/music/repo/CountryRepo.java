package com.poly.music.repo;

import com.poly.music.entity.Country;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CountryRepo extends JpaRepository<Country, Long> {

  List<Country> getAllByStatus(Integer status);

}
