package com.poly.music.repo;

import com.poly.music.entity.Genre;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface GenreRepo extends JpaRepository<Genre, Long>, GenreRepoCustom {

    List<Genre> findAllByNameContaining(String genreName);

    List<Genre> getAllByStatus(Integer status);
}
