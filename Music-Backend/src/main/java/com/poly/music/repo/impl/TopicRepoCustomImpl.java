package com.poly.music.repo.impl;

import com.poly.music.entity.Album;
import com.poly.music.entity.Topic;
import com.poly.music.repo.TopicRepo;
import com.poly.music.repo.TopicRepoCustom;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class TopicRepoCustomImpl implements TopicRepoCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Topic> get3topicRandom() {
        try {
            StringBuilder sql = new StringBuilder()
                    .append("From Topic WHERE status = 1 ORDER BY RAND() ");
            List<Topic> list = entityManager.createQuery(sql.toString()).setMaxResults(4).getResultList();
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
