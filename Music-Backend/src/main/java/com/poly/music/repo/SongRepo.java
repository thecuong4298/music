package com.poly.music.repo;

import com.poly.music.entity.Song;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SongRepo extends JpaRepository<Song, Long>, SongRepoCustom {

    List<Song> findAllByUserIdAndStatusOrderByReleaseDateDesc(Long userId, Integer status);
    
    List<Song> findAllByNameContaining(String songName);

}
