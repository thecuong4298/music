package com.poly.music.repo;

import com.poly.music.entity.Album;
import com.poly.music.entity.Topic;

import java.util.List;

public interface TopicRepoCustom {

    List<Topic> get3topicRandom();

}
