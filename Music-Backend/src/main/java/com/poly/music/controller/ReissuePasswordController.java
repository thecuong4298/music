package com.poly.music.controller;

import com.poly.music.dto.ApiResponse;
import com.poly.music.entity.User;
import com.poly.music.repo.UserRepo;
import com.poly.music.service.ReissuePasswordService;
import com.poly.music.service.UserService;
import com.poly.music.ultil.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

@RestController
@RequestMapping("user")
public class ReissuePasswordController {

    @Autowired
    ReissuePasswordService reissuePasswordService;

    @Autowired
    UserService userService;

    @Autowired
    UserRepo userRepo;

    @PostMapping("reissuePassword")
    public ApiResponse createOrUpdateTopic(@RequestParam String email, @RequestParam String username) {
        try {
            String result = reissuePasswordService.sendEmailAuthen(email, username);
            if ("False".equals(result)) {
                return ApiResponse.build(HttpServletResponse.SC_BAD_REQUEST, true, "Email hoặc password không chính xác", null);
            }
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null, result);
        } catch (Exception e) {
            return ApiResponse.build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }

    @PostMapping("authenOTP")
    public ApiResponse authenOTP(@RequestParam String otp, @RequestParam String username) {
        try {
            String result = reissuePasswordService.findAllByOtpCodeAndUsernameAndStatusOrderByCreateTimeDesc(otp, username, 1);
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null, result);
        } catch (BusinessException bx) {
            return ApiResponse.build(HttpServletResponse.SC_BAD_REQUEST, true, bx.getMessage(), null);
        } catch (Exception ex) {
            return ApiResponse.build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, ex.getMessage(), null);
        }
    }

    @PostMapping("saveNewPass")
    public ApiResponse saveNewPass(@RequestParam String newPass, @RequestParam String username) {
        try {
            Optional<User> userOptional = userService.findByUsername(username);
            if (!userOptional.isPresent()) {
                return ApiResponse.build(HttpServletResponse.SC_BAD_REQUEST, true, "Không tìm thấy user", null);
            }
//            userOptional.get().setPassword(new BCryptPasswordEncoder().encode(newPass));
            User newUser = userOptional.get();
            newUser.setPassword(new BCryptPasswordEncoder().encode(newPass));
            userRepo.save(newUser);

            return ApiResponse.build(HttpServletResponse.SC_OK, true, null, "Success");
        }   catch (Exception ex) {
            return ApiResponse.build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, ex.getMessage(), null);
        }
    }

}
