package com.poly.music.controller;

import com.poly.music.dto.ApiResponse;
import com.poly.music.entity.Genre;
import com.poly.music.service.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("music")
public class GenreController {

    @Autowired
    private GenreService genreService;

    @GetMapping("getByCondition")
    public ApiResponse getByCondition(@RequestParam(required = false) String genreName) {
        try {
            List<Genre> genreList = genreService.findByCondition(genreName);
            if (genreList == null || genreList.isEmpty()) {
                return ApiResponse.build(HttpServletResponse.SC_NO_CONTENT, true, null,new ArrayList<>());
            }
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null, genreList);
        } catch (Exception e) {
            return ApiResponse.build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }

    @GetMapping("getAll")
    public ApiResponse findAllGenre() {
        try {
            List<Genre> genreList = genreService.getAllGenre();
            if (genreList == null || genreList.isEmpty()) {
                return ApiResponse.build(HttpServletResponse.SC_NO_CONTENT, true, null,new ArrayList<>());
            }
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null, genreService.getAllGenre());
        } catch (Exception e) {
            return ApiResponse.build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }

    @PostMapping("save")
    public ApiResponse createOrUpdateGenre(@RequestBody Genre genre) {
        try {
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null,genreService.createOrUpdateGenre(genre));
        } catch (Exception e) {
            return ApiResponse.build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }

    @GetMapping("getGenreById/{id}")
    public ApiResponse getGenreById(@PathVariable Long id) {
        try {
            Optional<Genre> optionalGenre = genreService.getGenreById(id);
            if (!optionalGenre.isPresent()) {
                return ApiResponse.build(HttpServletResponse.SC_NO_CONTENT, true, "Không tìm thấy giá trị",null);
            }
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null, optionalGenre.get());
        } catch (Exception e) {
            return ApiResponse.build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }

    @DeleteMapping("deleteGenre/{id}")
    public ApiResponse deleteGenre(@PathVariable Long id) {
        try {
            Optional<Genre> optionalGenre = genreService.getGenreById(id);
            if (!optionalGenre.isPresent()) {
                return ApiResponse.build(HttpServletResponse.SC_NO_CONTENT, true, "Không tìm thấy giá trị",null);
            }
            genreService.deleteById(id);
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null, null);
        } catch (Exception e) {
            return ApiResponse.build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }

    @GetMapping("get7GenreRandom")
    public ApiResponse get7GenreRandom() {
        try {
            List<Genre> genreList = genreService.get7GenreRandom();
            if (genreList == null || genreList.isEmpty()) {
                return ApiResponse.build(HttpServletResponse.SC_NO_CONTENT, true, "Không tìm thấy giá trị",null);
            }
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null, genreList);
        } catch (Exception e) {
            return ApiResponse.build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }
}
