package com.poly.music.controller;

import com.poly.music.dto.ApiResponse;
import com.poly.music.entity.Artist;
import com.poly.music.service.ArtistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("music")
public class ArtistController {

    @Autowired
    ArtistService artistService;

    @GetMapping("getTop4")
    public ApiResponse getTop4Artist() {
        try {
            List<Artist> artists = artistService.getTop4Artist();
            if (artists == null || artists.isEmpty()) {
                return ApiResponse.build(HttpServletResponse.SC_NO_CONTENT, true, null,new ArrayList<>());
            }
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null, artists);
        } catch (Exception e) {
            return ApiResponse.build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }
    
    @GetMapping("getAllArtist")
    public ApiResponse findAllTopic() {
        try {
            List<Artist> artistList = artistService.getAllArtist();
            if (artistList == null || artistList.isEmpty()) {
                return ApiResponse.build(HttpServletResponse.SC_NO_CONTENT, true, null,new ArrayList<>());
            }
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null, artistService.getAllArtist());
        } catch (Exception e) {
            return ApiResponse.build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }
    
    @PostMapping("saveArtist")
    public ApiResponse createOrUpdateArtist(@RequestBody Artist artist) {
        try {
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null,artistService.createOrUpdateArtist(artist));
        } catch (Exception e) {
            return ApiResponse.build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }
    
    @GetMapping("getArtistById/{artistId}")
    public ApiResponse getArtistById(@PathVariable Long artistId) {
    	 try {
             return ApiResponse.build(HttpServletResponse.SC_OK, true, null, artistService.getById(artistId));
         } catch (Exception e) {
             return ApiResponse.build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
         }
    }
    
    @DeleteMapping("deleteArtist/{artistId}")
    public ApiResponse deleteArtist(@PathVariable Long artistId) {
        try {
            Optional<Artist> optionalArtist = artistService.getArtistsById(artistId);
            if (!optionalArtist.isPresent()) {
                return ApiResponse.build(HttpServletResponse.SC_NO_CONTENT, true, "Không tìm thấy giá trị",null);
            }
            artistService.deleteById(artistId);
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null, null);
        } catch (Exception e) {
            return ApiResponse.build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }
    
    @GetMapping("getArtistByCondition")
    public ApiResponse getByCondition(@RequestParam(required = false) String artistName) {
        try {
            List<Artist> artistList = artistService.findByCondition(artistName);
            if (artistList == null || artistList.isEmpty()) {
                return ApiResponse.build(HttpServletResponse.SC_NO_CONTENT, true, null,new ArrayList<>());
            }
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null, artistList);
        } catch (Exception e) {
            return ApiResponse.build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }

}
