package com.poly.music.controller;

import com.poly.music.dto.ApiResponse;
import com.poly.music.service.FileService;
import com.poly.music.ultil.ConvertUltil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("file")
public class FileController {

  @Value("${pathUpload}")
  private String mstrUpload;

  @Autowired
  private FileService fileService;

  @PostMapping("/uploadReport/{type}")
  public ApiResponse uploadFileDesign(@RequestParam("file") MultipartFile file,
      @PathVariable String type) {
    try {
      return ApiResponse
          .build(HttpServletResponse.SC_OK, true, "", fileService.upLoadFile(file, type));
    } catch (Exception e) {
      e.printStackTrace();
      return ApiResponse.build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, false, "", null);
    }
  }

  @PostMapping("/getMedia")
  public ApiResponse getMedia(@RequestBody String path) {
    try {
      String a = ConvertUltil.convertBase64(path);
      if (a == null) {
        return ApiResponse
            .build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, null, null);
      }
      return ApiResponse.build(HttpServletResponse.SC_OK, true, null,
          a);
    } catch (Exception e) {
      return ApiResponse
          .build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
    }
  }

  @PostMapping("/uploadSong")
  public ApiResponse uploadSong(@RequestParam("file") MultipartFile file) {
    try {
      return ApiResponse
          .build(HttpServletResponse.SC_OK, true, "", fileService.upLoadSong(file));
    } catch (Exception e) {
      e.printStackTrace();
      return ApiResponse.build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, false, "", null);
    }
  }

}
