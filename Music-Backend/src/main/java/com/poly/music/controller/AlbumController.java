package com.poly.music.controller;

import com.poly.music.dto.ApiResponse;
import com.poly.music.entity.Album;
import com.poly.music.service.AlbumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("music")
public class AlbumController {

    @Autowired
    AlbumService albumService;

    @GetMapping("getTop4Album")
    public ApiResponse getTop4Album() {
        try {
            List<Album> albums = albumService.get4AlbumRandom();
            if (albums == null || albums.isEmpty()) {
                return ApiResponse.build(HttpServletResponse.SC_NO_CONTENT, true, null,new ArrayList<>());
            }
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null, albums);
        } catch (Exception e) {
            return ApiResponse.build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }
    
    @GetMapping("getAllAlbum")
    public ApiResponse findAllTopic() {
        try {
            List<Album> topicAlbum = albumService.getAllAlbum();
            if (topicAlbum == null || topicAlbum.isEmpty()) {
                return ApiResponse.build(HttpServletResponse.SC_NO_CONTENT, true, null,new ArrayList<>());
            }
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null, albumService.getAllAlbum());
        } catch (Exception e) {
            return ApiResponse.build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }
    
    @PostMapping("saveAlbum")
    public ApiResponse createOrUpdateAlbum(@RequestBody Album album) {
        try {
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null,albumService.createOrUpdateAlbum(album));
        } catch (Exception e) {
            return ApiResponse.build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }
    
    @GetMapping("getAlbumById/{albumId}")
    public ApiResponse getAlbumById(@PathVariable Long topicId) {
    	 try {
             return ApiResponse.build(HttpServletResponse.SC_OK, true, null, albumService.getById(topicId));
         } catch (Exception e) {
             return ApiResponse.build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
         }
    }
    
    @DeleteMapping("deleteAlbum/{albumId}")
    public ApiResponse deleteAlbum(@PathVariable Long albumId) {
        try {
            Optional<Album> optionalAlbum = albumService.getAlbumById(albumId);
            if (!optionalAlbum.isPresent()) {
                return ApiResponse.build(HttpServletResponse.SC_NO_CONTENT, true, "Không tìm thấy giá trị",null);
            }
            albumService.deleteById(albumId);
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null, null);
        } catch (Exception e) {
            return ApiResponse.build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }
    
    @GetMapping("getAlbumByCondition")
    public ApiResponse getByCondition(@RequestParam(required = false) String albumName) {
        try {
            List<Album> albumList = albumService.findByCondition(albumName);
            if (albumList == null || albumList.isEmpty()) {
                return ApiResponse.build(HttpServletResponse.SC_NO_CONTENT, true, null,new ArrayList<>());
            }
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null, albumList);
        } catch (Exception e) {
            return ApiResponse.build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }
}
