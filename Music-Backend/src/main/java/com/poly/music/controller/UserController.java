package com.poly.music.controller;

import com.poly.music.dto.ApiResponse;
import com.poly.music.dto.SongDTO;
import com.poly.music.dto.SongUploadDTO;
import com.poly.music.entity.SlideShow;
import com.poly.music.entity.Song;
import com.poly.music.entity.SongPlaylist;
import com.poly.music.entity.User;
import com.poly.music.service.*;
import com.poly.music.ultil.ConvertUltil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("user")
public class UserController {

    @Value("${pathUpload}")
    private String mstrUpload;

    @Autowired
    private UserService userService;

    @Autowired
    private ArtistService artistService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private GenreService genreService;

    @Autowired
    private AlbumService albumService;

    @Autowired
    private TopicService topicService;

    @Autowired
    private SongService songService;

    @Autowired
    private SearchService searchService;

    @GetMapping("getByCondition")
    public ApiResponse getByCondition(@RequestParam(required = false) String userName) {
        try {
            List<User> userList = userService.findByCondition(userName);
            if (userList == null || userList.isEmpty()) {
                return ApiResponse.build(HttpServletResponse.SC_NO_CONTENT, true, null,new ArrayList<>());
            }
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null, userList);
        } catch (Exception e) {
            return ApiResponse.build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }
    @Autowired
    private CountryService countryService;

    @Autowired
    private SlideshowService slideshowService;

    @Autowired
    private SongPlaylistService songPlaylistService;

    @GetMapping("getAll")
    public ApiResponse getAll() {
        try {
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null, userService.getAllUser());
        } catch (Exception e) {
            return ApiResponse.build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }

    @GetMapping("getById/{id}")
    public ApiResponse getById(@PathVariable Long id) {
        try {
            Optional<User> optionalUser = userService.getUserById(id);
            if (!optionalUser.isPresent()) {
                return ApiResponse.build(HttpServletResponse.SC_NO_CONTENT, true, "Không tìm thấy giá trị",null);
            }
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null, optionalUser.get());
        } catch (Exception e) {
            return ApiResponse.build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }


//    @PostMapping("save")
//    public ApiResponse save(@RequestBody User user) {
//        try {
//            return ApiResponse.build(HttpServletResponse.SC_OK, true, null,userService.save(user));
//        } catch (Exception e) {
//            return ApiResponse.build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
//        }
//    }

    @PostMapping("save")
    public ApiResponse save(@RequestBody User user) {
        try {
            User newUser =  userService.save(user);
            if (newUser != null) {
                return ApiResponse.build(HttpServletResponse.SC_OK, true, null, newUser);
            }
            return ApiResponse.build(HttpServletResponse.SC_BAD_REQUEST, true, "Lưu không thành công", null);
        } catch (Exception e) {
            return ApiResponse.build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }

    @GetMapping("getUserById/{id}")
    public  ApiResponse getUserById(@PathVariable Long id){
        try {
            Optional<User> optionlUser = userService.getUserById(id);
            if(!optionlUser.isPresent()){{
                return ApiResponse.build(HttpServletResponse.SC_NO_CONTENT,true,"khong tim thay gia tri",null);
            }}
            return  ApiResponse.build(HttpServletResponse.SC_OK,true,null,optionlUser.get());

        }
        catch (Exception e){{
             return ApiResponse.build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }}
    }

    @DeleteMapping("deleteUser/{id}}")
    public ApiResponse deleteUser(@PathVariable Long id){

            try {
                Optional<User> optionalUser = userService.getUserById(id);
                if (!optionalUser.isPresent()) {
                        return ApiResponse.build(HttpServletResponse.SC_NO_CONTENT, true, "khong tim thay gia tri", null);
                    }
                userService.deleteById(id);
                return  ApiResponse.build(HttpServletResponse.SC_OK,true,null,null);
            }
            catch (Exception e) {{
                    return ApiResponse.build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
                } }
        }

    @GetMapping("get7UserRandom")
    public ApiResponse get7UserRandom() {
        try {
            List<User> UserList = userService.get7UserRandom();
            if (UserList == null || UserList.isEmpty()) {
                return ApiResponse.build(HttpServletResponse.SC_NO_CONTENT, true, "Không tìm thấy giá trị",null);
            }
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null, UserList);
        } catch (Exception e) {
            return ApiResponse.build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }





    @GetMapping("getRoleById/{id}")
    public ApiResponse getRoleById(@PathVariable Long id) {
        try {
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null, roleService.getRoleById(id));
        } catch (Exception e) {
            return ApiResponse.build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }

    @GetMapping("getAllArtistByCountry")
    public ApiResponse getAllArtistByCountry() {
        try {
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null, artistService.getAllArtistByCountry());
        } catch (Exception e) {
            return ApiResponse.build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }

    @GetMapping("getSongByArtist/{artistId}")
    public ApiResponse getSongByArtist(@PathVariable Long artistId) {
        try {
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null, artistService.getSongByArtist(artistId));
        } catch (Exception e) {
            return ApiResponse.build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }

    @GetMapping("getArtistById/{artistId}")
    public ApiResponse getArtistById(@PathVariable Long artistId) {
        try {
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null, artistService.getArtistById(artistId));
        } catch (Exception e) {
            return ApiResponse.build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }

    @GetMapping("getAllGenreActive")
    public ApiResponse getAllGener() {
        try {
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null,
                genreService.getAllByStatus());
        } catch (Exception e) {
            return ApiResponse
                .build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }

    @GetMapping("getSongByGenerId/{id}")
    public ApiResponse getSongByGenerId(@PathVariable Long id) {
        try {
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null,
                genreService.getSongByGener(id));
        } catch (Exception e) {
            return ApiResponse
                .build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }

    @GetMapping("getAlbumActive")
    public ApiResponse getAlbumActive() {
        try {
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null,
                albumService.getAllActive());
        } catch (Exception e) {
            return ApiResponse
                .build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }

    @GetMapping("getSongByAlbum/{id}")
    public ApiResponse getSongByAlbum(@PathVariable Long id) {
        try {
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null,
                albumService.getSongByAlbum(id));
        } catch (Exception e) {
            return ApiResponse
                .build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }

    @GetMapping("getAlbumById/{id}")
    public ApiResponse getAlbumById(@PathVariable Long id) {
        try {
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null,
                albumService.getById(id));
        } catch (Exception e) {
            return ApiResponse
                .build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }

    @GetMapping("getTopicActive")
    public ApiResponse getTopicActive() {
        try {
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null,
                topicService.getTopicActive());
        } catch (Exception e) {
            return ApiResponse
                .build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }

    @GetMapping("getTopicById/{id}")
    public ApiResponse getTopicById(@PathVariable Long id) {
        try {
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null,
                topicService.getTopicById(id));
        } catch (Exception e) {
            return ApiResponse
                .build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }

    @GetMapping("getSongByTopic/{id}")
    public ApiResponse getSongByTopic(@PathVariable Long id) {
        try {
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null,
                topicService.getSongByTopic(id));
        } catch (Exception e) {
            return ApiResponse
                .build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }

    @GetMapping("getAlbumByArtist/{id}")
    public ApiResponse getAlbumByArtist(@PathVariable Long id) {
        try {
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null,
                albumService.getAlbumByArtist(id));
        } catch (Exception e) {
            return ApiResponse
                .build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }

    @GetMapping("viewSong/{id}")
    public ApiResponse view(@PathVariable Long id) {
        try {
            songService.viewSong(id);
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null,null);
        } catch (Exception e) {
            return ApiResponse
                .build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }

    @GetMapping("getTopSongByCountry")
    public ApiResponse getTopSongByCountry(@RequestParam("topNumber") Integer topNumber,
        @RequestParam("countryId") Long countryId, @RequestParam("isMonth") boolean isMonth) {
        try {
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null, songService.getTopSongByCountry(topNumber, countryId, isMonth));
        } catch (Exception e) {
            return ApiResponse
                .build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }

    @GetMapping("getCountryActive")
    public ApiResponse getCountryActive() {
        try {
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null,countryService.getAllActive());
        } catch (Exception e) {
            return ApiResponse
                .build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }

    @GetMapping("getSongsByUserId/{id}")
    public ApiResponse getSongsByUserId(@PathVariable Long id) {
        try {
            List<Song> songs = songService.findAllByUserIdAndStatus(id, 1);
            if (songs.isEmpty()) {
                return ApiResponse.build(HttpServletResponse.SC_OK, true, null, new ArrayList<>());
            }
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null, songs);
        } catch (Exception e) {
            return ApiResponse
                    .build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }

    @DeleteMapping("revokeUpload/{id}")
    public ApiResponse revokeUpload(@PathVariable Long id) {
        try {
            Optional<Song> optionalSong = songService.findSongById(id);
            if (optionalSong.isPresent()) {
                if (optionalSong.get().getApproved() != 2) {
                    return ApiResponse.build(HttpServletResponse.SC_BAD_REQUEST, true, "Bài hát không trong trạng thái chờ", null);
                }
                songService.revokeSongUpload(id);
                return ApiResponse.build(HttpServletResponse.SC_OK, true, null, new ArrayList<>());
            } else {
                return ApiResponse.build(HttpServletResponse.SC_NOT_FOUND, true, "Không tìm thấy bài hát", null);
            }
        } catch (Exception e) {
            return ApiResponse
                    .build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }

    @GetMapping("findAllSlideshowActive")
    public ApiResponse findAllSlideshowActive() {
        try {
            List<SlideShow> slideShows = slideshowService.getAllByStatus();
            if (slideShows.isEmpty()) {
                return ApiResponse.build(HttpServletResponse.SC_OK, true, null, new ArrayList<>());
            }
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null, slideShows);
        } catch (Exception e) {
            return ApiResponse
                    .build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }

    @PostMapping("addSongToPlaylist")
    public ApiResponse addSongToPlaylist(@RequestBody SongPlaylist songPlaylist) {
        try {
            Boolean success = songPlaylistService.addSongToPlaylist(songPlaylist);
            if (success)
                return ApiResponse.build(HttpServletResponse.SC_OK, true, null, null);
            return ApiResponse.build(HttpServletResponse.SC_CONFLICT, true, null, "Bài hát đã tồn tại trong playlist");
        } catch (Exception e) {
            return ApiResponse
                .build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }

    @GetMapping("search")
    public ApiResponse searchAll(@RequestParam String name, @RequestParam boolean maxResult) {
        try {
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null, searchService.search(name, maxResult));
        } catch (Exception e) {
            return ApiResponse
                .build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }

    @PostMapping("addSongToQuenue")
    public ApiResponse addSongToPlaylist(@RequestBody List<SongDTO> songDTO) {
        try {
            return ApiResponse.build(HttpServletResponse.SC_CONFLICT, true, null, ConvertUltil.convertFromSong(songDTO, mstrUpload));
        } catch (Exception e) {
            return ApiResponse
                .build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }

    @PostMapping("uploadSong")
    public ApiResponse uploadSong(@RequestBody SongUploadDTO songUploadDTO, Authentication authentication) {
        try {
            songService.uploadSong(songUploadDTO, authentication);
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null, null);
        } catch (Exception e) {
            e.printStackTrace();
            return ApiResponse
                    .build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, false, e.getMessage(), null);
        }
    }


    @PostMapping("getAvatar")
    public ApiResponse getAvatar(@RequestBody String path) {
        try {
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null, ConvertUltil.convertBase64(path));
        } catch (Exception e) {
            return ApiResponse
                .build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }

}
