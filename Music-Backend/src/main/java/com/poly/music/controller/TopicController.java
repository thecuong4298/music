package com.poly.music.controller;

import com.poly.music.dto.ApiResponse;
import com.poly.music.dto.SaveTopicDTO;
import com.poly.music.entity.Topic;
import com.poly.music.service.TopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("music")
public class TopicController {

    @Autowired
    TopicService topicService;
    
    @GetMapping("getAllTopic")
    public ApiResponse findAllTopic() {
        try {
            List<Topic> topicList = topicService.getAllTopic();
            if (topicList == null || topicList.isEmpty()) {
                return ApiResponse.build(HttpServletResponse.SC_NO_CONTENT, true, null,new ArrayList<>());
            }
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null, topicService.getAllTopic());
        } catch (Exception e) {
            return ApiResponse.build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }

    @GetMapping("getTop3Topic")
    public ApiResponse getTop3Topic() {
        try {
            List<Topic> topics = topicService.get3TopicRandom();
            if (topics == null || topics.isEmpty()) {
                return ApiResponse.build(HttpServletResponse.SC_NO_CONTENT, true, null,new ArrayList<>());
            }
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null, topics);
        } catch (Exception e) {
            return ApiResponse.build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }
    
    @GetMapping("getTopicActive")
    public ApiResponse getTopicActive() {
    	try {
            List<Topic> topics = topicService.getTopicActive();
            if (topics == null || topics.isEmpty()) {
                return ApiResponse.build(HttpServletResponse.SC_NO_CONTENT, true, null,new ArrayList<>());
            }
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null, topics);
        } catch (Exception e) {
            return ApiResponse.build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }
    
    @GetMapping("getTopicByCondition")
    public ApiResponse getByCondition(@RequestParam(required = false) String topicName) {
        try {
            List<Topic> topicList = topicService.findByCondition(topicName);
            if (topicList == null || topicList.isEmpty()) {
                return ApiResponse.build(HttpServletResponse.SC_NO_CONTENT, true, null,new ArrayList<>());
            }
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null, topicList);
        } catch (Exception e) {
            return ApiResponse.build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }
    
    @GetMapping("getTopicById/{topicId}")
    public ApiResponse getTopicById(@PathVariable Long topicId) {
    	 try {
             return ApiResponse.build(HttpServletResponse.SC_OK, true, null, topicService.getTopicById(topicId));
         } catch (Exception e) {
             return ApiResponse.build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
         }
    }
    
    @PostMapping("saveTopic")
    public ApiResponse createOrUpdateTopic(@RequestBody SaveTopicDTO topic) {
        try {
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null,topicService.createOrUpdateTopic(topic.getTopic(), topic.getListSongId()));
        } catch (Exception e) {
            e.printStackTrace();
            return ApiResponse.build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }
    
    @DeleteMapping("deleteTopic/{topicId}")
    public ApiResponse deleteTopic(@PathVariable Long topicId) {
        try {
            Optional<Topic> optionalTopic = topicService.getTopicById(topicId);
            if (!optionalTopic.isPresent()) {
                return ApiResponse.build(HttpServletResponse.SC_NO_CONTENT, true, "Không tìm thấy giá trị",null);
            }
            topicService.deleteById(topicId);
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null, null);
        } catch (Exception e) {
            return ApiResponse.build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }
}
