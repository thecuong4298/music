package com.poly.music.controller;

import com.poly.music.dto.ApiResponse;
import com.poly.music.entity.SlideShow;
import com.poly.music.service.SlideshowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("slide")

public class SlideShowController {

    @Autowired
    private SlideshowService slideshowService;

    @GetMapping("getByCondition")
    public ApiResponse getByCondition(@RequestParam(required = false) String slideName) {
        try {
            List<SlideShow> slideList = slideshowService.findByCondition(slideName);
            if (slideList == null || slideList.isEmpty()) {
                return ApiResponse.build(HttpServletResponse.SC_NO_CONTENT, true, null,new ArrayList<>());
            }
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null, slideList);
        } catch (Exception e) {
            return ApiResponse.build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }


    @GetMapping("getAll")
    public ApiResponse findAllSilde() {
        try {
            List<SlideShow> slideList = slideshowService.getAllSlide();
            if (slideList == null || slideList.isEmpty()) {
                return ApiResponse.build(HttpServletResponse.SC_NO_CONTENT, true, null,new ArrayList<>());
            }
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null, slideshowService.getAllSlide());
        } catch (Exception e) {
            return ApiResponse.build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }

    @PostMapping("save")
    public ApiResponse createOrUpdateGenre(@RequestBody SlideShow slideShow) {
        try {
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null,slideshowService.createOrUpdateSlide(slideShow));
        } catch (Exception e) {
            return ApiResponse.build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }

    @GetMapping("getSlideById/{id}")
    public ApiResponse getGenreById(@PathVariable Long id) {
        try {
            Optional<SlideShow> optionalSlide = slideshowService.getSlideById(id);
            if (!optionalSlide.isPresent()) {
                return ApiResponse.build(HttpServletResponse.SC_NO_CONTENT, true, "Không tìm thấy giá trị",null);
            }
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null, optionalSlide.get());
        } catch (Exception e) {
            return ApiResponse.build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }

    @DeleteMapping("deleteSlide/{id}")
    public ApiResponse deleteGenre(@PathVariable Long id) {
        try {
            Optional<SlideShow> optionalSlide = slideshowService.getSlideById(id);
            if (!optionalSlide.isPresent()) {
                return ApiResponse.build(HttpServletResponse.SC_NO_CONTENT, true, "Không tìm thấy giá trị",null);
            }
            slideshowService.deleteById(id);
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null, null);
        } catch (Exception e) {
            return ApiResponse.build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }
//
//    @GetMapping("get7SlideRandom")
//    public ApiResponse get7GenreRandom() {
//        try {
//            List<SlideShow> SlideList = slideShowService.get7SlideRandom();
//            if (SlideList == null || SlideList.isEmpty()) {
//                return ApiResponse.build(HttpServletResponse.SC_NO_CONTENT, true, "Không tìm thấy giá trị",null);
//            }
//            return ApiResponse.build(HttpServletResponse.SC_OK, true, null, SlideList);
//        } catch (Exception e) {
//            return ApiResponse.build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
//        }
    }





