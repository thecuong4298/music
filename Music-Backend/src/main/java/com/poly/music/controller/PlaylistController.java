package com.poly.music.controller;

import com.poly.music.dto.ApiResponse;
import com.poly.music.entity.Playlist;
import com.poly.music.entity.Song;
import com.poly.music.entity.SongPlaylist;
import com.poly.music.repo.SongPlaylistRepo;
import com.poly.music.service.PlaylistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("music")
public class PlaylistController {

    @Autowired
    PlaylistService playlistService;

    @Autowired
    SongPlaylistRepo songPlaylistRepo;

    @GetMapping("getPlaylistById/{id}")
    public ApiResponse getGenreById(@PathVariable Long id) {
        try {
            List<Playlist> playlists = playlistService.getPlaylistByUserId(id, 1);
            if (playlists == null || playlists.isEmpty()) {
                return ApiResponse.build(HttpServletResponse.SC_NO_CONTENT, true, null,new ArrayList<>());
            }
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null, playlists);
        } catch (Exception e) {
            return ApiResponse.build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }

    @GetMapping("getSongInPlaylist/{id}")
    public ApiResponse getSongInPlaylist(@PathVariable Long id) {
        try {
            List<Song> playlists = playlistService.findSongInPlaylistById(id);
            if (playlists == null || playlists.isEmpty()) {
                return ApiResponse.build(HttpServletResponse.SC_NO_CONTENT, true, null,new ArrayList<>());
            }
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null, playlists);
        } catch (Exception e) {
            return ApiResponse.build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }

    @PostMapping("createPlaylist")
    public ApiResponse save(@RequestBody Playlist playlist) {
        try {
            Playlist newPlaylist =  playlistService.save(playlist);
            if (newPlaylist == null) {
                return ApiResponse.build(HttpServletResponse.SC_BAD_REQUEST, true, "Thất bại", null);
            }
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null, newPlaylist);
        } catch (Exception e) {
            return ApiResponse.build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }

    @GetMapping("getPlaylist/{id}")
    public ApiResponse getPlaylistById(@PathVariable Long id) {
        try {
            Optional<Playlist> optionalPlaylist = playlistService.getPlaylistById(id);
            if (!optionalPlaylist.isPresent()) {
                return ApiResponse.build(HttpServletResponse.SC_NO_CONTENT, true, "Không tìm thấy giá trị",null);
            }
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null, optionalPlaylist.get());
        } catch (Exception e) {
            return ApiResponse.build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }

    @DeleteMapping("deletePlaylist/{id}")
    public ApiResponse deleteGenre(@PathVariable Long id) {
        try {
            Optional<Playlist> optionalPlaylist = playlistService.getPlaylistById(id);
            if (!optionalPlaylist.isPresent()) {
                return ApiResponse.build(HttpServletResponse.SC_NO_CONTENT, true, "Không tìm thấy giá trị",null);
            }
            playlistService.deleteById(id);
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null, null);
        } catch (Exception e) {
            return ApiResponse.build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }

    @DeleteMapping("deleteSongInPlaylist")
    public ApiResponse deleteSongInPlaylist(@RequestParam Long idPlaylist, @RequestParam Long idSong) {
        try {
            List<SongPlaylist> songPlaylists = songPlaylistRepo.findAllByPlaylistIdAndSongId(idPlaylist, idSong);
            if (songPlaylists == null || songPlaylists.isEmpty()) {
                return ApiResponse.build(HttpServletResponse.SC_BAD_REQUEST, true, "Xóa không thành công",null);
            }
            playlistService.deleteSongInPlaylist(idPlaylist, idSong);
            return ApiResponse.build(HttpServletResponse.SC_OK, true, null, null);
        } catch (Exception e) {
            return ApiResponse.build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
        }
    }

}
