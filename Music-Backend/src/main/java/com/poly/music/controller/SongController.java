package com.poly.music.controller;

import com.poly.music.dto.SaveSongDTO;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import com.poly.music.dto.SongUploadDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.poly.music.dto.ApiResponse;
import com.poly.music.entity.Song;
import com.poly.music.service.SongService;

@RestController
@RequestMapping("music")
public class SongController {

  @Autowired SongService songService;

  @GetMapping("getAllSong")
  public ApiResponse findAllSong() {
    try {
      List<Song> listSong = songService.getAllSong();
      if (listSong == null || listSong.isEmpty()) {
        return ApiResponse.build(HttpServletResponse.SC_NO_CONTENT, true, null, new ArrayList<>());
      }
      return ApiResponse.build(HttpServletResponse.SC_OK, true, null, songService.getAllSong());
    } catch (Exception e) {
      return ApiResponse.build(
          HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
    }
  }

  @GetMapping("getSongByCondition")
  public ApiResponse getByCondition(@RequestParam(required = false) String songName) {
    try {
      List<Song> songList = songService.findByCondition(songName);
      if (songList == null || songList.isEmpty()) {
        return ApiResponse.build(HttpServletResponse.SC_NO_CONTENT, true, null, new ArrayList<>());
      }
      return ApiResponse.build(HttpServletResponse.SC_OK, true, null, songList);
    } catch (Exception e) {
      return ApiResponse.build(
          HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
    }
  }

  @PostMapping("saveSong")
  public ApiResponse createOrUpdateSong(@RequestBody SaveSongDTO song) {
    try {
      return ApiResponse
          .build(HttpServletResponse.SC_OK, true, null, songService.createOrUpdateSong(song.getSong(), song.getListArtistId()));
    } catch (Exception e) {
      return ApiResponse.build(
          HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
    }
  }

  @DeleteMapping("deleteSong/{songId}")
  public ApiResponse deleteSong(@PathVariable Long songId) {
    try {
      Optional<Song> optionalSong = songService.findSongById(songId);
      if (!optionalSong.isPresent()) {
        return ApiResponse.build(
            HttpServletResponse.SC_NO_CONTENT, true, "Không tìm thấy giá trị", null);
      }
      songService.deleteById(songId);
      return ApiResponse.build(HttpServletResponse.SC_OK, true, null, null);
    } catch (Exception e) {
      return ApiResponse.build(
          HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
    }
  }

  @GetMapping("getSongById/{id}")
  public ApiResponse getSongById(@PathVariable Long id) {
    try {
      return ApiResponse.build(HttpServletResponse.SC_OK, true, null, songService.getById(id));
    } catch (Exception e) {
      return ApiResponse.build(
          HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
    }
  }

  @GetMapping("getAllSongUploadByCustomer")
  public ApiResponse getAllSongUploadByCustomer() {
    try {
      List<Song> songs = songService.getAllSongUploadByCustomer();
      if (songs != null && !songs.isEmpty()) {
        return ApiResponse.build(HttpServletResponse.SC_OK, true, null, songs);
      } else {
        return ApiResponse.build(HttpServletResponse.SC_OK, true, null, new ArrayList<>());
      }
    } catch (Exception e) {
      return ApiResponse.build(
          HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
    }
  }

  @GetMapping("getDetailSongUpload/{id}")
  public ApiResponse getDetailSongUpload(@PathVariable Long id) {
    try {
      SongUploadDTO songUploadDTO = songService.getDetailSongUpload(id);
      if (songUploadDTO != null) {
        return ApiResponse.build(HttpServletResponse.SC_OK, true, null, songUploadDTO);
      } else {
        return ApiResponse.build(HttpServletResponse.SC_NOT_FOUND, true, null, null);
      }
    } catch (Exception e) {
      return ApiResponse.build(
          HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
    }
  }

  @PostMapping("acceptOrRejectSongUpload")
  public ApiResponse acceptOrRejectSongUpload(@RequestParam Long songId, @RequestParam Integer type) {
    try {
      songService.acceptOrRejectSongUpload(songId, type);
      return ApiResponse.build(HttpServletResponse.SC_OK, true, null, null);
    } catch (Exception e) {
      return ApiResponse.build(
          HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
    }
  }

  @GetMapping("getArtistBySongId/{id}")
  public ApiResponse getArtistBySongId(@PathVariable Long id) {
    try {
      return ApiResponse.build(HttpServletResponse.SC_OK, true, null, songService.getArtistBySongId(id));
    } catch (Exception e) {
      return ApiResponse
          .build(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true, e.getMessage(), null);
    }
  }
}
