package com.poly.music.entity;

import com.poly.music.ultil.ConvertUltil;
import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "album")
public class Album {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "path_avatar")
    private String path;

    @Column(name = "release_date")
    private Date releaseDate;

    @Column(name = "status")
    private int status;

    private String base64;

    public Album(Object... ob) {
        this.id = ConvertUltil.parseObjectToLong(ob[0]);
        this.name = ConvertUltil.parseObjectToString(ob[1]);
        this.base64 = ConvertUltil.convertBase64(ob[2].toString());
    }

    public Album() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getBase64() {
        return base64;
    }

    public void setBase64(String base64) {
        this.base64 = base64;
    }
}
