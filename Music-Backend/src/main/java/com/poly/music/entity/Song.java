package com.poly.music.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "song")
public class Song {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "name")
  private String name;

  @Column(name = "description")
  private String description;

  @Column(name = "path")
  private String path;

  @Column(name = "user_id")
  private Long userId;

  @Column(name = "album_id")
  private Long albumId;

  @Column(name = "path_avatar")
  private String pathAvatar;

  @Column(name = "genre_id")
  private Long genreId;

  @Column(name = "country_id")
  private Long countryId;

  @Column(name = "approved")
  private int approved;

  @Column(name = "release_date")
  private Date releaseDate;

  @Column(name = "status")
  private Integer status;

  @Column(name = "time")
  private Integer time;

  private String base64;

  private String songBase64;

  @Column(name = "lyric")
  private String lyric;

  @Column(name = "view")
  private Long view;

  @Column(name = "view_month")
  private Long viewMonth;

  @Column(name = "view_week")
  private Long viewWeek;

  @Column(name = "type")
  private Integer type;

  public String getSongBase64() {
    return songBase64;
  }

  public void setSongBase64(String songBase64) {
    this.songBase64 = songBase64;
  }

  public Long getViewMonth() {
    return viewMonth;
  }

  public void setViewMonth(Long viewMonth) {
    this.viewMonth = viewMonth;
  }

  public Long getViewWeek() {
    return viewWeek;
  }

  public void setViewWeek(Long viewWeek) {
    this.viewWeek = viewWeek;
  }

  public Long getView() {
    return view;
  }

  public void setView(Long view) {
    this.view = view;
  }

  public String getLyric() {
    return lyric;
  }

  public void setLyric(String lyric) {
    this.lyric = lyric;
  }

  public String getBase64() {
    return base64;
  }

  public void setBase64(String base64) {
    this.base64 = base64;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public Long getAlbumId() {
    return albumId;
  }

  public void setAlbumId(Long albumId) {
    this.albumId = albumId;
  }

  public String getPathAvatar() {
    return pathAvatar;
  }

  public void setPathAvatar(String pathAvatar) {
    this.pathAvatar = pathAvatar;
  }

  public Long getGenreId() {
    return genreId;
  }

  public void setGenreId(Long genreId) {
    this.genreId = genreId;
  }

  public Long getCountryId() {
    return countryId;
  }

  public void setCountryId(Long countryId) {
    this.countryId = countryId;
  }

  public int getApproved() {
    return approved;
  }

  public void setApproved(int approved) {
    this.approved = approved;
  }

  public Date getReleaseDate() {
    return releaseDate;
  }

  public void setReleaseDate(Date releaseDate) {
    this.releaseDate = releaseDate;
  }

  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }

  public Integer getTime() {
    return time;
  }

  public void setTime(Integer time) {
    this.time = time;
  }

  public Integer getType() {
    return type;
  }

  public void setType(Integer type) {
    this.type = type;
  }
}
