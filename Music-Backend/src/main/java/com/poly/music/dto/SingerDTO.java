package com.poly.music.dto;

import java.util.List;

public class SingerDTO {

  private String singer;
  private List<Long> id;

  public SingerDTO() {
  }

  public String getSinger() {
    return singer;
  }

  public void setSinger(String singer) {
    this.singer = singer;
  }

  public List<Long> getId() {
    return id;
  }

  public void setId(List<Long> id) {
    this.id = id;
  }
}
