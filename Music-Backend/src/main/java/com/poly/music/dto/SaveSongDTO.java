package com.poly.music.dto;

import com.poly.music.entity.Song;
import java.util.List;

public class SaveSongDTO {
  Song song;

  List<Long> listArtistId;

  public SaveSongDTO() {
  }

  public Song getSong() {
    return song;
  }

  public void setSong(Song song) {
    this.song = song;
  }

  public List<Long> getListArtistId() {
    return listArtistId;
  }

  public void setListArtistId(List<Long> listArtistId) {
    this.listArtistId = listArtistId;
  }
}
