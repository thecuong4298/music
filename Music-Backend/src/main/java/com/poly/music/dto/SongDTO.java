package com.poly.music.dto;

import com.poly.music.service.ArtistService;
import com.poly.music.ultil.ConvertUltil;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

public class SongDTO {

  @Autowired
  private ArtistService artistService;

  private Long id;

  private String name;

  private String description;

  private String path;

  private Long userId;

  private Long albumId;

  private String pathAvatar;

  private Long genreId;

  private Long countryId;

  private int approved;

  private Date releaseDate;

  private Integer status;

  private Integer time;

  private String base64;

  private String lyric;

  private Long view;

  private String pathAvatarp;

  public String getPathAvatarp() {
    return pathAvatarp;
  }

  public void setPathAvatarp(String pathAvatarp) {
    this.pathAvatarp = pathAvatarp;
  }

  public Long getView() {
    return view;
  }

  public void setView(Long view) {
    this.view = view;
  }

  private String singer;

  public SongDTO() {
  }

  public SongDTO(Object... fields) {
    this.id = ConvertUltil.parseObjectToLong(fields[0]);
    this.time = ConvertUltil.parseObjectToInteger(fields[3]);
    this.name = ConvertUltil.parseObjectToString(fields[1]);
    this.path = ConvertUltil.parseObjectToString(fields[2]);
//    this.singer = artistService.getArtistBySongId(this.id);
    this.view = ConvertUltil.parseObjectToLong(fields[6]);
    this.pathAvatar = ConvertUltil.parseObjectToString(fields[7]);
    this.base64 = ConvertUltil.convertBase64(this.pathAvatar);
    this.lyric = ConvertUltil.parseObjectToString(fields[8]);
  }

  public String getSinger() {
    return singer;
  }

  public void setSinger(String singer) {
    this.singer = singer;
  }

  public String getLyric() {
    return lyric;
  }

  public void setLyric(String lyric) {
    this.lyric = lyric;
  }

  public String getBase64() {
    return base64;
  }

  public void setBase64(String base64) {
    this.base64 = base64;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public Long getAlbumId() {
    return albumId;
  }

  public void setAlbumId(Long albumId) {
    this.albumId = albumId;
  }

  public String getPathAvatar() {
    return pathAvatar;
  }

  public void setPathAvatar(String pathAvatar) {
    this.pathAvatar = pathAvatar;
  }

  public Long getGenreId() {
    return genreId;
  }

  public void setGenreId(Long genreId) {
    this.genreId = genreId;
  }

  public Long getCountryId() {
    return countryId;
  }

  public void setCountryId(Long countryId) {
    this.countryId = countryId;
  }

  public int getApproved() {
    return approved;
  }

  public void setApproved(int approved) {
    this.approved = approved;
  }

  public Date getReleaseDate() {
    return releaseDate;
  }

  public void setReleaseDate(Date releaseDate) {
    this.releaseDate = releaseDate;
  }

  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }

  public Integer getTime() {
    return time;
  }

  public void setTime(Integer time) {
    this.time = time;
  }
}
