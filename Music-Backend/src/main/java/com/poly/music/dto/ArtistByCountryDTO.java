package com.poly.music.dto;

import com.poly.music.entity.Artist;
import com.poly.music.entity.Country;
import java.util.List;

public class ArtistByCountryDTO {

  private Country country;
  private List<Artist> artistList;
  private List<Slide> slide;

  public List<Slide> getSlide() {
    return slide;
  }

  public void setSlide(List<Slide> slide) {
    this.slide = slide;
  }

  public ArtistByCountryDTO(Country country, List<Artist> artistList) {
    this.country = country;
    this.artistList = artistList;
  }

  public ArtistByCountryDTO() {

  }

  public Country getCountry() {
    return country;
  }

  public void setCountry(Country country) {
    this.country = country;
  }

  public List<Artist> getArtistList() {
    return artistList;
  }

  public void setArtistList(List<Artist> artistList) {
    this.artistList = artistList;
  }
}
