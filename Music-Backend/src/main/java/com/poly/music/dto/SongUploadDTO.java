package com.poly.music.dto;

import java.util.Date;
import java.util.List;

public class SongUploadDTO {

    private Long songId;
    private String songName;
    private String artistName;
    private int artistGender;
    private Long artistCountryId;
    private String songGenreName;
    private Long songGenreId;
    private String songCountryName;
    private Long songCountryId;
    private String songDescription;
    private String pathAvatar;
    private String pathTrack;
    private List<Long> artistId;
    private String base64Track;
    private String base64Avatar;
    private String userName;
    private Date dateOfUpload;

    public String getSongName() {
        return songName;
    }

    public void setSongName(String songName) {
        this.songName = songName;
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public int getArtistGender() {
        return artistGender;
    }

    public void setArtistGender(int artistGender) {
        this.artistGender = artistGender;
    }

    public Long getArtistCountryId() {
        return artistCountryId;
    }

    public void setArtistCountryId(Long artistCountryId) {
        this.artistCountryId = artistCountryId;
    }

    public Long getSongGenreId() {
        return songGenreId;
    }

    public void setSongGenreId(Long songGenreId) {
        this.songGenreId = songGenreId;
    }

    public Long getSongCountryId() {
        return songCountryId;
    }

    public void setSongCountryId(Long songCountryId) {
        this.songCountryId = songCountryId;
    }

    public String getSongDescription() {
        return songDescription;
    }

    public void setSongDescription(String songDescription) {
        this.songDescription = songDescription;
    }

    public String getPathAvatar() {
        return pathAvatar;
    }

    public void setPathAvatar(String pathAvatar) {
        this.pathAvatar = pathAvatar;
    }

    public String getPathTrack() {
        return pathTrack;
    }

    public void setPathTrack(String pathTrack) {
        this.pathTrack = pathTrack;
    }

    public List<Long> getArtistId() {
        return artistId;
    }

    public void setArtistId(List<Long> artistId) {
        this.artistId = artistId;
    }

    public String getSongGenreName() {
        return songGenreName;
    }

    public void setSongGenreName(String songGenreName) {
        this.songGenreName = songGenreName;
    }

    public String getSongCountryName() {
        return songCountryName;
    }

    public void setSongCountryName(String songCountryName) {
        this.songCountryName = songCountryName;
    }

    public Long getSongId() {
        return songId;
    }

    public void setSongId(Long songId) {
        this.songId = songId;
    }

    public String getBase64Track() {
        return base64Track;
    }

    public void setBase64Track(String base64Track) {
        this.base64Track = base64Track;
    }

    public String getBase64Avatar() {
        return base64Avatar;
    }

    public void setBase64Avatar(String base64Avatar) {
        this.base64Avatar = base64Avatar;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Date getDateOfUpload() {
        return dateOfUpload;
    }

    public void setDateOfUpload(Date dateOfUpload) {
        this.dateOfUpload = dateOfUpload;
    }
}
