package com.poly.music.dto;

import com.poly.music.entity.Topic;
import java.util.List;

public class SaveTopicDTO {

  Topic topic;

  List<Long> listSongId;

  public SaveTopicDTO() {
  }

  public Topic getTopic() {
    return topic;
  }

  public void setTopic(Topic topic) {
    this.topic = topic;
  }

  public List<Long> getListSongId() {
    return listSongId;
  }

  public void setListSongId(List<Long> listSongId) {
    this.listSongId = listSongId;
  }
}
