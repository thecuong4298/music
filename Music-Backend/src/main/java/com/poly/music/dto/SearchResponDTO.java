package com.poly.music.dto;

import com.poly.music.entity.Album;
import com.poly.music.entity.Artist;
import java.util.List;

public class SearchResponDTO {

 private List<SongDTO> listSong;

 private List<Album> listAlbum;

 private List<Artist> artist;

  public SearchResponDTO(List<SongDTO> listSong,
      List<Album> listAlbum, List<Artist> artist) {
    this.listSong = listSong;
    this.listAlbum = listAlbum;
    this.artist = artist;
  }

  public List<SongDTO> getListSong() {
    return listSong;
  }

  public void setListSong(List<SongDTO> listSong) {
    this.listSong = listSong;
  }

  public List<Album> getListAlbum() {
    return listAlbum;
  }

  public void setListAlbum(List<Album> listAlbum) {
    this.listAlbum = listAlbum;
  }

  public List<Artist> getArtist() {
    return artist;
  }

  public void setArtist(List<Artist> artist) {
    this.artist = artist;
  }
}
