package com.poly.music.service.serviceImpl;

import com.poly.music.dto.SongDTO;
import com.poly.music.entity.Album;
import com.poly.music.entity.Topic;
import com.poly.music.repo.AlbumRepo;
import com.poly.music.repo.SongRepo;
import com.poly.music.service.AlbumService;

import com.poly.music.ultil.ConvertUltil;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AlbumServiceImpl implements AlbumService {

  @Autowired
  private AlbumRepo albumRepo;

  @Autowired
  private SongRepo songRepo;

  @Override
  public List<Album> getAllActive() {
    List<Album> list = albumRepo.getAllByStatus(1);
    for (Album artist : list) {
      artist.setBase64(ConvertUltil.convertBase64(artist.getPath()));
    }
    return list;
  }

  @Override
  public Album getById(Long id) {
    Optional<Album> optional = albumRepo.findById(id);
    if(optional.isPresent()){
      optional.get().setBase64(ConvertUltil.convertBase64(optional.get().getPath()));
    }
    return optional.get();
  }

  @Override
  public List<Album> get4AlbumRandom() {
    List<Album> list = albumRepo.get4AlbumRandom();
    for (Album artist : list) {
      artist.setBase64(ConvertUltil.convertBase64(artist.getPath()));
    }
    return list;
  }

  @Override
  public List<Album> getAllAlbum() {
    List<Album> listAritst = albumRepo.findAll();
    for (Album artist : listAritst) {
      artist.setBase64(ConvertUltil.convertBase64(artist.getPath()));
    }
    return listAritst;
  }

  @Override
  public Album createOrUpdateAlbum(Album album) {
    return albumRepo.save(album);
  }

  @Override
  public void deleteById(Long id) {
    albumRepo.deleteById(id);

  }

  @Override
  public Optional<Album> getAlbumById(Long id) {
    return albumRepo.findById(id);
  }

  @Override
  public List<Album> findByCondition(String albumName) {
    List<Album> list;
    if (albumName == null || "".equals(albumName.trim())) {
      list = albumRepo.findAll();
    } else {
      list = albumRepo.findAllByNameContaining(albumName.trim());
    }
    for (Album album : list) {
      album.setBase64(ConvertUltil.convertBase64(album.getPath()));
    }
    return list;
  }

  @Override
  public List<SongDTO> getSongByAlbum(Long albumId) {
    return albumRepo.getSongBy("album_id", albumId);
  }

  @Override
  public List<Album> getAlbumByArtist(Long artistId) {
    return albumRepo.getByArtist(artistId);
  }

}
