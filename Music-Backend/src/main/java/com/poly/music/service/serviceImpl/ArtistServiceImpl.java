package com.poly.music.service.serviceImpl;

import com.poly.music.dto.ArtistByCountryDTO;
import com.poly.music.dto.Slide;
import com.poly.music.dto.SongDTO;
import com.poly.music.entity.Artist;
import com.poly.music.entity.Country;
import com.poly.music.entity.Song;
import com.poly.music.entity.SongArtist;
import com.poly.music.repo.ArtistRepo;
import com.poly.music.repo.CountryRepo;
import com.poly.music.repo.SongArtistRepo;
import com.poly.music.service.ArtistService;
import com.poly.music.ultil.ConvertUltil;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class ArtistServiceImpl implements ArtistService {

  @Value("${pathUpload}")
  private String mstrUpload;

  @Autowired
  private ArtistRepo artistRepo;

  @Autowired
  private CountryRepo countryRepo;

  @Autowired
  private SongArtistRepo songArtistRepo;

  @Override
  public List<Artist> getAllArtistActive() {
    return artistRepo.getAllByStatus(1);
  }

  @Override
  public List<ArtistByCountryDTO> getAllArtistByCountry() {
    List<Country> countryList = countryRepo.getAllByStatus(1);
    List<ArtistByCountryDTO> list = new ArrayList<>();
    for (Country country : countryList) {
      ArtistByCountryDTO artistForCountryDTO = new ArtistByCountryDTO();
      List<Slide> slide = new ArrayList<>();
      artistForCountryDTO.setCountry(country);
      List<Artist> listAritst = artistRepo.getAllByCountryIdAndStatus(country.getId(), 1);
      for (Artist artist : listAritst) {
        artist.setBase64(ConvertUltil.convertBase64(artist.getPath()));
      }
      artistForCountryDTO.setArtistList(listAritst);
      list.add(artistForCountryDTO);
    }
    return list;
  }

  @Override
  public List<SongDTO> getSongByArtist(Long artistId) {
    return artistRepo.getSongByArtist(artistId);
  }

  @Override
  public List<Artist> getTop4Artist() {
    List<Artist> listAritst = artistRepo.getTop4Artist();
    for (Artist artist : listAritst) {
      artist.setBase64(ConvertUltil.convertBase64(artist.getPath()));
    }
    return listAritst;
  }

  public Artist getArtistById(Long artistId) {
    Optional<Artist> optional = artistRepo.getById(artistId);
    if (optional.isPresent()) {
      Artist artist = optional.get();
      artist.setBase64(ConvertUltil.convertBase64(artist.getPath()));
      return optional.get();
    }
    return new Artist();
  }

  @Override
  public List<Artist> getAllArtist() {
    List<Artist> listAritst = artistRepo.findAll();
    for (Artist artist : listAritst) {
      artist.setBase64(ConvertUltil.convertBase64(artist.getPath()));
    }
    return listAritst;
  }

  @Override
  public Artist createOrUpdateArtist(Artist artist) {
    artist.setCountLike(0L);
    return artistRepo.save(artist);
  }

  @Override
  public Artist getById(Long id) {
    Optional<Artist> optional = artistRepo.findById(id);
    return optional.orElseGet(Artist::new);
  }

  @Override
  public Optional<Artist> getArtistsById(Long id) {
    return artistRepo.findById(id);
  }

  @Override
  public void deleteById(Long id) {
    artistRepo.deleteById(id);

  }

  @Override
  public List<Artist> findByCondition(String artistName) {
    if (artistName == null || "".equals(artistName.trim())) {
      return artistRepo.findAll();
    }
    return artistRepo.findAllByNameContaining(artistName.trim());
  }

  @Override
  public String getArtistBySongId(Long songId) {
    List<SongArtist> list = songArtistRepo.getAllBySongIdAndStatus(songId, 1);
    String singer = "";
    for (SongArtist songArtist : list) {
      Optional<Artist> optional = artistRepo.getById(songArtist.getArtistId());
      if (optional.isPresent()) {
        singer = singer + optional.get().getName() + ", ";
      }
    }
    return singer.substring(0, singer.length() - 2);
  }
}
