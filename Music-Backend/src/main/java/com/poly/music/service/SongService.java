package com.poly.music.service;

import com.poly.music.dto.SingerDTO;
import com.poly.music.dto.SongDTO;
import com.poly.music.dto.SongUploadDTO;
import com.poly.music.entity.Song;
import com.poly.music.entity.SongArtist;
import org.springframework.security.core.Authentication;

import java.util.List;
import java.util.Optional;

public interface SongService {

  void viewSong(Long songId);

  List<SongDTO> getTopSongByCountry(Integer topNumber, Long countryId, boolean isMonth);

  List<Song> findAllByUserIdAndStatus(Long userId, Integer status);

  void revokeSongUpload(Long songId);

  Optional<Song> findSongById(Long songId);

  List<Song> getAllSong();

  void uploadSong(SongUploadDTO songUploadDTO, Authentication authentication);

  Song createOrUpdateSong(Song song, List<Long> singerId);

  void deleteById(Long id);

  List<Song> findByCondition(String songName);

  Song getById(Long id);

  SingerDTO getArtistBySongId(Long id);

  List<Song> getAllSongUploadByCustomer();

  SongUploadDTO getDetailSongUpload(Long songId);

  void acceptOrRejectSongUpload(Long id, Integer approved);

}
