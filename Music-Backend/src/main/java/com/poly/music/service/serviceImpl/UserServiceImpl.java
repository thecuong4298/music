package com.poly.music.service.serviceImpl;

import com.poly.music.entity.SlideShow;
import com.poly.music.entity.User;
import com.poly.music.repo.UserRepo;
import com.poly.music.service.UserService;
import com.poly.music.ultil.ConvertUltil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

  @Autowired private UserRepo userRepo;

  @Override
  public List<User> getAllUser() {

    List<User> list = userRepo.findAll();
    for (User user : list) {
      user.setBase64(ConvertUltil.convertBase64(user.getPathAvatar()));
    }
    return list;
  }

  @Override
  public User save(User user) {
    if (user.getId() != null) {
      if (user.getPassword() == null) {
        user.setPassword(userRepo.findById(user.getId()).get().getPassword());
      }
      user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
    }
    User user1 = userRepo.save(user);
    return user1;
  }

  @Override
  public List<User> findByCondition(String userName) {
    List<User> list;
    if (userName == null || "".equals(userName.trim())) {
      list = userRepo.findAll();
    } else {
      list = userRepo.findAllByUsernameContaining(userName.trim());
    }
    for (User user : list) {
      user.setBase64(ConvertUltil.convertBase64(user.getPathAvatar()));
    }
    return list;
  }

  @Override
  public Optional<User> getUserById(Long id) {
    Optional<User> optional = userRepo.findById(id);
    if (optional.isPresent()) {
      optional
          .get()
          .setBase64(ConvertUltil.convertBase64(optional.get().getPathAvatar()));
    }
    return optional;
  }

  @Override
  public void deleteById(Long id) {
    userRepo.deleteById(id);
  }

  @Override
  public List<User> getAllByStatus() {
    return userRepo.getAllByStatus(1);
  }

  @Override
  public List<User> get7UserRandom() {
    return userRepo.get7UserRandom();
  }

  @Override
  public Optional<User> findByUsername(String username) {
    return userRepo.findByUsername(username);
  }

  public static void main(String[] args) {


    System.out.println("===================================================");

    long millis = System.currentTimeMillis();
    System.out.println("Now.........:" + millis);



    Calendar cal1 = Calendar.getInstance();
    cal1.set(Calendar.HOUR_OF_DAY, 0); // ! clear would not reset the hour of day !
    cal1.clear(Calendar.MINUTE);
    cal1.clear(Calendar.SECOND);
    cal1.clear(Calendar.MILLISECOND);
//
//// get start of the month
    cal1.set(Calendar.DAY_OF_MONTH, 1);
    System.out.println("Start of the month:       " + cal1.getTime());
    System.out.println("... in milliseconds:      " + cal1.getTimeInMillis());

// get start of the next month
    cal1.add(Calendar.MONTH, 1);
    System.out.println("Start of the next month:  " + cal1.getTime());
    System.out.println("... in milliseconds:      " + cal1.getTimeInMillis());
  }
}
