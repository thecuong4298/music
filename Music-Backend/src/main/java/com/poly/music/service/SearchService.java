package com.poly.music.service;

import com.poly.music.dto.SearchResponDTO;

public interface SearchService {

SearchResponDTO search(String search, boolean maxResult);

}
