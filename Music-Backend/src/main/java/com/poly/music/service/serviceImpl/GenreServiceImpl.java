package com.poly.music.service.serviceImpl;

import com.poly.music.dto.SongDTO;
import com.poly.music.entity.Genre;
import com.poly.music.entity.Song;
import com.poly.music.repo.AlbumRepo;
import com.poly.music.repo.GenreRepo;
import com.poly.music.repo.SongRepo;
import com.poly.music.service.GenreService;
import com.poly.music.ultil.ConvertUltil;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class GenreServiceImpl implements GenreService {

  @Value("${pathUpload}")
  private String mstrUpload;

  @Autowired
  GenreRepo genreRepo;

  @Autowired
  SongRepo songRepo;

  @Autowired
  AlbumRepo albumRepo;


  @Override
  public List<Genre> getAllGenre() {
    List<Genre> list = genreRepo.findAll();
    for (Genre genre : list) {
      genre.setBase64(ConvertUltil.convertBase64(genre.getPath()));
    }
    return list;
  }

  @Override
  public List<Genre> findByCondition(String genreName) {
    List<Genre> list;
    if (genreName == null || "".equals(genreName.trim())) {
      list = genreRepo.findAll();
    } else {
      list = genreRepo.findAllByNameContaining(genreName.trim());
    }
    for (Genre genre : list) {
      genre.setBase64(ConvertUltil.convertBase64(genre.getPath()));
    }
    return list;
  }

  @Override
  public Genre createOrUpdateGenre(Genre genre) {
    return genreRepo.save(genre);
  }

  @Override
  public Optional<Genre> getGenreById(Long id) {
    Optional<Genre> optional = genreRepo.findById(id);
    if (optional.isPresent()) {
      optional.get().setBase64(ConvertUltil.convertBase64(optional.get().getPath()));
    }
    return optional;
  }

  @Override
  public void deleteById(Long id) {
    genreRepo.deleteById(id);
  }

  @Override
  public List<Genre> getAllByStatus() {
    List<Genre> list = genreRepo.getAllByStatus(1);
    for (Genre genre : list) {
      genre.setBase64(ConvertUltil.convertBase64(genre.getPath()));
    }
    return list;
  }

  @Override
  public List<SongDTO> getSongByGener(Long generId) {
    return albumRepo.getSongBy("genre_id", generId);
  }

  public List<Genre> get7GenreRandom() {
    List<Genre> list = genreRepo.get7GenreRandom();
    for (Genre genre : list) {
      genre.setBase64(ConvertUltil.convertBase64(genre.getPath()));
    }
    return list;
  }


}
