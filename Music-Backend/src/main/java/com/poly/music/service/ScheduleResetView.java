package com.poly.music.service;

import com.poly.music.entity.AuthenReissuePassword;
import com.poly.music.repo.SongRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.Calendar;
import java.util.List;

@Configuration
@EnableScheduling
public class ScheduleResetView {

  @Autowired SongRepo songRepo;

  @Scheduled(fixedDelay = 5000)
  public void scheduleResetViewInWeek() {
    Calendar cal = Calendar.getInstance();
    cal.set(Calendar.HOUR_OF_DAY, 0); // ! clear would not reset the hour of day !
    cal.clear(Calendar.MINUTE);
    cal.clear(Calendar.SECOND);
    cal.clear(Calendar.MILLISECOND);

    // get start of this week in milliseconds
    cal.set(Calendar.DAY_OF_WEEK, cal.getFirstDayOfWeek());
    cal.add(Calendar.DATE, 1);
    Long firstMondayOfWeek = cal.getTimeInMillis();

    cal.add(Calendar.SECOND, 20);
    Long timeToReset = cal.getTimeInMillis();
    long now = System.currentTimeMillis();

    if (firstMondayOfWeek < now && timeToReset > now) {
      this.songRepo.resetViewWeekInSong();
    }
  }

  @Scheduled(fixedDelay = 5000)
  public void scheduleResetViewInMonth() {
    Calendar cal = Calendar.getInstance();
    cal.set(Calendar.HOUR_OF_DAY, 0); // ! clear would not reset the hour of day !
    cal.clear(Calendar.MINUTE);
    cal.clear(Calendar.SECOND);
    cal.clear(Calendar.MILLISECOND);

    cal.set(Calendar.DAY_OF_MONTH, 1);

    cal.add(Calendar.MONTH, 1);
    long firstDayOfMonth = cal.getTimeInMillis();

    cal.add(Calendar.SECOND, 20);
    long timeToReset = cal.getTimeInMillis();

    long now = System.currentTimeMillis();

    if (firstDayOfMonth < now && timeToReset > now) {
      this.songRepo.resetViewMonthInSong();
    }
  }
}
