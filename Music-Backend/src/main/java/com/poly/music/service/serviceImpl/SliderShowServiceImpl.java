package com.poly.music.service.serviceImpl;

import com.poly.music.entity.SlideShow;
import com.poly.music.entity.SongPlaylist;
import com.poly.music.repo.SlideshowRepo;
import com.poly.music.service.SlideshowService;
import com.poly.music.ultil.ConvertUltil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SliderShowServiceImpl implements SlideshowService {

  @Autowired
  SlideshowRepo silderShowRepo;

  @Override
  public List<SlideShow> getAllSlide() {
    List<SlideShow> list = silderShowRepo.findAll();
    for (SlideShow slideShow : list) {
      slideShow.setBase64(ConvertUltil.convertBase64(slideShow.getPathImg()));
    }
    return list;
  }

  @Override
  public List<SlideShow> findByCondition(String slideName) {
    List<SlideShow> list;
    if (slideName == null || "".equals(slideName.trim())) {
      list = silderShowRepo.findAll();
    } else {
      list = silderShowRepo.findAllByTitleContaining(slideName.trim());
    }
    for (SlideShow slideShow : list) {
      slideShow.setBase64(ConvertUltil.convertBase64(slideShow.getPathImg()));
    }
    return list;
  }

  @Override
  public SlideShow createOrUpdateSlide(SlideShow slideShow) {
    return silderShowRepo.save(slideShow);
  }

  @Override
  public Optional<SlideShow> getSlideById(Long id) {
    Optional<SlideShow> optional = silderShowRepo.findById(id);
    if (optional.isPresent()) {
      optional.get().setBase64(ConvertUltil.convertBase64(optional.get().getPathImg()));
    }
    return optional;
  }

  @Override
  public void deleteById(Long id) {
    silderShowRepo.deleteById(id);
  }

  @Override
  public List<SlideShow> getAllByStatus() {

    List<SlideShow> songPlaylists = silderShowRepo.getAllByStatus(1);
    if (songPlaylists != null && !songPlaylists.isEmpty()) {
      for (SlideShow songPlaylist : songPlaylists) {
        songPlaylist.setBase64(ConvertUltil.convertBase64(songPlaylist.getPathImg()));
      }
    }
    return songPlaylists;
  }

//    @Override
//    public List<SlideShow> get7SlideRandom() {
//        return silderShowRepo.get7SlideRandom();
//    }
}
