package com.poly.music.service.serviceImpl;

import com.poly.music.dto.SingerDTO;
import com.poly.music.dto.SongDTO;
import com.poly.music.dto.SongUploadDTO;
import com.poly.music.entity.*;
import com.poly.music.login.CustomeUser;
import com.poly.music.repo.*;
import com.poly.music.service.SongService;
import com.poly.music.ultil.ConvertUltil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class SongServiceImpl implements SongService {

  @Autowired private SongRepo songRepo;

  @Autowired private ArtistRepo artistRepo;

  @Autowired private SongArtistRepo songArtistRepo;

  @Autowired private GenreRepo genreRepo;

  @Autowired private CountryRepo countryRepo;

  @Autowired private UserRepo userRepo;

  @Override
  public void viewSong(Long songId) {
    Optional<Song> optional = songRepo.findById(songId);
    if (optional.isPresent()) {
      Song song = optional.get();
      Long view = song.getView() != null ? song.getView() : 0;
      song.setView(view + 1);
      songRepo.save(song);
    }
  }

  @Override
  public List<SongDTO> getTopSongByCountry(Integer topNumber, Long countryId, boolean isMonth) {
    return artistRepo.getTopSongByCountry(topNumber, countryId, isMonth);
  }

  @Override
  public List<Song> findAllByUserIdAndStatus(Long userId, Integer status) {
    return songRepo.findAllByUserIdAndStatusOrderByReleaseDateDesc(userId, status);
  }

  @Override
  public void revokeSongUpload(Long songId) {
    songRepo.deleteById(songId);
  }

  @Override
  public Optional<Song> findSongById(Long songId) {
    return songRepo.findById(songId);
  }

  @Override
  public List<Song> getAllSong() {
    List<Song> list = songRepo.findAll();
    for (Song song : list) {
      song.setBase64(ConvertUltil.convertBase64(song.getPathAvatar()));
    }
    return list;
  }

  @Override
  @Transactional
  public Song createOrUpdateSong(Song song, List<Long> singerId) {
    Song newSong = songRepo.saveAndFlush(song);
    if (singerId != null && singerId.size() > 0) {
      songRepo.deleteSongArtistBySongId(newSong.getId());
      for(Long id: singerId){
        SongArtist songArtist = new SongArtist();
        songArtist.setArtistId(id);
        songArtist.setStatus(1);
        songArtist.setSongId(newSong.getId());
        songArtistRepo.save(songArtist);
      }
    }
    return newSong;
  }

  @Override
  public void deleteById(Long id) {
    songRepo.deleteById(id);
  }

  @Override
  public List<Song> findByCondition(String songName) {
    List<Song> list;
    if (songName == null || "".equals(songName.trim())) {
      list = songRepo.findAll();
    }else
    list = songRepo.findAllByNameContaining(songName.trim());
    for (Song song : list) {
      song.setBase64(ConvertUltil.convertBase64(song.getPathAvatar()));
    }
    return list;
  }

  @Override
  public Song getById(Long id) {
    Optional<Song> optional = songRepo.findById(id);
    if (optional.isPresent()) {
      Song song = optional.get();
      song.setBase64(ConvertUltil.convertBase64(song.getPathAvatar()));
      song.setSongBase64(ConvertUltil.convertBase64(song.getPath()));
      return song;
    }
    return null;
  }

  @Override
  public SingerDTO getArtistBySongId(Long id) {
    return songRepo.getSingDTOBySongId(id);
  }

  @Transactional
  @Override
  public void uploadSong(SongUploadDTO songUploadDTO, Authentication authentication) {
    CustomeUser user = (CustomeUser) authentication.getPrincipal();

    Song song = new Song();
    song.setName(songUploadDTO.getSongName());
    song.setDescription(songUploadDTO.getSongDescription());
    song.setPath(songUploadDTO.getPathTrack());
    song.setPathAvatar(songUploadDTO.getPathAvatar());
    song.setGenreId(songUploadDTO.getSongGenreId());
    song.setCountryId(songUploadDTO.getSongCountryId());
    song.setApproved(2);
    song.setReleaseDate(new Date());
    song.setLyric("");
    song.setTime(0);
    song.setStatus(1);
    song.setView(0L);
    song.setType(2);
    song.setUserId(user.getUser().getId());

    if (songUploadDTO.getArtistId() != null && !songUploadDTO.getArtistId().isEmpty()) {
      for (Long artistId: songUploadDTO.getArtistId()) {
        Song newSong = songRepo.saveAndFlush(song);
        SongArtist songArtist = new SongArtist();
        songArtist.setSongId(newSong.getId());
        songArtist.setStatus(1);
        songArtist.setArtistId(artistId);
        songArtistRepo.save(songArtist);
      }
    }

  }

  @Override
  public List<Song> getAllSongUploadByCustomer() {
    List<Song> songs = songRepo.getAllSongUploadByCustomer();
    for (Song song : songs) {
      if (song.getPathAvatar() != null && !"".equals(song.getPathAvatar())) {
        song.setBase64(
                ConvertUltil.convertBase64(song.getPathAvatar()));
      }

      if (song.getPath() != null && !"".equals(song.getPath())) {
        song.setSongBase64(ConvertUltil.convertBase64(song.getPath()));
      }
    }
    return songs;
  }

  @Override
  public SongUploadDTO getDetailSongUpload(Long songId) {
    Optional<Song> songOptional = songRepo.findById(songId);
    if (songOptional.isPresent()) {
      Song song = songOptional.get();
      List<SongArtist> artistList = songArtistRepo.findAllBySongIdAndStatus(songId, 1);

      SongUploadDTO songUploadDTO = new SongUploadDTO();
      songUploadDTO.setSongId(song.getId());
      songUploadDTO.setSongName(song.getName());

      if (artistList != null && !artistList.isEmpty()) {
        if (artistList.size() < 2) {
          Optional<Artist> artist = artistRepo.findById(artistList.get(0).getId());
          if (artist.isPresent()) {
            songUploadDTO.setArtistName(artist.get().getName());
            songUploadDTO.setArtistGender(artist.get().getGender());
            songUploadDTO.setSongCountryName(countryRepo.findById(artist.get().getCountryId()).get().getName());
          }
        } else {
          StringBuilder stringBuilder = new StringBuilder();
          for (int i = 0; i < artistList.size(); i++) {
            Optional<Artist> artist = artistRepo.findById(artistList.get(i).getId());
            if (artist.isPresent()) {
              if (i == artistList.size() - 1) {
                stringBuilder.append(artist.get().getName());
              } else {
                stringBuilder.append(artist.get().getName());
                stringBuilder.append(",");
              }
            }
          }
          songUploadDTO.setArtistGender(3);
          songUploadDTO.setSongCountryName("");
          songUploadDTO.setArtistName(stringBuilder.toString());
        }
      }

      songUploadDTO.setSongDescription(song.getDescription());
      songUploadDTO.setDateOfUpload(song.getReleaseDate());

      if (song.getPathAvatar() != null && !"".equals(song.getPathAvatar())) {
        songUploadDTO.setBase64Avatar(
            ConvertUltil.convertBase64(song.getPathAvatar()));
      }

      if (song.getPath() != null && !"".equals(song.getPath())) {
        songUploadDTO.setBase64Track(ConvertUltil.convertBase64(song.getPath()));
      }

      if (song.getGenreId() != null) {
        Optional<Genre> genre = genreRepo.findById(song.getGenreId());
        if (genre.isPresent()) {
          songUploadDTO.setSongGenreName(genre.get().getName());
        } else {
          songUploadDTO.setSongGenreName("");
        }
      }

      if (song.getCountryId() != null) {
        Optional<Country> country = countryRepo.findById(song.getCountryId());
        if (country.isPresent()) {
          songUploadDTO.setSongCountryName(country.get().getName());
        } else {
          songUploadDTO.setSongGenreName("");
        }
      }

      if (song.getUserId() != null) {
        Optional<User> optionalUser = userRepo.findById(song.getUserId());
        if (optionalUser.isPresent()) {
          songUploadDTO.setUserName(optionalUser.get().getUsername());
        } else {
          songUploadDTO.setUserName("");
        }
      }

      return songUploadDTO;
    } else {
      return null;
    }
  }

  @Override
  public void acceptOrRejectSongUpload(Long id, Integer approved) {
    songRepo.acceptOrRejectSongUpload(id, approved);
  }
}
