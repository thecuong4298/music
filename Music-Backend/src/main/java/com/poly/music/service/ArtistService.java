package com.poly.music.service;

import com.poly.music.dto.ArtistByCountryDTO;
import com.poly.music.dto.SongDTO;
import com.poly.music.entity.Artist;

import java.util.List;
import java.util.Optional;

public interface ArtistService {

  List<Artist> getAllArtistActive();

  List<ArtistByCountryDTO> getAllArtistByCountry();

  List<SongDTO> getSongByArtist(Long artistId);

  List<Artist> getTop4Artist();

  Artist getArtistById(Long artistId);
  
  List<Artist> getAllArtist();
  
  Artist createOrUpdateArtist(Artist artist);
  
  Artist getById(Long id);
  
  Optional<Artist> getArtistsById(Long id);
  
  void deleteById(Long id);
  
  List<Artist> findByCondition(String artistName);

  String getArtistBySongId(Long songId);
}
