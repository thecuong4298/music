package com.poly.music.service;
import org.springframework.web.multipart.MultipartFile;



public interface FileService {
  String upLoadFile(MultipartFile file, String type) throws Exception;

  String[] upLoadSong(MultipartFile file) throws Exception;

}
