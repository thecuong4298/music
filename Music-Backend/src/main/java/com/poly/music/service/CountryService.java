package com.poly.music.service;

import com.poly.music.entity.Country;
import java.util.List;

public interface CountryService {

  List<Country> getAllActive();

}
