package com.poly.music.service.serviceImpl;

import com.poly.music.entity.SongPlaylist;
import com.poly.music.repo.SongPlaylistRepo;
import com.poly.music.service.SongPlaylistService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SongPlaylistServiceImpl implements SongPlaylistService {

  @Autowired
  private SongPlaylistRepo songPlaylistRepo;

  @Override
  public Boolean addSongToPlaylist(SongPlaylist songPlaylist) {
    List<SongPlaylist> songPlaylists = songPlaylistRepo.findAllByPlaylistIdAndSongId(songPlaylist.getPlaylistId(), songPlaylist.getSongId());
    if (songPlaylists == null || songPlaylists.isEmpty()) {
      songPlaylistRepo.save(songPlaylist);
      return true;
    }
    return false;
  }
}
