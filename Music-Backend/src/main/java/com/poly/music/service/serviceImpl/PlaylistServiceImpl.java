package com.poly.music.service.serviceImpl;

import com.poly.music.entity.Playlist;
import com.poly.music.entity.Song;
import com.poly.music.entity.SongPlaylist;
import com.poly.music.entity.Topic;
import com.poly.music.repo.PlayListRepo;
import com.poly.music.repo.SongPlaylistRepo;
import com.poly.music.repo.SongRepo;
import com.poly.music.service.PlaylistService;
import com.poly.music.ultil.ConvertUltil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class PlaylistServiceImpl implements PlaylistService {

    @Autowired
    PlayListRepo playListRepo;

    @Autowired
    SongPlaylistRepo songPlaylistRepo;

    @Autowired
    SongRepo songRepo;

    @Override
    public List<Playlist> getPlaylistByUserId(Long id, Integer status) {
        List<Playlist> list = playListRepo.findAllByUserIdAndStatus(id, status);
        for (Playlist playlist : list) {
            playlist.setBase64(ConvertUltil.convertBase64(playlist.getPath()));
        }
        return list;
    }

    @Override
    public Playlist save(Playlist playlist) {
        playlist.setCreateDate(new Date());
        return playListRepo.save(playlist);
    }

    @Override
    public Optional<Playlist> getPlaylistById(Long id) {
        return playListRepo.findById(id);
    }

    @Override
    public void deleteById(Long id) {
        List<SongPlaylist> songPlaylists = songPlaylistRepo.findAllByPlaylistId(id);
        if (songPlaylists != null && !songPlaylists.isEmpty()) {
            for (SongPlaylist songPlaylist : songPlaylists) {
                songPlaylistRepo.deleteById(songPlaylist.getId());
            }
        }

        playListRepo.deleteById(id);
    }

    @Override
    public void deleteSongInPlaylist(Long idPlaylist, Long idSong) {
        List<SongPlaylist> songPlaylists = songPlaylistRepo.findAllByPlaylistIdAndSongId(idPlaylist, idSong);
        if (songPlaylists != null && !songPlaylists.isEmpty()) {
            for (SongPlaylist songPlaylist : songPlaylists) {
                songPlaylistRepo.deleteById(songPlaylist.getId());
            }
        }

    }

    @Override
    public List<Song> findSongInPlaylistById(Long idPlaylist) {
        List<Song> list =songRepo.findSongInPlaylistById(idPlaylist);
        for (Song artist : list) {
            artist.setBase64(ConvertUltil.convertBase64(artist.getPathAvatar()));
        }
        return list;
    }
}
