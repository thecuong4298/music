package com.poly.music.service;

import com.poly.music.entity.Role;

public interface RoleService {

  Role getRoleById(Long id);

}
