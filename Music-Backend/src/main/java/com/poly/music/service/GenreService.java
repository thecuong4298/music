package com.poly.music.service;

import com.poly.music.dto.SongDTO;
import com.poly.music.entity.Genre;

import com.poly.music.entity.Song;
import java.util.List;
import java.util.Optional;

public interface GenreService {

    List<Genre> getAllGenre();

    List<Genre> findByCondition(String genreName);

    Genre createOrUpdateGenre(Genre genre);

    Optional<Genre> getGenreById(Long id);

    void deleteById(Long id);

    List<Genre> getAllByStatus();

    List<SongDTO> getSongByGener(Long generId);

    List<Genre> get7GenreRandom();
}
