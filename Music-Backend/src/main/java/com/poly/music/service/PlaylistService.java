package com.poly.music.service;

import com.poly.music.entity.Playlist;
import com.poly.music.entity.Song;

import java.util.List;
import java.util.Optional;

public interface PlaylistService {

    List<Playlist> getPlaylistByUserId(Long id, Integer status);

    Playlist save(Playlist playlist);

    Optional<Playlist> getPlaylistById(Long id);

    void deleteById(Long id);

    void deleteSongInPlaylist(Long idPlaylist, Long idSong);

    List<Song> findSongInPlaylistById(Long idPlaylist);

}
