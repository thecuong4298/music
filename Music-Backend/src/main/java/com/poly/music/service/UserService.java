package com.poly.music.service;

import com.poly.music.entity.Genre;
import com.poly.music.entity.User;

import java.util.List;
import java.util.Optional;

public interface UserService {
  List<User> getAllUser();

  List<User> findByCondition(String userName);

  User save(User user);

  Optional<User> getUserById(Long id);

  void deleteById(Long id);

  List<User> getAllByStatus();

  List<User> get7UserRandom();

  Optional<User> findByUsername(String username);
}
