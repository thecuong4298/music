package com.poly.music.service.serviceImpl;

import com.poly.music.dto.SearchResponDTO;
import com.poly.music.dto.SongDTO;
import com.poly.music.entity.Album;
import com.poly.music.entity.Artist;
import com.poly.music.repo.ArtistRepo;
import com.poly.music.service.SearchService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SearchServiceImpl implements SearchService {

  @Autowired
  private ArtistRepo artistRepo;

  @Override
  public SearchResponDTO search(String search, boolean maxResult) {

    List<SongDTO> songDTO = artistRepo.searchSong(search, true);

    List<Artist> artists = artistRepo.getArtistByName(search, true);

    List<Album> albums = artistRepo.getAlbumByName(search, true);

    SearchResponDTO searchResponDTO = new SearchResponDTO(songDTO, albums, artists);
    return searchResponDTO;
  }
}
