package com.poly.music.service.serviceImpl;

import com.poly.music.entity.AuthenReissuePassword;
import com.poly.music.entity.User;
import com.poly.music.repo.AuthenReissuePasswordRepo;
import com.poly.music.repo.UserRepo;
import com.poly.music.service.ReissuePasswordService;
import com.poly.music.ultil.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class ReissuePasswordImpl implements ReissuePasswordService {

  @Autowired private JavaMailSender javaMailSender;

  @Autowired
  private AuthenReissuePasswordRepo authenReissuePasswordRepo;

  @Autowired
  private UserRepo userRepo;

  @Override
  @Transactional
  public String sendEmailAuthen(String email, String username) {

    Optional<User> optionalUser = userRepo.findByEmailAndUsernameAndStatus(email, username, 1);

    if (!optionalUser.isPresent()) {
      return "False";
    } else {
      String otpCode = this.getAlphaNumericString(6);
      Base64.Encoder encoder = Base64.getMimeEncoder();
      String eStr = encoder.encodeToString(otpCode.getBytes());

      try {
        sendEmail(email, otpCode);

        AuthenReissuePassword authenReissuePassword = new AuthenReissuePassword();
        authenReissuePassword.setCreateTime(new Date());
        authenReissuePassword.setDecoded(otpCode);
        authenReissuePassword.setOtpCode(eStr);
        authenReissuePassword.setUsername(username);
        authenReissuePassword.setStatus(1);

        authenReissuePasswordRepo.save(authenReissuePassword);

        return "Success";

      } catch (Exception e) {
        e.printStackTrace();
        return "";
      }
    }
  }

  @Override
  public String findAllByOtpCodeAndUsernameAndStatusOrderByCreateTimeDesc(String otpCode, String username, Integer status) throws BusinessException {

    Base64.Encoder encoder = Base64.getMimeEncoder();
    String eStr = encoder.encodeToString(otpCode.getBytes());

    List<AuthenReissuePassword> list = authenReissuePasswordRepo.findAllByOtpCodeAndUsernameAndStatusOrderByCreateTimeDesc(eStr, username, status);
    if (list == null || list.isEmpty()) {
      throw new BusinessException("Mã OTP không chính xác");
    } else {
      AuthenReissuePassword authenReissuePassword = list.get(0);
      if (authenReissuePassword.getStatus() == 0) {
        throw new BusinessException("Mã otp đã hết hạn");
      } else {
        return "Success";
      }
    }
  }

  void sendEmail(String email, String content) {

    SimpleMailMessage msg = new SimpleMailMessage();
    msg.setTo(email);

    msg.setSubject("Reissue Password");
    msg.setText(content);

    javaMailSender.send(msg);
  }

  public String getAlphaNumericString(int n) {
    String AlphaNumericString =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "0123456789" + "abcdefghijklmnopqrstuvxyz";
    StringBuilder sb = new StringBuilder(n);
    for (int i = 0; i < n; i++) {
      int index = (int) (AlphaNumericString.length() * Math.random());
      sb.append(AlphaNumericString.charAt(index));
    }
    return sb.toString();
  }
}
