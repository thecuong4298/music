package com.poly.music.service;

import com.poly.music.dto.SongDTO;
import com.poly.music.entity.Album;

import java.util.List;
import java.util.Optional;

public interface AlbumService {

  List<Album> getAllActive();

  List<SongDTO> getSongByAlbum(Long albumId);

  Album getById(Long id);

  List<Album> get4AlbumRandom();

  List<Album> getAllAlbum();
  
  Album createOrUpdateAlbum(Album album);
  
  void deleteById(Long id);
  
  Optional<Album> getAlbumById(Long id);
  
  List<Album> findByCondition(String albumName);

  List<Album> getAlbumByArtist(Long artistId);
}
