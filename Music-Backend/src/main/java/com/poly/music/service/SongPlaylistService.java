package com.poly.music.service;

import com.poly.music.entity.SongPlaylist;

public interface SongPlaylistService {

  Boolean addSongToPlaylist(SongPlaylist songPlaylist);

}
