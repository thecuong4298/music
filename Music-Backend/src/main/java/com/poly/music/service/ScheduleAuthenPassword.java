package com.poly.music.service;

import com.poly.music.entity.AuthenReissuePassword;
import com.poly.music.repo.AuthenReissuePasswordRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Configuration
@EnableScheduling
public class ScheduleAuthenPassword {

    @Autowired
    AuthenReissuePasswordRepo authenReissuePasswordRepo;

    @Scheduled(fixedDelay = 10000)
    public void scheduleFixedDelayTask() {
        List<AuthenReissuePassword> list = authenReissuePasswordRepo.findAll();
        if (!list.isEmpty()) {
            for (AuthenReissuePassword authenReissuePassword : list) {
                double timeMinus = this.calculateMinus(authenReissuePassword.getCreateTime());
                if (timeMinus < 15 && timeMinus > 10) {
                    authenReissuePassword.setStatus(0);
                    authenReissuePasswordRepo.save(authenReissuePassword);
                } else if (timeMinus >= 15) {
                    authenReissuePasswordRepo.deleteById(authenReissuePassword.getId());
                }
            }
        }
    }


    public double calculateMinus(Date dob) {
        try {
            Calendar createTime = Calendar.getInstance();
            Calendar now = Calendar.getInstance();
            createTime.setTime(dob);
            now.setTime(new Date());
            return (double) (now.getTime().getTime() - createTime.getTime().getTime()) / (60 * 1000);
        } catch (Exception ex) {
            ex.printStackTrace();
            return 30;
        }
    }

}
