package com.poly.music.service.serviceImpl;

import com.poly.music.entity.Country;
import com.poly.music.repo.CountryRepo;
import com.poly.music.service.CountryService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CountryServiceImpl implements CountryService {

  @Autowired
  private CountryRepo countryRepo;

  @Override
  public List<Country> getAllActive() {
    return countryRepo.getAllByStatus(1);
  }
}
