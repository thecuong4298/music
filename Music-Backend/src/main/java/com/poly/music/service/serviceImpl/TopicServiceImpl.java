package com.poly.music.service.serviceImpl;

import com.poly.music.dto.SongDTO;
import com.poly.music.entity.Album;
import com.poly.music.entity.SongArtist;
import com.poly.music.entity.SongTopic;
import com.poly.music.entity.Topic;
import com.poly.music.repo.ArtistRepo;
import com.poly.music.repo.SongRepo;
import com.poly.music.repo.SongTopicRepo;
import com.poly.music.repo.TopicRepo;
import com.poly.music.service.TopicService;
import com.poly.music.ultil.ConvertUltil;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TopicServiceImpl implements TopicService {

  @Autowired
  private TopicRepo topicRepo;

  @Autowired
  private ArtistRepo artistRepo;

  @Autowired
  private SongTopicRepo songTopicRepo;

  @Autowired
  private SongRepo songRepo;

  @Override
  public List<Topic> getTopicActive() {
    List<Topic> list = topicRepo.getAllByStatus(1);
    for (Topic artist : list) {
      artist.setBase64(ConvertUltil.convertBase64(artist.getPath()));
    }
    return list;
  }


  @Override
  public List<SongDTO> getSongByTopic(Long topicId) {
    return artistRepo.getSongsByTopic(topicId);
  }

  @Override
  public List<Topic> get3TopicRandom() {
    List<Topic> list = topicRepo.get3topicRandom();
    for (Topic artist : list) {
      artist.setBase64(ConvertUltil.convertBase64(artist.getPath()));
    }
    return list;
  }

  @Override
  @Transactional
  public Topic createOrUpdateTopic(Topic topic, List<Long> lstSongId) {
    Topic newTopic = topicRepo.saveAndFlush(topic);
    if (lstSongId != null && lstSongId.size() > 0) {
      songRepo.deleteSongTopicBySongId(newTopic.getId());
      for (Long id : lstSongId) {
        SongTopic songTopic = new SongTopic();
        songTopic.setTopicId(newTopic.getId());
        songTopic.setStatus(1);
        songTopic.setSongId(id);
        songTopicRepo.save(songTopic);
      }
    }
    return newTopic;
  }

  @Override
  public List<Topic> getAllTopic() {
    List<Topic> list = topicRepo.findAll();
    for (Topic artist : list) {
      artist.setBase64(ConvertUltil.convertBase64(artist.getPath()));
    }
    return list;
  }


  @Override
  public Optional<Topic> getTopicById(Long id) {
    Optional<Topic> optional = topicRepo.findById(id);
    if (optional.isPresent()) {
      optional.get().setBase64(ConvertUltil.convertBase64(optional.get().getPath()));
    }
    return optional;
  }


  @Override
  public void deleteById(Long id) {
    topicRepo.deleteById(id);

  }


  @Override
  public List<Topic> findByCondition(String topicName) {
    List<Topic> list;
    if (topicName == null || "".equals(topicName.trim())) {
      list = topicRepo.findAll();
    } else {
      list = topicRepo.findAllByNameContaining(topicName.trim());
    }
    for (Topic topic : list) {
      topic.setBase64(ConvertUltil.convertBase64(topic.getPath()));
    }
    return list;
  }
}
