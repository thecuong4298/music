package com.poly.music.service.serviceImpl;

import com.poly.music.entity.Role;
import com.poly.music.repo.RoleRepo;
import com.poly.music.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl implements RoleService {

  @Autowired
  private RoleRepo roleRepo;

  @Override
  public Role getRoleById(Long id) {
    return roleRepo.getById(id);
  }
}
