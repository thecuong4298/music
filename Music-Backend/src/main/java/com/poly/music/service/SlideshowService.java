package com.poly.music.service;

import com.poly.music.entity.SlideShow;

import java.util.List;
import java.util.Optional;


public interface SlideshowService {


    List<SlideShow> getAllSlide();

    List<SlideShow> findByCondition(String slideName);

    SlideShow createOrUpdateSlide(SlideShow slideShow);

    Optional<SlideShow> getSlideById(Long id);

    void deleteById(Long id);

    List<SlideShow> getAllByStatus();

//    List<SongDTO> getSongByGener(Long generId);

//    List<SlideShow> get7GenreRandom();

//    List<SlideShow> findAllByStatus(Integer status);

}
