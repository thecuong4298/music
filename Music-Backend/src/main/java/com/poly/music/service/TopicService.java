package com.poly.music.service;

import com.poly.music.dto.SongDTO;
import com.poly.music.entity.Topic;

import java.util.List;
import java.util.Optional;

public interface TopicService {

  List<Topic> getTopicActive();
  
  List<Topic> findByCondition(String topicName);
  
  List<Topic> getAllTopic();

  Optional<Topic> getTopicById(Long id);
  
  void deleteById(Long id);

  List<SongDTO> getSongByTopic(Long topicId);

  List<Topic> get3TopicRandom();
  
  Topic createOrUpdateTopic(Topic topic, List<Long> lstSongId);

}
