package com.poly.music.service;

import com.poly.music.ultil.BusinessException;

public interface ReissuePasswordService {

    String sendEmailAuthen(String email, String username);

    String findAllByOtpCodeAndUsernameAndStatusOrderByCreateTimeDesc(String otpCode, String username, Integer status) throws BusinessException;

}
