package com.poly.music.service.serviceImpl;

import com.poly.music.service.FileService;
import com.poly.music.ultil.ConvertUltil;
import java.util.Date;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;

@Service
public class FileServiceImpl implements FileService {

  @Value("${pathUpload}")
  private String mstrUpload;

  @Value("${pathUploadTrack}")
  private String pathUploadTrack;

  @Value("${pathUploadSongAvatar}")
  private String pathUploadSongAvatar;

  @Override
  public String upLoadFile(MultipartFile file, String type) throws Exception {

    int vintIndex = file.getOriginalFilename().lastIndexOf(".");
    Date date = new Date();
    long millis = date.getTime();
    String vstrOriginalName =
        file.getOriginalFilename().substring(0, vintIndex) + millis + file.getOriginalFilename()
            .substring(vintIndex, file.getOriginalFilename().length());
    String vstrPath = mstrUpload + "/" + type;

    File customDir = new File(vstrPath);
    ;

    if (!customDir.exists()) {
      customDir.mkdir();
    }

    saveFile(file.getInputStream(), vstrPath + "/" + vstrOriginalName);
    return type + "/" + vstrOriginalName;
  }

  @Override
  public String[] upLoadSong(MultipartFile file) throws Exception {
    int vintIndex = file.getOriginalFilename().lastIndexOf(".");
    Date date = new Date();
    long millis = date.getTime();
    String vstrOriginalName =
        file.getOriginalFilename().substring(0, vintIndex) + millis + file.getOriginalFilename()
            .substring(vintIndex, file.getOriginalFilename().length());
    String vstrPath = mstrUpload + "/song";

    File customDir = new File(vstrPath);
    ;

    if (!customDir.exists()) {
      customDir.mkdir();
    }

    saveFile(file.getInputStream(), vstrPath + "/" + vstrOriginalName);
      return new String[]{"song/" + vstrOriginalName, ConvertUltil.convertBase64("song/" + vstrOriginalName)};
  }

  private void saveFile(InputStream inputStream, String pstrPath) throws IOException {

    OutputStream outputStream = new FileOutputStream(new File(pstrPath));
    int vintRead;
    byte[] bytes = new byte[1024];

    while ((vintRead = inputStream.read(bytes)) != -1) {
      outputStream.write(bytes, 0, vintRead);
    }
    outputStream.flush();
    outputStream.close();
  }
}
